﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SharedErp.Models;
using Microsoft.AspNetCore.Authorization;
using SharedErp.Models.Enum;

namespace SharedErp.Controllers
{

    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        [Route("transport")]
        public IActionResult Transport()
        {
            return View();
        }
        [Route("vetting")]
        public IActionResult Vetting()
        {
            return View();
        }
        [Authorize]
        public IActionResult Privacy()
        {
            //TempData["SectorId"] = SectorsName.InventoryAndPOS.ToString();
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
