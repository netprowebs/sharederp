import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {authenticatedRoutes, IAuthenticatedRoutes} from '../../models/routeModel';


@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  @Input() currentPage! : string
  currentPageRoute: IAuthenticatedRoutes = {...authenticatedRoutes}

  constructor(private route: Router, public toastCtrl: ToastrService) { }

  ngOnInit(): void {
  }

  logout() {
    window.sessionStorage.clear();
    this.route.navigate(['/login'])
    this.toastCtrl.success('Logged out successfully')
  }

  onClick() {
    const sidebar: any = document.querySelector(".sidebar");
    sidebar.classList.toggle("-translate-x-full");
  }


}
