export interface IUser {
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    address?: string;
    companyId?: Number;
    dateJoined?: string;
    dateOfBirth?: string;
    deviceType?: Number;
    gender?: string;
    idExpireDate?: string;
    idIssueDate?: string;
    identificationCode?: string;
    identificationType?: Number;
    middleName?: string;
    nextOfKin?: string;
    nextOfKinPhone?: string;
    referralCode?: string;
    referrer?: string
    title?: string;
    userType?: Number;
}

export const blankUser: IUser = {
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: '',
    idExpireDate: '',
    companyId: 0,
    dateJoined: '',
    dateOfBirth: '',
    deviceType: 0,
    gender: '',
    idIssueDate: '',
    identificationCode: '',
    nextOfKin: '',
    identificationType: 0,
    referralCode: '',
    title: '',
    userType: 0,
    referrer: '' 
}