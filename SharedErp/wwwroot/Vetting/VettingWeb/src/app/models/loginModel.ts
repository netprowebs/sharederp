export interface ILoginModel {
    username: string;
    password: string;
}

export const blankLogin: ILoginModel = {
    username: '',
    password: ''
}