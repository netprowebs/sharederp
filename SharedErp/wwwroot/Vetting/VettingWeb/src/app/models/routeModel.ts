export interface IAuthenticatedRoutes {
    DASHBOARD: string;
    COMPLETEDVETTING: string;
    PENDINGVETTING: string;
    VETHISTORY: string;
    SETTINGS: string;
}


export const authenticatedRoutes: IAuthenticatedRoutes = {
    DASHBOARD: 'dashboard',
    COMPLETEDVETTING: 'completed-vetting',
    PENDINGVETTING: 'pending-vetting',
    VETHISTORY: 'vet-history',
    SETTINGS: 'settings'
}