export interface ISecurity {
    currentPassword: string;
    newPassword: string;
    confirmPassword?: string;
}

export const blankSecurity:ISecurity = {
    currentPassword: '',
    newPassword: '',
    confirmPassword: ''
}