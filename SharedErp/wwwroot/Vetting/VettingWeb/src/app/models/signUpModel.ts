export interface ISignUpModel {
    customerFirstName: string;
    customerLastName: string;
    customerPhone: string;
    customerEmail: string;
    password: string;
    confirmPassword?:string;
    tenantId?: Number;
}

export const BlankSignup: ISignUpModel = {
    customerFirstName: '',
    customerLastName: '',
    customerPhone: '',
    customerEmail: '',
    password: '',
    confirmPassword: '',
    tenantId: 0
}