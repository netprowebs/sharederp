export interface IContactUs {
    fullName: string;
    email: string;
    message: string;
}

export const blankContact: IContactUs = {
    fullName: '',
    email: '',
    message: ''
}