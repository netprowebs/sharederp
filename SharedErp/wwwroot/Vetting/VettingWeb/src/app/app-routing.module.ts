import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { PlansComponent } from './pages/plans/plans.component';
import { LoginComponent } from './pages/login/login.component';
import { CreateAccountComponent } from './pages/create-account/create-account.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthGuard } from './core/auth/auth-guard';
import { CompletedVettingComponent } from './pages/completed-vetting/completed-vetting.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { PendingVettingComponent } from './pages/pending-vetting/pending-vetting.component';
import { VetHistoryComponent } from './pages/vet-history/vet-history.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { SubscribeComponent } from './pages/subscribe/subscribe.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'plans',
    component: PlansComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'createAccount',
    component: CreateAccountComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'completed-vetting',
    component: CompletedVettingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'settings',
    component: SettingsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'contact-us',
    component: ContactUsComponent
  },
  {
    path: 'pending-vetting',
    component: PendingVettingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'vet-history',
    component: VetHistoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'subscribe',
    component: SubscribeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: ''
  }
];



@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
