import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ILoginModel } from '../../../models/loginModel';
import { ISignUpModel } from '../../../models/signUpModel';
import { IUser } from 'src/app/models/userModel';
import { ISecurity } from 'src/app/models/securityModel';
import { IContactUs } from 'src/app/models/contactModel';


@Injectable({
  providedIn: 'root'
})
export class ApiClientService {
  private token: any = window.sessionStorage.getItem('token');


  constructor(private http: HttpClient) {

  }

  //* all api calls would be all here
  //* could use observables or promises
 
  async toLogin(obj: ILoginModel) {
    const res: any = await this.http.post('Token', obj)
      .toPromise();
    if (res.code === '200' && res.shortDescription === 'SUCCESS') {
      const { token } = res.object;
      window.sessionStorage.setItem('token', token);
      this.token = token;
      this.getUserProfile().then((response: any) => {
        const { object } = response;
        const userObj: IUser = {
          firstName: object.firstName,
          lastName: object.lastName,
          phoneNumber: object.phoneNumber,
          email: object.email,
          address: object.address
        };
        window.sessionStorage.setItem('user', JSON.stringify(userObj));
      });
      return res;
    } else {
      return res;
    }
  }

  async toRegister(data: ISignUpModel) {
    const res = await this.http.post('CustomerInformation/add', data)
      .toPromise();
    return res;
  }

  async getUserProfile() {
    const res: any = await this.http.get('Account/GetProfile', {
      headers: new HttpHeaders({
        Authorization: `Bearer ${this.token}`
      })
    })
      .toPromise();
    return res;
  }

  async updateUserProfile(data: IUser) {
    const response: any = await this.http.post('Account/UpdateProfile', data, {
      headers: new HttpHeaders({
        Authorization: `Bearer ${this.token}`
      })
    })
      .toPromise()
    return response
  }

  async changePassword(data: ISecurity) {
    const res: any = await this.http.post('Account/ChangePassword', data, {
      headers: new HttpHeaders({
        Authorization: `Bearer ${this.token}`
      })
    })
      .toPromise();
    return res;
  }

  async sendFeedback(data: IContactUs) {
    const resp: any = await this.http.post('Feedback/AddFeedback', data)
    .toPromise();
    return resp
  }

  async activateAccount(data: any) {
    const resp: any = await this.http.post('Account/Activate', data)
    .toPromise();
    return resp;
  }



}
