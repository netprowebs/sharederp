import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  private token: any = window.sessionStorage.getItem('token')
  constructor(private _router: Router){}

  canActivate(): boolean {
    if(this.token) {
      return true
    } else {
      this._router.navigate(['/login'])
      return false
    }
  }
  
}
