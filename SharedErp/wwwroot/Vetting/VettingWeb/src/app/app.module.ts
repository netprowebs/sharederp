import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { HomeComponent } from './pages/home/home.component';
import { SideBarComponent } from './layouts/side-bar/side-bar.component';
import { HeaderComponent } from './layouts/header/header.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PlansComponent } from './pages/plans/plans.component';
import { BlackBackgroundComponent } from './components/black-background/black-background.component';
import { WhiteBackgroundComponent } from './components/white-background/white-background.component';
import { LoginComponent } from './pages/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CreateAccountComponent } from './pages/create-account/create-account.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { CompletedVettingComponent } from './pages/completed-vetting/completed-vetting.component';
import { PendingVettingComponent } from './pages/pending-vetting/pending-vetting.component';
import { VetHistoryComponent } from './pages/vet-history/vet-history.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { SettingsModalComponent } from './components/modals/settings-modal/settings-modal.component';
import { NotificationModalComponent } from './components/modals/notification-modal/notification-modal.component';
import { ActivationModalComponent } from './components/modals/activation-modal/activation-modal.component';
import { ProfileModalComponent } from './components/modals/profile-modal/profile-modal.component';
import { CardModalComponent } from './components/modals/card-modal/card-modal.component';
import { LoaderComponent } from './components/loader/loader.component';
import { EncodeHttpParamsInterceptor } from './core/services/interceptors/customInterceptor';
import { AuthGuard } from './core/auth/auth-guard'

// Angular Material

import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatDividerModule} from '@angular/material/divider';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { BackArrowComponent } from './components/back-arrow/back-arrow.component';
import { InsufficientUnitModalComponent } from './components/modals/insufficient-unit-modal/insufficient-unit-modal.component';
import { SubscribeComponent } from './pages/subscribe/subscribe.component';






@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SideBarComponent,
    HeaderComponent,
    FooterComponent,
    PlansComponent,
    BlackBackgroundComponent,
    WhiteBackgroundComponent,
    LoginComponent,
    CreateAccountComponent,
    DashboardComponent,
    LoaderComponent,
    CompletedVettingComponent,
    PendingVettingComponent,
    VetHistoryComponent,
    SettingsComponent,
    SettingsModalComponent,
    NotificationModalComponent,
    ProfileModalComponent,
    CardModalComponent,
    ActivationModalComponent,
    ContactUsComponent,
    BackArrowComponent,
    InsufficientUnitModalComponent,
    SubscribeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatProgressBarModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatSlideToggleModule,
    MatDividerModule,
    HttpClientModule,
    ToastrModule.forRoot(), // ToastrModule added
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: EncodeHttpParamsInterceptor,
    multi: true
  },
    AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
