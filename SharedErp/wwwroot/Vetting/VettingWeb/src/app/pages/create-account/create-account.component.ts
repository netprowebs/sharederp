import { Component, OnInit } from '@angular/core';
import { ISignUpModel, BlankSignup } from '../../models/signUpModel';
import { ApiClientService } from '../../core/services/apis/api-client.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {
  account: ISignUpModel = { ...BlankSignup }
  loading: boolean = false;
  opened: boolean = false;
  email: string = '';

  constructor(private _apiService: ApiClientService, private toastr: ToastrService,
    private router: Router) { }

  ngOnInit(): void {
    window.sessionStorage.clear();
  }


  onClickNav() {
    this.router.navigate(['/'])
  }

  createAccount() {
    //eventually call api

    if (this.account.customerFirstName === '' ||
      this.account.customerLastName === '' ||
      this.account.customerEmail === '' ||
      this.account.customerPhone === '' ||
      this.account.password === '' ||
      this.account.confirmPassword === '') {
      this.toastr.error('All Fields are required')
      return;
    } else if (this.account.confirmPassword !== this.account.password) {
      this.toastr.error('Password and confirm password do not match');
      return;

    }
    this.loading = true;
    const payload: ISignUpModel = {
      customerFirstName: this.account.customerFirstName,
      customerLastName: this.account.customerLastName,
      customerEmail: this.account.customerEmail,
      customerPhone: this.account.customerPhone,
      password: this.account.password,
      tenantId: 13
    }
    this._apiService.toRegister(payload)
      .then((data: any) => {
        //do success 
        if(data.code === '200') {
          this.loading = false;
          this.toastr.success('Kindly enter the activation code sent to your email');
          this.opened = true;
          this.email = this.account.customerEmail;
          this.account = {...BlankSignup}
        } else {
          this.toastr.error(data.code);
          this.loading = false;
        }
      })
      .catch(({message}) => {
        this.toastr.error(message);
        this.loading = false;
      })
  }

}
