import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IContactUs, blankContact } from '../../models/contactModel';
import { ToastrService } from 'ngx-toastr';
import { ApiClientService } from 'src/app/core/services/apis/api-client.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  contactUsForm: IContactUs = { ...blankContact }
  loading: boolean = false;

  constructor(private router: Router, private toastCtrl: ToastrService, private auth_service: ApiClientService) { }

  ngOnInit(): void {
  }

  goToHome() {
    this.router.navigate(['/'])
  }

  onSubmitted() {
    if (this.contactUsForm.fullName === '' ||
      this.contactUsForm.email === '' ||
      this.contactUsForm.message === '') {
      this.toastCtrl.error('All fields are required')
      return;
    }
    this.loading = true;
    this.auth_service.sendFeedback(this.contactUsForm)
    .then((response: any) => {
      if(response.code === '200') {
        console.log(response, 'res')
      }
    })
    .catch(({message}) => {
      this.toastCtrl.error(message);
      this.loading = false;
    })
  }

}
