import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ILoginModel, blankLogin } from '../../models/loginModel';
import { ApiClientService } from '../../core/services/apis/api-client.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginCredentials: ILoginModel = { ...blankLogin }
  loading: boolean = false;
  constructor(private _apiService: ApiClientService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    window.sessionStorage.clear();
  }

  onClickNav() {
    this.router.navigate(['/'])
  }

  login() {
    if (this.loginCredentials.username === '' || this.loginCredentials.password === '') {
      this.toastr.error('All fields are required')
      return;
    }
    this.loading = true;
    //eventually call api
    this._apiService.toLogin(this.loginCredentials)
      .then((data: any) => {
        //the navigate to dashboard
        if (data.shortDescription === 'SUCCESS') {
          setTimeout(() => {
            this.toastr.success('Login Successful')
            this.loading = false;
            this.router.navigate(['/dashboard'])
          }, 3000)
        } else {
          // Invalid credentials
          this.toastr.error(data.shortDescription)
          this.loading = false;
          return;
        }
      })
      .catch(({ message }) => {
        this.toastr.error(message)
        this.loading = false;
      })
  }

}
