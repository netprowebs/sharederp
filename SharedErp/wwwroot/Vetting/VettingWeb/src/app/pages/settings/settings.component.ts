import { Component, OnInit } from '@angular/core';
import {IUser, blankUser} from '../../models/userModel';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  customer: IUser = {...blankUser}
  showProfileModal: boolean = false;
  showNotificationModal: boolean = false;
  showCardModal: boolean = false;
  showSecurityModal: boolean = false;
  opened: boolean = false;
  constructor() { }

  ngOnInit(): void {
    let user: any = window.sessionStorage.getItem('user')
    this.customer = JSON.parse(user)
  }

  onClickProfile() {
    this.showCardModal = false;
    this.showNotificationModal = false;
    this.showSecurityModal = false;
    this.showProfileModal = true;
    this.opened = true;
  }

  onClickNotifications() {
    this.showSecurityModal = false;
    this.showProfileModal = false;
    this.showCardModal = false;
    this.showNotificationModal = true;
    this.opened = true;
  }

  onClickCardSettings() {

  }

  onClickSecuritySettings() {
    this.showCardModal = false;
    this.showNotificationModal = false;
    this.showProfileModal = false;
    this.showSecurityModal = true;
    this.opened = true;
    
  }

}
