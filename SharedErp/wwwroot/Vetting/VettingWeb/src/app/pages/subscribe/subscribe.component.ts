import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnInit {
  plans: Array<any> = [
    'Diam integer pellentesque pulvinar gravida quis.',
    'Diam integer pellentesque pulvinar gravida quis.',
    'Diam integer pellentesque pulvinar gravida quis.',
    'Diam integer pellentesque pulvinar gravida quis.',
    'Diam integer pellentesque pulvinar gravida quis.'
  ];
  planTypes: Array<any> = [
    {
      type: 'Regular'
    },
    {
      type: 'Standard'
    },
    {
      type: 'Premium'
    },
    {
      type: 'VIP'
    }
  ]

  megaTypes: Array<any> = [
    {
      type: 'Mega'
    },
    {
      type: 'Mega Plus'
    }
  ]

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToHome() {
    this.router.navigate(['/dashboard'])
  }
}
