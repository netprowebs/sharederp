import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {
  plans: Array<any> = [
    'Diam integer pellentesque pulvinar gravida quis.',
    'Diam integer pellentesque pulvinar gravida quis.',
    'Diam integer pellentesque pulvinar gravida quis.',
    'Diam integer pellentesque pulvinar gravida quis.',
    'Diam integer pellentesque pulvinar gravida quis.'
  ];
  planTypes: Array<any> = [
    {
      type: 'Regular'
    },
    {
      type: 'Standard'
    },
    {
      type: 'Premium'
    },
    {
      type: 'VIP'
    }
  ]

  megaTypes: Array<any> = [
    {
      type: 'Mega'
    },
    {
      type: 'Mega Plus'
    }
  ]

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToHome() {
    this.router.navigate(['/'])
  }

}
