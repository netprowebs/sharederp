import { Component, OnInit } from '@angular/core';
import {IUser, blankUser} from '../../models/userModel';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  userDetails: IUser = {...blankUser}
  opened: boolean = false;

  constructor() { }

  ngOnInit(): void {
    let user: any = window.sessionStorage.getItem('user')
    this.userDetails = JSON.parse(user)
  }

  startVetting() {
    this.opened = true;
  }
}
