import { Component, OnInit } from '@angular/core';
import { ApiClientService } from '../../core/services/apis/api-client.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private apiService : ApiClientService) { }

  ngOnInit(): void {
    window.sessionStorage.clear();
  }

}
