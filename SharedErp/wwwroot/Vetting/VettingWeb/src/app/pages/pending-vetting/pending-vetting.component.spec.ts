import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingVettingComponent } from './pending-vetting.component';

describe('PendingVettingComponent', () => {
  let component: PendingVettingComponent;
  let fixture: ComponentFixture<PendingVettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendingVettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingVettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
