import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedVettingComponent } from './completed-vetting.component';

describe('CompletedVettingComponent', () => {
  let component: CompletedVettingComponent;
  let fixture: ComponentFixture<CompletedVettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompletedVettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedVettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
