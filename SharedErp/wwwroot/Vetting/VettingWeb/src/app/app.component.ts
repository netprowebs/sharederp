import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { authenticatedRoutes, IAuthenticatedRoutes } from './models/routeModel';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  title = 'VettingWeb';
  showSideBar: boolean = false;
  currentPage: string = '';
  authRoutes: IAuthenticatedRoutes = {...authenticatedRoutes}

  constructor(private router: Router) {
    this.router.events.pipe(
      filter((event: any) => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      let url: string = event.url.replace('/', '');
      this.currentPage = url;
      if (Object.values(this.authRoutes).includes(url)) {
        this.showSideBar = true
      } else {
        this.showSideBar = false
      }
    });
  }


}
