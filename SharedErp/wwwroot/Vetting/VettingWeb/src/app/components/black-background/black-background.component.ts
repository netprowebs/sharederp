import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-black-background',
  templateUrl: './black-background.component.html',
  styleUrls: ['./black-background.component.scss']
})
export class BlackBackgroundComponent implements OnInit {
  
  @Output() btnClick = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onClickIcon() {
    this.btnClick.emit()
  }

}
