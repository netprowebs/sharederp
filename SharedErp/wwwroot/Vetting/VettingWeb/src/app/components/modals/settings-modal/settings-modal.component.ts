import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ISecurity, blankSecurity } from '../../../models/securityModel';
import { ToastrService } from 'ngx-toastr';
import { ApiClientService } from '../../../core/services/apis/api-client.service';

@Component({
  selector: 'app-settings-modal',
  templateUrl: './settings-modal.component.html',
  styleUrls: ['./settings-modal.component.scss']
})
export class SettingsModalComponent implements OnInit {
  @Input() opened!: boolean;
  @Output() close = new EventEmitter();

  securityDetails: ISecurity = { ...blankSecurity }
  loading: boolean = false;
  notMatch: boolean = false;
  mustNotMatch: boolean = false;
  constructor(private toastCtrl: ToastrService, private api_service: ApiClientService) { }

  ngOnInit(): void {
  }

  onClose() {
    this.close.emit();
  }

  onKeyNewPassword() {
    this.mustNotMatch = false;
    if (this.securityDetails.currentPassword === this.securityDetails.newPassword) {
      this.mustNotMatch = true
    } else {
      this.mustNotMatch = false;
    }
  }

  onConfirmPassword() {
    this.notMatch = false;
    if (this.securityDetails.confirmPassword !== this.securityDetails.newPassword) {
      this.notMatch = true;
    } else {
      this.notMatch = false
    }
  }


  updatePassword() {
    if (this.securityDetails.currentPassword === '' ||
      this.securityDetails.newPassword === '' ||
      this.securityDetails.confirmPassword === '') {
      this.toastCtrl.error('All fields are required.')
      return;
    } else if (this.securityDetails.currentPassword === this.securityDetails.newPassword) {
      this.mustNotMatch = true
      this.toastCtrl.error('New password must be different from current password')
      return
    }
    const payload: ISecurity = {
      currentPassword: this.securityDetails.currentPassword,
      newPassword: this.securityDetails.newPassword
    }
    this.loading = true;
    this.api_service.changePassword(payload)
      .then((response: any) => {
        if (response.code === '200' && response.isValid) {
          this.toastCtrl.success('Password changed successfully')
          this.securityDetails = { ...blankSecurity }
          this.loading = false;
        } else {
          this.toastCtrl.error(response.shortDescription);
          this.loading = false;
        }
      })
      .catch(({ message }) => {
        this.toastCtrl.error(message)
        this.loading = false;
      })
  }

}
