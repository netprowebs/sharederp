import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-insufficient-unit-modal',
  templateUrl: './insufficient-unit-modal.component.html',
  styleUrls: ['./insufficient-unit-modal.component.scss']
})
export class InsufficientUnitModalComponent implements OnInit {
  @Input() opened!: boolean;
  @Output() close = new EventEmitter();
  
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onClose() {
    this.close.emit();
  }

  buy() {
    this.router.navigate(['/subscribe'])
  }

}
