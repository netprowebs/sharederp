import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiClientService } from 'src/app/core/services/apis/api-client.service';

@Component({
  selector: 'app-activation-modal',
  templateUrl: './activation-modal.component.html',
  styleUrls: ['./activation-modal.component.scss']
})
export class ActivationModalComponent implements OnInit {
  @Input() opened!: boolean;
  @Input() email!: string;
  @Output() close = new EventEmitter();

  activateAccount: {usernameOrEmail: string; activationCode: string} = {
    usernameOrEmail: '',
    activationCode: ''
  }
  loading: boolean = false;

  constructor(private auth_service: ApiClientService, private toastCtrl: ToastrService, private router: Router) { }

  ngOnInit(): void {
      this.activateAccount.usernameOrEmail = this.email
  }

  onClose() {
    this.close.emit();
  }

  activate() {
    if(this.activateAccount.activationCode === '' ||
      this.activateAccount.usernameOrEmail === '') {
        this.toastCtrl.error('All fields are required');
        return;
      }
    this.loading = true;
    this.auth_service.activateAccount(this.activateAccount)
    .then((response: any) => {
      if(response.code === '200') {
        this.toastCtrl.success('Account successfully created, Kindly login to continue your account setup');
        setTimeout(() => {
          this.loading = false;
          this.activateAccount.usernameOrEmail = '';
          this.activateAccount.activationCode = '';
          this.router.navigate(['/login'])
        }, 2500)
      } else {
        this.toastCtrl.error(response.shortDescription);
        this.loading = false;
      }
    })
    .catch(({message}) => {
      this.toastCtrl.error(message);
      this.loading = false;
    })
  }

}
