import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsufficientUnitModalComponent } from './insufficient-unit-modal.component';

describe('InsufficientUnitModalComponent', () => {
  let component: InsufficientUnitModalComponent;
  let fixture: ComponentFixture<InsufficientUnitModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsufficientUnitModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsufficientUnitModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
