import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-notification-modal',
  templateUrl: './notification-modal.component.html',
  styleUrls: ['./notification-modal.component.scss']
})
export class NotificationModalComponent implements OnInit {
  @Input() opened!: boolean
  @Output() close = new EventEmitter();

  isOffersChecked: boolean = false;
  isFeedbackChecked: boolean = false;
  constructor(private toastCtrl: ToastrService) { }

  ngOnInit(): void {
  }

  onClose() {
    this.close.emit();
  }

}
