import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiClientService } from 'src/app/core/services/apis/api-client.service';
import { IUser, blankUser } from '../../../models/userModel'

@Component({
  selector: 'app-profile-modal',
  templateUrl: './profile-modal.component.html',
  styleUrls: ['./profile-modal.component.scss']
})
export class ProfileModalComponent implements OnInit {
  @Input() opened!: boolean;
  @Output() close = new EventEmitter();

  loading: boolean = false;
  fetchingUserProfile: boolean = true;
  userProfile: IUser = { ...blankUser }

  constructor(private auth_service: ApiClientService, private toastCtrl: ToastrService) { }

  ngOnInit(): void {
    this.getProfile();
  }

  onClose() {
    this.close.emit();
  }

  getProfile() {
    this.auth_service.getUserProfile()
      .then((response: any) => {
        if (response.code === '200' && response.isValid) {
          const { object } = response;
          this.userProfile = { ...object }
          this.fetchingUserProfile = false;
        }
      })
      .catch(({ message }) => {
        this.toastCtrl.error(message)
        this.fetchingUserProfile = false;
      })
  }

  updateProfile() {
    if (this.userProfile.firstName === '' ||
      this.userProfile.lastName === '' ||
      this.userProfile.email === '') {
      this.toastCtrl.error('Kindly fill in the blank fields');
      return;
    }
    this.loading = true;
    this.auth_service.updateUserProfile(this.userProfile)
    .then((response: any) => {
      if(response.code === '200' && response.isValid) {
        const storedUser: any = window.sessionStorage.getItem('user');
        const parsedUser: IUser = JSON.parse(storedUser);
        parsedUser.firstName = this.userProfile.firstName;
        parsedUser.lastName = this.userProfile.lastName;
        parsedUser.email = this.userProfile.email;
        window.sessionStorage.setItem('user', JSON.stringify(parsedUser))
        setTimeout(() => {
          this.toastCtrl.success('Profile updated Successfully')
          this.loading = false
        }, 2000)
      }
    })
    .catch(({message}) => {
      this.toastCtrl.error(message);
      this.loading = false;
    })
  }

}
