﻿// XUI Framework (v1)
// Author: Alexander Karagulamos
// March 3rd, 2017

window.Utility = window.Utility || GetUtility();

$(function () {
    if (!$("#modal-container")[0]) {
        var modalContainer =
            '<div id="modal-container" class="modal fade col-xs-12" tabindex="-1" role="dialog">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content"></div>' +
                "</div>" +
             "</div>";

        $("body").prepend(modalContainer);
    }

    $(".ui-dialog-source").on("click", function () {
        var targetRoute = $(this).data("ui-target-route");

        $(".ui-dialog-ok").on("click", function () {
            window.location = targetRoute;
        });
    });

    $("body").on("click", ".ui-form-view-modal", function (e) {
        e.preventDefault();

        $(this).attr("data-target", "#modal-container");
        $(this).attr("data-toggle", "modal");
        $('#mainer').show().css('display', 'flex');
        var targetFormActionRoute = $(this).data("ui-target-route");

        if (targetFormActionRoute) {
            $(document).on("loaded.bs.modal", "#modal-container", function () {
                $("form").attr("action", targetFormActionRoute);
            });
        }
    });



    $("body").on('loaded.bs.modal', function () {
    	//$('#mainer').hide();
    	$("#mainer").hide().css('display', 'none');

    });





    $("body").on("click", ".modal-close-btn", function () {
        $("#modal-container").modal("hide");
        //$('#mainer').hide();

    });

    $(document).on("hidden.bs.modal", "#modal-container", function () {
        $("div.model-content").html("");
        $(this).removeData("bs.modal");
    });

    $(document).ajaxComplete(function () {
        $.validator.unobtrusive.parse($("form"));
    });

    $.ajaxSetup({
        cache: false,
        dataFilter: function (result) {
            var data = Utility.JSON.Parse(result);

            if (data.hasOwnProperty("code") && data.hasOwnProperty("isSubmit") && data.isSubmit === false) {
                $("#modal-container").modal("toggle");

                if (data.code && (data.code === Utility.StatusCodes.Unauthorized || data.code === Utility.StatusCodes.InternalServerError)) {
                    window.location = data.redirectTo;
                    return false;
                }

                if ($("#ui-form-alert-msg")[0]) {
                    $("#ui-form-alert-msg").remove();
                }

                var alert = Utility.GetAlert("A connection with the server could not be established.", "ui-async-alert", "warning");

                $("#ui-async-alert").replaceWith(alert);

                Utility.RemoveElement(".ui-alert-handle", 3000);

                return false;
            }

            return result;
        },

        headers: { 'x-ui-framework': "true" }
    });

    $(document).on("click", "#ui-submit-form", function (event) {
        event.preventDefault();
        $("#modal-container").trigger("loaded.bs.modal");

        debugger;
        var submitButton = this;
        var form = this.form;

        if ($(form).valid()) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                headers: { 'x-ui-framework-submit': "true" },
                beforeSend: function () {
                    $(submitButton).prop("disabled", true);
                },
                success: function (result) {
                    if (!result.hasOwnProperty("code") && !result.hasOwnProperty("message")) {
                        $(form).replaceWith($(result).filter("form"));
                        return;
                    }

                    debugger;
                    var isSuccessCode = result.code === Utility.StatusCodes.Successful;
                    var isUnauthorized = result.code === Utility.StatusCodes.Unauthorized;

                    if (result.message) {
                        var alertType = isUnauthorized ? "error" : isSuccessCode ? "success" : "warning";

                        var alertBox = Utility.GetAlert(result.message, "ui-form-alert-msg", alertType);

                        var alert = $("#ui-form-alert-msg");

                        if (alert[0]) {
                            alert.replaceWith(alertBox);
                        } else {
                            $(form).prepend(alertBox);
                        }
                    }

                    if (isSuccessCode || isUnauthorized) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                        return;
                    }

                    $(submitButton).removeAttr("disabled");
                }
            }).fail(function () {
                $(submitButton).removeAttr("disabled");
            });
        }

        return false;
    });

    if ($("#ui-table-search")[0]) {
        $("#ui-table-search").dataTable({
            responsive: true,
            aLengthMenu: [
                [5, 10, 12, 20],
                [5, 10, 12, 20]
            ]
        });

        $("#ui-table-search_filter input[type=search], #ui-table-search_length select").unbind();

        var searchDiv = $("#ui-table-search_filter");

        var searchForm =
            '<form action = "" id="ui-table-search_filter" class="dataTables_filter">' +
                searchDiv.html() +
                '<button type=submit style="color: blue;" class="form-control input-ms">' +
                '<i class="fa fa-search"></i>' +
                "</button>" +
                "</form>";

        searchDiv.replaceWith(searchForm);

        $(document).on("click", "#ui-table-search_filter button[type=submit]", function (event) {
            event.preventDefault();

            var params = {
                page: 1,
                size: $("#ui-table-search_length select").val(),
                searchTerm: $("#ui-table-search_filter input[type=search]").val()
            };

            var query = Utility.Urls.buildQueryString(params);

            window.location = window.location.pathname + encodeURI(query ? "?" + query : "");
        });

        var pagedResult = Utility.Cookies.get(".UI.Paged.Search.Result");

        if (pagedResult) {
            var paginated = JSON.parse(pagedResult);
            var actualSize = paginated.Size < paginated.Count ? paginated.Size : paginated.Count;
            var footerText = "Showing " + paginated.Page + " to " + actualSize + " of " + paginated.Count + " entries.";

            $("#ui-table-search_filter input[type=search]").val(paginated.SearchTerm);
            $("#ui-table-search_length select").val(paginated.Size);

            if (actualSize < paginated.Count) {
                $("#ui-table-search_paginate li.next").removeClass("disabled");
            }

            if (actualSize > paginated.Count) {
                $("#ui-table-search_paginate li.prev").removeClass("disabled");
            }

            $("#ui-table-search_paginate ul li.active > a").text(paginated.Page);

            $("#ui-table-search_paginate li.next").on("click",
                function () {

                    var params = {
                        page: paginated.Page + 1,
                        size: paginated.Size,
                        searchTerm: $("#ui-table-search_filter input[type=search]").val()
                    };

                    var query = Utility.Urls.buildQueryString(params);

                    window.location = window.location.pathname + encodeURI((query ? "?" + query : ""));
                });

            $("#ui-table-search_info").text(footerText);
        }
    }

    //Utility.RemoveElement(".ui-alert-handle", 20000);
});

function GetUtility() {
    return {
        Cookies: {
            get: function (cname) {
                var name = cname + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(";");

                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) === " ") {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) === 0) {
                        return c.substring(name.length, c.length);
                    }
                }

                return "";
            }
        },

        Urls: {
            buildQueryString: function (params) {
                var query = "", keys = Object.keys(params);

                for (var idx = 0; idx < keys.length; ++idx) {
                    var key = keys[idx];

                    if (params[key]) {
                        query += query ? "&" : "";
                        query += key + "=" + String(params[key]).trim();
                    }
                }

                return query;
            }
        },

        JSON: {
            Parse: function (text) {
                if (!/^\s*(?:{\s*.*\s*}|\[\s*{\s*.*\s*}.*\s*\])\s*$/g.test(text)) {
                    return text;
                }

                return JSON.parse(text.replace(/^[\"\s]*|[\"\s]*$/g, ""));
            }
        },

        StatusCodes: {
            Successful: "200",
            Unauthorized: "401",
            InternalServerError: "500",
            ServiceUnavailable: "503"
        },

        ResponseTypes: {
            Json: "json",
            Html: "html"
        },

        GetAlert: function (message, targetId, alertType) {
            return '<div class="col-xs-12 ui-alert-handle" id="' + targetId + '">' +
                    '<div class="page-title alert-' + alertType + ' alert-dismissable">' +
                    '<div class="pull-left">' +
                    '<h1 style="color: white;">' + message + "</h1>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
        },

        RemoveElement: function (element, timeout) {
            setTimeout(function () {
                $(element).fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, timeout);
        }
    };
}