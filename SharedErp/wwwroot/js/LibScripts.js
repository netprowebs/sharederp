﻿
var xmlobject = false;

try {
    //create the object for older internet explorer
    xmlobject = new ActiveXObject("Microsoft.XMLHTTP");
} catch (e) {
    try {
        //create for new versions of internet explorer
        xmlobject = new ActiveXObject("Mssml2.XMLHTTP");
    } catch (E) {
        xmlobject = false;
    }
}

//If we are using a non-IE browser, create a javascript instance of the object. showswapbo
if (!xmlobject && typeof XMLHttpRequest !== 'undefined') {
    xmlobject = new XMLHttpRequest();
} else {
    //alert ("You must enable javascript on your browser to use this site");	
}

//////////////////////////////////////////////////////////

function getData(page, url, loading, target) {
    $.ajax({
        method: "POST",
        url: url + page,
        data: { page: page },
        beforeSend: function () {
            $(loading).show();
        },
        success: function (data) {
            $(loading).hide();
            $(target).html(data);
        }
    });
}

/////////////////// LIB API//////////////////////////////////////
function LIB_CallView(controller, action, param, objid) {

    var objide = document.getElementById(objid);

    xmlobject.open("GET", document.location.origin + "/" + controller + "/" + action + "?" + param);

    //objid.innerHTML = '<div id="loader" class ="center-block"><img width="32" height="32"  src="http://localhost:5000/images/loading.gif" /></div>';

    xmlobject.onreadystatechange = function () {

        if (xmlobject.readyState === 4 && xmlobject.status === 200) {
            objide.innerHTML = xmlobject.responseText; //alert(xmlobject.responseText);
        } else if (xmlobject.readyState === 3) {
            objide.innerHTML = '';
        }

    }
    xmlobject.send(null);

}

function LIB_CallPost(controller, action, param, objid) {



    var objide = document.getElementById(objid);

    $.ajax({
        url: document.location.origin + "/" + controller + "/" + action,
        contentType: "application/json; charset=utf-8",
        data: {'captainCode': param },
        type: 'GET',
        cache: false,
        success: function (result) {
            objide.innerHTML =result;
        }
    });

    //xmlobject.open("POST", document.location.origin + "/" + controller + "/" + action);

    ////objid.innerHTML = '<div id="loader" class ="center-block"><img width="32" height="32"  src="http://localhost:5000/images/loading.gif" /></div>';

    //xmlobject.onreadystatechange = function () {

    //    if (xmlobject.readyState == 4 && xmlobject.status == 200) {
    //        objid.innerHTML = xmlobject.responseText; //alert(xmlobject.responseText);
    //    } else if (xmlobject.readyState == 3) {
    //        objid.innerHTML = '';
    //    }

    //}
    //xmlobject.send(param);

}

function LIB_CallAssign(controller, action, param, objid) {


    var objide = document.getElementById(objid);

    $.ajax({
        url: document.location.origin + "/" + controller + "/" + action,
        contentType: "application/json; charset=utf-8",
        data: { 'VehicleTripRegistrationId': param },
        type: 'GET',
        cache: false,
        success: function (result) {
            objide.innerHTML = result;
        }
    });

    //xmlobject.open("POST", document.location.origin + "/" + controller + "/" + action);

    ////objid.innerHTML = '<div id="loader" class ="center-block"><img width="32" height="32"  src="http://localhost:5000/images/loading.gif" /></div>';

    //xmlobject.onreadystatechange = function () {

    //    if (xmlobject.readyState == 4 && xmlobject.status == 200) {
    //        objid.innerHTML = xmlobject.responseText; //alert(xmlobject.responseText);
    //    } else if (xmlobject.readyState == 3) {
    //        objid.innerHTML = '';
    //    }

    //}
    //xmlobject.send(param);

}


$(document).ready(function () {
	$('.btn-default').click(function () {
		$('#mainer').hide().css('display', 'none');
	});
});

function LIB_Calljourney(controller, action, param, objid) {


    $('#mainer').show().css('display', 'flex');
    $('.modal-dialog').hide().css('display', 'none');
    var objide = document.getElementById(objid);

    $.ajax({
        url: document.location.origin + "/" + controller + "/" + action,
        contentType: "application/json; charset=utf-8",
        data: { 'JourneyManagementId': param },
        type: 'GET',
        cache: false,
        success: function (result) {
            $('#mainer').hide().css('display', 'none');
        
            objide.innerHTML = result;
         
            $('.modal-dialog').show();


        },

        error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.status + " Error has Occured, Pls try again");
        $("#mainer").hide().css('display', 'none');

        //$('.modal-dialog').hide().css('display', 'none');
        $("#mainer").hide().css('display', 'none');


    }
    });

}

function LIB_Callvehicletrip(controller, action, param, objid, passengers) {
    debugger;
    var objide = document.getElementById(objid);

    $.ajax({
        url: document.location.origin + "/" + controller + "/" + action,
        contentType: "application/json; charset=utf-8",
        data: { 'VehicleTripRegistrationId': param, 'Passengers': passengers },
        type: 'GET',
        cache: false,
        success: function (result) {
            objide.innerHTML = result;
        }
         
    });
    setTimeout(function () {   //calls click event after a certain time
        $('.dropdown-select2').select2({
            dropdownParent: $('#assignModal')
        });
    }, 1800);
    

}


function LIB_FormSubmitAPI(urlPage, buttonName, formName, ParentdivID) {
    var x = document.location.origin;
    urlPage = x.concat(urlPage);
    $.ajax(
    {
        type: "post",
        url: urlPage,
        data: $(formName).serialize(),
        success: function (response) {
            if (response === "done") {
                //alert("Form submitted successfully!");
                document.getElementById(ParentdivID).innerHTML = "<pre>" + "Operation Was Successful!" + "</pre>";

                //submitCreateFormData
                document.getElementById(ParentdivID).innerHTML = "<pre>" + "Operation Was Successful!" + "</pre>";
            }

        },
        error: function (response) { alert(response); }
    });

};

function LIB_FormSubmitAPI_WithRefreshDIV(urlPage, buttonName, formName, ParentdivID, refreshURL, refreshAction, refreshParam, refreshDIV, MODE) {

    var refreshParamVal = "";

    if (refreshParam !== "") {
        refreshParamVal = refreshParam;
    }
    var x = document.location.origin;
   // urlPage = x.concat(urlPage);
    console.log("url:",urlPage);
    $.ajax(
    {
        type: "post",
        url: urlPage,
        data: $(formName).serialize(),
        success: function (response) {
            if (response === "done") {
                //alert("Form submitted successfully!");
                document.getElementById(ParentdivID).innerHTML = "<pre>" + "Operation Was Successful!" + "</pre>";

                LIB_CallView(refreshURL, refreshAction, refreshParamVal, refreshDIV);

                // reset form
                if (MODE === 'CREATE') {
                    $(formName).trigger("reset");
                }

            }

        },
        error: function (response) {
            console.log(response);
            alert(response);
        }
    });


};

function LIB_FormSubmitAPI_WithRefreshDIV_WithFileUpload(urlPage, buttonName, formName, ParentdivID, refreshURL, refreshAction, refreshParam, refreshDIV, MODE, uploadForm) {

    var refreshParamVal = "";

    if (refreshParam !== "") {
        refreshParamVal = refreshParam;
    }

    var form = document.forms.namedItem(uploadForm);
    var formData = new FormData(form);
    var x = document.location.origin;
    urlPage = x.concat(urlPage);

    //alert(y);
    //formData.append('image', $('input[type=file]')[0].files[0]);
    //formData.append('file1', $('input[type=file]')[0].files[0]);

    //alert(typeof (formData));


    $.ajax({
        type: "POST",
        url: urlPage,
        //data: $(formName).serialize(),
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            if (response === "done") {
                //alert("Form submitted successfully!");
                document.getElementById(ParentdivID).innerHTML = "<pre>" + "Operation Was Successful!" + "</pre>";

                LIB_CallView(refreshURL, refreshAction, refreshParamVal, refreshDIV);

                // reset form
                if (MODE === 'CREATE') {
                    $(formName).trigger("reset");
                }

            }
        },
        error: function (response) {
            console.log(response);
            alert(response);
        }
    });
};


function callform_upload(urlPage, buttonName, formName, ParentdivID, refreshURL, refreshAction, refreshParam, refreshDIV, MODE) {
    var x = document.location.origin;
    urlPage = x.concat(urlPage);
    //var myForm = document.getElementById('cform');
    var form = document.forms.namedItem("pickUpForm");
    var formData = new FormData(form);
    //formData.append("image", window.fileInputElement.files[0]);

    var objid = document.getElementById(ParentdivID);

    //xmlobject.open("GET", controller + "/" + method);
    xmlobject.open("POST", urlPage, true);
    xmlobject.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xmlobject.onreadystatechange = function () {
        if (xmlobject.responseText.indexOf("done") > -1) {
            alert("done = success ");
            objid.innerHTML = xmlobject.responseText; //alert(xmlobject.responseText);
        } else {
            //alert(xmlobject.responseText);
            alert("done = failed ");
            objid.innerHTML = xmlobject.responseText; //alert(xmlobject.responseText);
            //location.reload();
        }
    }
    xmlobject.send(formData);
    //}
}


function LIB_refreshPage(reponseResult, div) {

    if (reponseResult === "done") {
        //document
    }

};


function LIB_ReloadPage() {
    location.reload(true);
    self.close();
}



///////////////////////////////////////////////////////////////////////