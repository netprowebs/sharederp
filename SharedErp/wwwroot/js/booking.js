﻿$(function () {
    //$(".datepicker").datepicker({
    //    startDate: "+1d",
    //    endDate: "+7d",
    //    //endDate: "Mon, 8 January 2018",
    //    format: "D, dd MM yyyy"
    //});

  

    $(".select-terminal").select2();


    //$("#TripType").change(
    //    function (e) {
    //        e.preventDefault();
    //        if (document.getElementById("TripType").selectedIndex = 0) {
    //            alert("one");
    //        }
    //        else if (document.getElementById("TripType").selectedIndex = 1) {
    //            alert("two");
    //        }
    //       // alert("g");
    //    });


    //$('#example2').DataTable({
    //    responsive: true
    //});
    $.ajaxSetup({
        crossOrigin: true
    });

    $(document).on("change", "#DepartureTerminalId", function () {
        var departureTerminalId = $(this).val();
        var url = "https://cors-anywhere.herokuapp.com/https://client.libmot.com/api/Route/GetDestinationTerminals/" + departureTerminalId;// +"?callback=?";
       // var url = "/terminals/booking/destination/" + departureTerminalId;// +"?callback=?";
        var options =
        {
            "url": url,
            "method": "GET",
            //"dataType": "JSONP",

            "headers": {
                "Access-Control-Allow-Origin": "* ",
                "Content-Type": "application/json",
                "X-Atlassian-Token": "nocheck"
            }
        };
       
        if (departureTerminalId) {
            var destinationTerminalsDropdown = $("#DestinationTerminalId");
            $.getJSON(options, { format: "json" }, function (data) {
                console.log(data);
                var options = "<option value='"+"'> Destination Terminal </option>";
                for (var idx = 0; idx < data.object.length; ++idx) {
                    var terminal = data.object[idx];
                    options +=
                        "<option value='" + terminal.id + "'>" + terminal.name + "</option>";
                }

                destinationTerminalsDropdown.html(options);
            });
        }
    });




    //checks if max seat has been selected and toggles the done button
    function toggleDoneButton(modal) {
        var selectedSeats = modal.find("input[type='checkbox']:checked");
        var maxSeatAllowed = modal.find("input[type=hidden]").val();

        modal.find("#modal-done")
             .prop("disabled", selectedSeats.length < +maxSeatAllowed);
    }

    var previousRow, seatSelectionModal;

    $(document).on("click", ".trip-selection input[type=radio]", function () {
        if (seatSelectionModal && !seatSelectionModal.find(".alert-box.hidden")[0])
            seatSelectionModal.find(".alert-box").addClass("hidden");

        var self = $(this);
        var tripType = self.val();
        var parent = self.parent();

        var previousTripType = previousRow && previousRow.find("input[type=radio]").val();
        var previousTripId = previousRow && previousRow.find("#VehicleTripRegistrationId").val();
        var currentTripId = parent.find("#VehicleTripRegistrationId").val();

        if (previousTripType && previousTripId && previousTripType === tripType && previousTripId !== currentTripId) {
            previousRow.find("#BookedSeats").val("");
        }

        previousRow = parent;

        seatSelectionModal = $("#showSeatSelection" + tripType);

        var availableSeats = parent.find("#AvailableSeats").val();

        var totalNumberOfSeats = parent.find("#TotalNumberOfSeats").val();


        if (totalNumberOfSeats === '14') {
            $(".10seater").remove();
            $(".12seater").remove();
            $(".17seater").remove();
            $(".13seater").remove();
            $(".15seater").remove();

        }
        else if (totalNumberOfSeats === '12') {
            $(".10seater").remove();
            $(".14seater").remove();
            $(".17seater").remove();
            $(".13seater").remove();
            $(".15seater").remove();

        }
        else if (totalNumberOfSeats === '15') {
            $(".10seater").remove();
            $(".12seater").remove();
            $(".14seater").remove();
            $(".17seater").remove();
            $(".13seater").remove();

        }
        else if (totalNumberOfSeats === '10') {

            $(".12seater").remove();
            $(".14seater").remove();
            $(".17seater").remove();
            $(".13seater").remove();
            $(".15seater").remove();
        }
        else if (totalNumberOfSeats === '13') {

            $(".12seater").remove();
            $(".14seater").remove();
            $(".10seater").remove();
            $(".17seater").remove();
            $(".15seater").remove();
        }
        else if (totalNumberOfSeats === '17' || totalNumberOfSeats === '16') {

            $(".12seater").remove();
            $(".14seater").remove();
            $(".10seater").remove();
            $(".13seater").remove();
            $(".15seater").remove();
        }
        else {
            $(".10seater").remove();
            $(".12seater").remove();
            $(".13seater").remove();
            $(".14seater").remove();
            $(".17seater").remove();
            $(".15seater").remove();
        }

        if (availableSeats) {
            var seats = availableSeats.split(",");
            var bookedSeats = parent.find("#BookedSeats").val().split(",");

            for (var idx = 0; idx < seats.length; ++idx) {
                var checked = bookedSeats.indexOf(seats[idx]) > -1 ? "checked" : "";

                var checkBox =
                    "<input style='display:none;' class='label label-success' type='checkbox' name='seats[]' " + checked +
                   " id='" + tripType + seats[idx] + "' data-seat-id='" + seats[idx] + "'> " +
                   "<label title='Seat Available' class='seat-label' for='" + tripType + seats[idx] + "'>" + seats[idx] + "</label>";

                var currentSeat = seats[idx];

                seatSelectionModal.find("tr td:contains(" + currentSeat + ")").filter(function () {
                    return $(this).text() === currentSeat;
                }).html(checkBox);
            }

            toggleDoneButton(seatSelectionModal);
            $(seatSelectionModal).modal("show");
        }
    });

    $(document).on("click", "input[type='checkbox']", function (event) {

        var selectedSeats = seatSelectionModal.find("input[type='checkbox']:checked");
        var maxSeatAllowed = seatSelectionModal.find("input[type=hidden]").val();

        toggleDoneButton(seatSelectionModal);

        if (selectedSeats.length > maxSeatAllowed) {
            event.preventDefault();
            seatSelectionModal.find(".alert-box").removeClass("hidden");
            selectedSeats.unbind("click");
        } else {
            if (!seatSelectionModal.find(".alert-box.hidden")[0])
                seatSelectionModal.find(".alert-box").addClass("hidden");
        }
    });

    $(document).on("click", "#modal-done", function () {
        var checkedBoxes = seatSelectionModal.find("#selectedSeats input[type='checkbox']:checked");

        var selectedValues = checkedBoxes.map(function () {
            return $(this).data("seat-id");
        }).get().join(",");

        previousRow.find("#BookedSeats").val(selectedValues);
        seatSelectionModal.modal("hide");
    });

    $(document).on("click", ".ui-form-view-modal", function () {

        var radioBoxes = $(document).find("input[type='radio']:checked");
        //var routeid = parent.find("#RouteId").val();
        //var issub = parent.find("#isSub").val();
        var seatRegistrations = radioBoxes.map(function (index, self) {
            var parent = $(radioBoxes[index].parentNode);
            var regId = parent.find("#VehicleTripRegistrationId").val();

            var bookedSeats = parent.find("#BookedSeats").val();
            
            $("#SeatRegistrations").val(regId + ":" + bookedSeats) ;

            return regId + ":" + bookedSeats;
        }).get().join(";");

        var routes = radioBoxes.map(function (index) {
            var parent = $(radioBoxes[index].parentNode);
            var routeId = parent.find("#RouteId").val();

            var issub = parent.find("#isSub").val();

            $("#Routes").val(routeId + ":" + issub);

            return routeId + ":" + issub;
        }).get().join(";");

        $(document).on("loaded.bs.modal", function () {
            $(this).find("#SeatRegistrations").val(seatRegistrations);
            $(this).find("#Routes").val(routes);

        });
    });

    var seatsTableTemplate = $(".booking-modal table").html();

    $(document).on("hidden.bs.modal", ".booking-modal", function () {
        $(".booking-modal table").html(seatsTableTemplate);
        $(this).removeData("bs.modal");
    });
});
