﻿namespace SharedErp.Utils
{
    public class Constants
    {
        public const string ClientNoToken = "webapi_no_token";
        public const string ClientWithToken = "webapi_with_token";
        public const string HumanDateFormat = "dd MMMM, yyyy";
        public const string SystemDateFormat = "dd/MM/yyyy";

        public class Keys
        {
            public const string JwtBearerSection = "Authentication:JwtBearer";
            public const string ApiBaseUrl = "App:ApiBaseUrl";
            public const string Alerts = "_Alerts";
        }

        public class URL
        {
            public const string Default = "/home/index";
            public const string LoginPath = "/account/login";
            public const string AccessDeniedPath = "/error/accessdenied";
        }

        public class ClientRoutes
        {
            public const string Token = "/api/token";
            public const string RefreshToken = "/api/token/refreshtoken";

            public const string AccountGetClaims = "/api/account/getcurrentuserclaims";
            public const string AccountGetProfile = "/api/account/getprofile";
            public const string AccountUpdateProfile = "/api/account/updateprofile";
            public const string AccountChangePassword = "/api/account/changepassword";
            public const string AccountResetPassword = "/api/account/ForgotPassword";
            public const string AccountResetNewPassword = "/api/account/ResetPassword";
            public const string AccountGetuserbyemail = "/api/account/GetUserInfo/{0}";

            public const string Drivers = "/api/driver/get";
            public const string Driver = "/api/driver";
            public const string DriverGet = "/api/driver/get/{0}";
            public const string DriverDelete = "/api/driver/delete/{0}";
            public const string DriverCreate = "/api/driver/add";
            public const string DriverUpdate = "/api/driver/update/{0}";
            public const string GetDriverByVehicleReg = "/api/driver/GetDriverbyVehicleRegistrationNumber";
            public const string GetAvailableDriverAsync = "api/driver/getavailabledriverasync";
           


            public const string Employees = "/api/employee/getemployees";
            public const string AllUsers = "/api/employee/getallusers";
            public const string Employee = "/api/employee";
            public const string EmployeeGet = "/api/employee/getemployee/{0}";
            public const string EmployeeDelete = "/api/employee/deleteemployee/{0}";
            public const string EmployeeCreate = "/api/employee/addemployee";
            public const string EmployeeUpdate = "/api/employee/updateemployee/{0}";
            public const string GetEmployeeTerminalId = "/api/employee/getemployeeterminal";
            public const string activateaccount = "/api/employee/activateaccount";

            public const string Roles = "/api/role/get";
            public const string RoleGet = "/api/role/get/{0}";
            public const string RoleDelete = "/api/role/delete/{0}";
            public const string RoleCreate = "/api/role/add";
            public const string RoleCreates = "/api/role/adds";
            public const string RoleUpdate = "/api/role/update/{0}";

            public const string Route = "/api/route";
            public const string Routes = "/api/route/get";
            public const string RouteGet = "/api/route/get/{0}";
            public const string RouteDelete = "/api/route/delete/{0}";
            public const string RouteCreate = "/api/route/add";
            public const string RouteUpdate = "/api/route/update/{0}";
            public const string RouteGetDestinationTerminals = "/api/route/getdestinationterminals/{0}";
            public const string RoutesGetByTerminalId = "/api/route/terminals/routes/{0}";

            public const string SubRoutes = "/api/subroute/get";
            public const string SubRouteGet = "/api/subroute/get/{0}";
            public const string SubRouteDelete = "/api/subroute/delete/{0}";
            public const string SubRouteCreate = "/api/subroute/add";
            public const string SubRouteUpdate = "/api/subroute/update/{0}";

            public const string Manifests = "/api/Manifest";
            public const string OpenManifest = "/api/Manifest/OpenManifest";
            public const string UpdateManifest = "/api/Manifest/GetByVehicleTripId/{0}";
            public const string UpdateManifestAmount = "/api/Manifest/UpdateFare/{0}";
            public const string GetVehicleDetails = "/api/Manifest/getvehicletrip/{0}";

            public const string Terminals = "/api/terminal/get";
            public const string TerminalRoutes = "/api/terminal/getroutes/{0}";
            public const string TerminalGet = "/api/terminal/get/{0}";
            public const string TerminalDelete = "/api/terminal/delete/{0}";
            public const string TerminalCreate = "/api/terminal/add";
            public const string TerminalUpdate = "/api/terminal/update/{0}";
            public const string StaffRoutes = "/api/terminal/getstaffterminalroutes";
            public const string LoginInTerminal = "/api/terminal/getloginemployeeterminal";
            public const string GetVirtualAndPhysicalTerminals = "/api/terminal/getVirtualAndPhysicalTerminals";


            public const string Trips = "/api/trip/get";
            public const string TripGet = "/api/trip/get/{0}";
            public const string TripDelete = "/api/trip/delete/{0}";
            public const string TripCreate = "/api/trip/add";
            public const string TripUpdate = "/api/trip/update/{0}";
            public const string RouteTrips = "/api/trip/getbyrouteid";
            public const string RouteBuses = "/api/route/route/physicalbuses";

            public const string Subroute = "/api/subroute/";

            public const string Regions = "/api/region/get";
            public const string RegionGet = "/api/region/get/{0}";
            public const string RegionDelete = "/api/region/delete/{0}";
            public const string RegionCreate = "/api/region/add";
            public const string RegionUpdate = "/api/region/update/{0}";

            public const string States = "/api/state/get";
            public const string StateGet = "/api/state/get/{0}";
            public const string StateDelete = "/api/state/delete/{0}";
            public const string StateCreate = "/api/state/add";
            public const string StateUpdate = "/api/state/update/{0}";

            public const string Discount = "/api/discount";
            public const string Discounts = "/api/discount/get";
            public const string DiscountGet = "/api/discount/get/{0}";
            public const string DiscountDelete = "/api/discount/delete/{0}";
            public const string DiscountCreate = "/api/discount/add";
            public const string DiscountUpdate = "/api/discount/update/{0}";


            public const string FareCalendar = "/api/farecalendar";
            public const string FareCalendars = "/api/farecalendar/get";
            public const string FareCalendarGet = "/api/farecalendar/get/{0}";
            public const string FareCalendarDelete = "/api/farecalendar/delete/{0}";
            public const string FareCalendarCreate = "/api/farecalendar/add";
            public const string FareCalendarUpdate = "/api/farecalendar/update/{0}";


            public const string Fares = "/api/fare/get";
            public const string FareGet = "/api/fare/get/{0}";
            public const string FareDelete = "/api/fare/delete/{0}";
            public const string FareCreate = "/api/fare/add";
            public const string FareUpdate = "/api/fare/update/{0}";
            public const string FareByRouteIdAndModelId = "/api/fare/GetFareByRouteId/{0}";

            public const string Booking = "api/booking";
            public const string BookingDetails = "api/booking/alldetails/{0}";
            public const string Bookings = "api/booking/search";
            public const string PostBookings = "api/booking/postbooking";
            public const string UpdateBookings = "api/booking/updatebooking";
            public const string GetBookingDetails = "api/booking/details/{0}";
            public const string RescheduleTicketSearch = "api/booking/rescheduleticketsearch";
            public const string RescheduleBooking = "/api/Booking/RescheduleBooking";
            public const string GetSeatsAvailable = "api/booking/getseatsavailable/{0}";
            public const string GetTripHistory = "/api/booking/gettriphistory/{0}";
            public const string GetTraveledCustomers = "/api/booking/GetTraveledCustomers";
            public const string GetAllTraveledCustomers = "/api/booking/GetAllTraveledCustomers";
            public const string GetNewRouteFareByModel = "/api/booking/GetNewRoutefare/{0}/{1}";
            public const string GetNonIdAmount = "/api/booking/GetNewRoutefare/{0}/{1}";
            public const string CancelSeat = "/api/booking/cancelticket/{0}";

            public const string SeatManagement = "api/seatmanagement";

            public const string VehicleM = "/api/vehiclemodel";
            public const string VehicleModels = "/api/vehiclemodel/get";
            public const string VehicleModelGet = "/api/vehiclemodel/get/{0}";
            public const string VehicleModelDelete = "/api/vehiclemodel/delete/{0}";
            public const string VehicleModelCreate = "/api/vehiclemodel/add";
            public const string VehicleModelUpdate = "/api/vehiclemodel/update/{0}";

            public const string VehicleMakes = "/api/vehiclemake/get";
            public const string VehicleMakeGet = "/api/vehiclemake/get/{0}";
            public const string VehicleMakeDelete = "/api/vehiclemake/delete/{0}";
            public const string VehicleMakeCreate = "/api/vehiclemake/add";
            public const string VehicleMakeUpdate = "/api/vehiclemake/update/{0}";

            //add available vehicle
            public const string Vehicles = "/api/vehicle/get";
            public const string Vehicle = "/api/vehicle";
            public const string GetVehicleByIdWithDriverName = "/api/vehicle/getVehicleByIdWithDriverName/{0}";
            public const string VehicleGet = "/api/vehicle/get/{0}";
            public const string VehicleDelete = "/api/vehicle/delete/{0}";
            public const string VehicleCreate = "/api/vehicle/add";
            public const string VehicleUpdate = "/api/vehicle/update/{0}";
            public const string VehiclesInTerminal = "/api/vehicle/getavailablevehiclesinterminal";
            public const string VehiclesByTerminal = "/api/vehicle/GetAvailableVehiclesByTerminal/{0}";
            public const string TerminalToAllocate = "/api/vehicle/GetTerminalHeader";
            public const string VechicleToAllocate = "/api/vehicle";
            public const string Vehiclesdropdown = "/api/vehicle/GetVehicleslist";
            public const string Allocatebuses = "/api/vehicle/allocatebuses";
            public const string VehicleInTerminal = "/api/Vehicle/GetVehiclesByTerminalHeader";
            public const string VehicleByRegNum = "/api/Vehicle/GetByRegNumber/{0}";
            public const string AvailableVehicles = "/api/vehicle/getavailablevehicles";
            public const string UnusedInTerminals = "/api/vehicle/getterminalremainingvehicle/{0}";
            public const string VehicleAllocationConfirm = "/api/vehicle/confirm/{0}";
            public const string DeleteFromVehicleAllocationConfirm = "/api/vehicle/deletefromvehicleallocationtable/{0}";
            public const string GetVehicleByStatus = "/api/vehicle/vehicleByStatus";
            public const string GetVehiclesInWorkshop = "/api/vehicle/GetVehiclesInWorkshop";
            public const string UpdateVehicleStatus = "/api/vehicle/UpdateVehicleStatus/{0}/{1}";

            public const string VehicleTripRegistration = "/api/vehicletripregistration";
            public const string VehicletripsCreateBus = "/api/vehicletripregistration/createphysicalbus";
            public const string UpdateVehicleTripId = "/api/vehicletripregistration/UpdateVehicleTripId";
            public const string RouteIdByDestinationAndTerminalId = "api/route/GetRouteIdByDestinationAndDepartureId/{0}/{1}";
            public const string CreateBlow = "/api/vehicletripregistration/createBlow";
            public const string GetBlownBuses = "/api/vehicletripregistration/getblown";

            public const string Feedbacks = "/api/Feedback/get";
            public const string Feedback = "/api/Feedback";
            public const string FeedbackGet = "/api/Feedback/get/{0}";
            public const string UpdateFeedback = "/api/Feedback/update/{0}";


            //Reports
            public const string BookingReports = "/api/report/search";
            public const string BookedTrips = "/api/report/bookedtrips";
            public const string BookedSales = "/api/report/bookedsales";
            public const string PassengerReport = "/api/report/passengerReport";
            public const string SalesPerBus = "/api/report/salesbybus";
            public const string DriverSalary = "/api/report/driversalary";
            public const string HiredTrips = "/api/report/hiretrips";
            public const string JourneyCharts = "/api/report/journeyCharts";
            public const string JourneyChartsForStateTerminals = "/api/report/journeychartsforstateterminals";
            public const string journeyChartsForTerminals = "/api/report/journeyChartsForTerminals";
            public const string journeyChartsDetailsByState = "/api/report/journeyChartsDetailsByState";
            public const string TotalSalesReportPerState = "/api/report/totalSalesReportPerState";
            public const string TotalTerminalSalesInState = "/api/report/totalTerminalSalesInState";
            public const string TerminalSalesSummary = "/api/report/terminalSalesSummary";
            public const string TotalRevenueReportPerState = "/api/report/totalRevenueReportPerState";
            public const string TotalTerminalRevenueInState = "/api/report/totalTerminalRevenueInState";
            public const string TerminalRevenueSummary = "/api/report/terminalRevenueSummary";
            public const string PilotPaySlip = "/api/driver/searchpilotpaySlip";

            //GetHireTravelled
            public const string HireTravelled = "/api/report/HireTravelled";

            //GetHireTravelledPass
            public const string HireTravelledPass = "/api/report/GetAllHirePassengers/{0}";

            //dashboard reports
            public const string SalesSummaryUrl = "/api/report/getsalessummary";
            public const string BookingSummaryUrl = "/api/report/getbookingsummary";

            //trip management
            public const string InboundTrips = "/api/journey/JourneysIncoming";
            public const string OutboundTrips = "/api/journey/JourneysOutgoing";
            public const string ReceiveTrips = "/api/journey/receive";
            public const string ApproveTrips = "/api/journey/approve";
            public const string BlowJourneys = "/api/Journey/blowjourneys";


            //manifest management
            public const string ManifestByVehicleTrip = "/api/vehicletripregistration/GetByVehicleTripId";
            public const string ManifestByVehicleTripWithoutDispatch = "/api/vehicletripregistration/GetByVehicleTripIdWithoutManifest";

            //Manifest expensis

            //public const string GetTripExpenses = "/api/manifest/GetManifestExpenses";
            //public const string GetTripExpenses2 = "/api/manifest/GetTripExpenses2";
            public const string GetManifestExpenses = "/api/manifest/GetManifestExpenses";
            public const string GetManifestExpenses2 = "/api/manifest/GetManifestExpenses2";


            //bookings and trips
            public const string GetVehicleTripsByDriverCode = "/api/vehicletripregistration/vehicletripbydrivercode";

            //for franchise            //for franchise
            public const string Franchises = "/api/franchize/get";
            public const string Franchise = "/api/franchize";
            public const string FranchiseGet = "/api/franchize/get/{0}";
            public const string FranchiseDelete = "/api/franchize/delete/{0}";
            public const string FranchiseCreate = "/api/franchize/add";
            public const string FranchiseUpdate = "/api/franchize/update/{0}";

            //for blowing
            //public const string BlowVehicle = "/api/blowtrip/add";
            public const string Passport = "/api/PassportType/GetPassportTypeAsync";
            public const string GetPassportTypeById = "/api/passporttype/getbyrouteandid";
            public const string PassportTypeGet = "/api/passporttype/get/{0}";

            //for MTU Reports and Journey
            public const string AddReport = "/api/mtureport/add";
            public const string SearchReport = "/api/mtureport/getallreport";
            public const string GetMTUById = "/api/mtureport/get/{0}";
            public const string Transload = "/api/Journey/transloadbus";
            public const string GetTransload = "/api/Journey/GetTransloadedVehicles";
            public const string GetJourneyManagementByVehicleTripRegistrationId = "/api/Journey/GetJourneyManagementByVehicleTripRegistrationId/{0}";
            public const string Workshop = "/api/mtureport/workshop";
            //fleet History

            public const string GetFleetHistory = "/api/vehicletripregistration/getfleethistory";

            //Hire
            public const string HireABus = "/api/hirebus";

            //HirePassenger
            public const string HirePassenger = "/api/HirePassenger";

            //Agents
            public const string Agents = "api/agents";


            //AgentsTransaction
            public const string AgentTrans = "api/getAgentTransaction";

            //AgentLocation
            public const string AgentLocation = "api/agentlocation";

            //AgentCommission
            public const string AgentCommission = "api/agentcommission";

            //AgentReport
            public const string AgentReport = "api/agentreport";

            //Workshop
            public const string WorkshopBus = "api/workshop";
            public const string WorkshopBusGet = "api/workshop/Get";
            public const string WorkshopBusPostSearchModel = "api/workshop/PostSearchModel";
            public const string WorkshopBusCreate = "api/workshop/add";
            public const string WorkshopBusRelease = "api/workshop/release/{0}/{1}/{2}";

            //General Transaction
            public const string AddGeneralTransaction = "/api/GeneralTransaction/Add";
            public const string GetAllGeneralTransaction = "/api/GeneralTransaction/GetAll";
            public const string GetGeneralTransactionById = "/api/GeneralTransaction/GetTransaction/{0}";
            public const string EditGeneralTransaction = "/api/GeneralTransaction/Edit/{0}";
            public const string DeactivateGeneralTransaction = "/api/GeneralTransaction/Deactivate/{0}";

            //Company Info
            public const string CompanyInfo = "/api/CompanyInfo/get";
            public const string CompanyInfoGet = "/api/CompanyInfo/get/{0}";
            public const string CompanyInfoCreate = "/api/companyinfo/add";
            public const string CompanyInfoUpdate = "/api/CompanyInfo/update/{0}";
            public const string CompanyInfoDelete = "/api/CompanyInfo/Delete/{0}";

            //INVENTORY Items
            public const string InventorySetup = "/api/InventorySetup/GetAllInventoryItems";
            public const string InventorySetupGet = "/api/InventorySetup/GetInventoryItem/{0}";
            public const string InventorySetupCreate = "/api/InventorySetup/AddInventoryItem";
            public const string InventorySetupUpdate = "/api/InventorySetup/UpdateInventoryItem/{0}";
            public const string InventorySetupDelete = "/api/InventorySetup/DeleteInventoryItem/{0}";

            //Inventory Category
            public const string InventorySetupCategory = "/api/InventorySetup/GetAllItemCategories";
            public const string InventorySetupCategoryGet = "/api/InventorySetup/GetItemCategory/{0}";
            public const string InventorySetupCategoryCreate = "/api/InventorySetup/AddItemCategory";
            public const string InventorySetupCategoryUpdate = "/api/InventorySetup/UpdateItemCategory/{0}";
            public const string InventorySetupCategoryDelete = "/api/InventorySetup/DeleteItemCategory/{0}";

            //Inventory Item Type
            public const string InventorySetupItemType = "/api/InventorySetup/GetAllItemTypes";
            public const string InventorySetupItemTypeGet = "/api/InventorySetup/GetItemType/{0}";
            public const string InventorySetupItemTypeCreate = "/api/InventorySetup/AddItemType";
            public const string InventorySetupItemTypeUpdate = "/api/InventorySetup/UpdateItemType/{0}";
            public const string InventorySetupItemTypeDelete = "/api/InventorySetup/DeleteItemType/{0}";

            //Inventory Attributes
            public const string InventorySetupAttributes = "/api/InventorySetup/GetAllAttributes";
            public const string InventorySetupAttributesGet = "/api/InventorySetup/GetAttribute/{0}";
            public const string InventorySetupAttributesCreate = "/api/InventorySetup/AddAttribute";
            public const string InventorySetupAttributesUpdate = "/api/InventorySetup/UpdateAttribute/{0}";
            public const string InventorySetupAttributesDelete = "/api/InventorySetup/DeleteAttribute/{0}";

            //Inventory Family
            public const string InventorySetupFamilies = "/api/InventorySetup/GetAllItemFamilies";
            public const string InventorySetupFamiliesGet = "/api/InventorySetup/GetItemFamily/{0}";
            public const string InventorySetupFamiliesCreate = "/api/InventorySetup/AddItemFamily";
            public const string InventorySetupFamiliesUpdate = "/api/InventorySetup/UpdateItemFamily/{0}";
            public const string InventorySetupFamiliesDelete = "/api/InventorySetup/DeleteItemFamily/{0}";

            //Inventory Warehouse
            public const string InventorySetupWarehouse = "/api/InventorySetup/GetAllWarehouses";
            public const string InventorySetupWarehouseGet = "/api/InventorySetup/GetWarehouse/{0}";
            public const string InventorySetupWarehouseCreate = "/api/InventorySetup/AddWarehouse";
            public const string InventorySetupWarehouseUpdate = "/api/InventorySetup/UpdateWarehouse/{0}";
            public const string InventorySetupWarehouseDelete = "/api/InventorySetup/DeleteWarehouse/{0}";

            //Inventory WarehouseBin
            public const string InventorySetupWarehouseBin = "/api/InventorySetup/GetAllWarehouseBins";
            public const string InventorySetupWarehouseBinGet = "/api/InventorySetup/GetWarehouseBin/{0}";
            public const string InventorySetupWarehouseBinCreate = "/api/InventorySetup/AddWarehouseBin";
            public const string InventorySetupWarehouseBinUpdate = "/api/InventorySetup/UpdateWarehouseBin/{0}";
            public const string InventorySetupWarehouseBinDelete = "/api/InventorySetup/DeleteWarehouseBin/{0}";
            public const string GetBinByWarehouse = "api/InventorySetup/GetBinByWarehouseId/{0}";

            //Inventory WarehouseBinType
            public const string InventorySetupWarehouseBinType = "/api/InventorySetup/GetAllWarehouseBinTypes";
            public const string InventorySetupWarehouseBinTypeGet = "/api/InventorySetup/GetWarehouseBinType/{0}";
            public const string InventorySetupWarehouseBinTypeCreate = "/api/InventorySetup/AddWarehouseBinType";
            public const string InventorySetupWarehouseBinTypeUpdate = "/api/InventorySetup/UpdateWarehouseBinType/{0}";
            public const string InventorySetupWarehouseBinTypeDelete = "/api/InventorySetup/DeleteWarehouseBinType/{0}";

            //Inventory AdjustmentType
            public const string InventorySetupAdjustmentType = "/api/InventorySetup/GetAllInventoryAdjustmentTypes";
            public const string InventorySetupAdjustmentTypeGet = "/api/InventorySetup/GetInventoryAdjustmentType/{0}";
            public const string InventorySetupAdjustmentTypeCreate = "/api/InventorySetup/AddInventoryAdjustmentType";
            public const string InventorySetupAdjustmentTypeUpdate = "/api/InventorySetup/UpdateInventoryAdjustmentType/{0}";
            public const string InventorySetupAdjustmentTypeDelete = "/api/InventorySetup/DeleteInventoryAdjustmentType/{0}";

            //Next Number
            public const string InventorySetupNextNumber = "/api/InventorySetup/GetAllNextNumbers";
            public const string InventorySetupNextNumberGet = "/api/InventorySetup/GetNextNumber/{0}";
            public const string InventorySetupNextNumberCreate = "/api/InventorySetup/AddNextNumber";
            public const string InventorySetupNextNumberUpdate = "/api/InventorySetup/UpdateNextNumber/{0}";
            public const string InventorySetupNextNumberDelete = "/api/InventorySetup/DeleteNextNumber/{0}";

            //LedgerChartOfAccounts
            public const string InventorySetupGeneralLedger = "/api/InventorySetup/GetAllLedgerChartOfAccounts";
            public const string InventorySetupGeneralLedgerGet = "/api/InventorySetup/GetLedgerChartOfAccount/{0}";
            public const string InventorySetupGeneralLedgerCreate = "/api/InventorySetup/AddLedgerChartOfAccount";
            public const string InventorySetupGeneralLedgerUpdate = "/api/InventorySetup/UpdateLedgerChartOfAccount/{0}";
            public const string InventorySetupGeneralLedgerrDelete = "/api/InventorySetup/DeleteLedgerChartOfAccount/{0}";

            // Vendor Information
            public const string VendorInfos = "/api/VendorInformation/GetAllVendorInfos";
            public const string VendorInfoGet = "/api/VendorInformation/GetVendorInfo/{0}";
            public const string VendorInfoCreate = "/api/VendorInformation/AddVendorInfo";
            public const string VendorInfoUpdate = "/api/VendorInformation/UpdateVendorInfo/{0}";
            public const string VendorInfoDelete = "/api/VendorInformation/DeleteVendorInfo/{0}";

            // Vendor Types
            public const string VendorTypes = "/api/VendorTypes/GetAllVendorTypes";
            public const string VendorTypeGet = "/api/VendorTypes/GetVendorType/{0}";
            public const string VendorTypeCreate = "/api/VendorTypes/AddVendorType";
            public const string VendorTypeUpdate = "/api/VendorTypes/UpdateVendorType/{0}";
            public const string VendorTypeDelete = "/api/VendorTypes/DeleteVendorType/{0}";

            // Account Statuses
            public const string AccountStats = "/api/AccountStat/GetAllAccountStat";
            public const string AccountStatGet = "/api/AccountStat/GetAccountStat/{0}";
            public const string AccountStatCreate = "/api/AccountStat/AddAccountStat";
            public const string AccountStatUpdate = "/api/AccountStat/UpdateAccountStat/{0}";
            public const string AccountStatDelete = "/api/AccountStat/DeleteAccountStat/{0}";

            // Customer Information
            public const string CustomerInfos = "/api/CustomerInformation/Get";
            public const string CustomerInfoGet = "/api/CustomerInformation/GetCustomer/{0}";
            public const string CustomerInfoCreate = "/api/CustomerInformation/Add";
            public const string CustomerInfoUpdate = "/api/CustomerInformation/UpdateCustomer/{0}";
            public const string CustomerInfoDelete = "/api/CustomerInformation/DeleteCustomer/{0}";

            // Customer Types
            public const string CustomerTypes = "/api/CustomerTypes/GetAllCustomerTypes";
            public const string CustomerTypeGet = "/api/CustomerTypes/GetCustomerType/{0}";
            public const string CustomerTypeCreate = "/api/CustomerTypes/AddCustomerType";
            public const string CustomerTypeUpdate = "/api/CustomerTypes/UpdateCustomerType/{0}";
            public const string CustomerTypeDelete = "/api/CustomerTypes/DeleteCustomerType/{0}";


            // Inventory Transaction
            public const string ReceivedHeader = "/api/InventoryTransactions/GetAllHeader";
            public const string ReceivedHeaderGet = "/api/InventoryTransactions/GetHeader/{0}";
            public const string ReceivedHeaderCreate = "/api/InventoryTransactions/AddHeader";
            public const string ReceivedHeaderUpdate = "/api/InventoryTransactions/UpdateHeader/{0}";
            public const string ReceivedHeaderDelete = "/api/InventoryTransactions/DeleteHeader/{0}";

            public const string InvReceive = "/api/InventoryTransactions/InvReceive/{0}";
            public const string InvRecVerify = "/api/InventoryTransactions/InvRecVerify/{0}";
            public const string InvRecApprove = "/api/InventoryTransactions/InvRecApprove/{0}";
            public const string InvRecReturn = "/api/InventoryTransactions/InvRecReturn/{0}";

            public const string ReceivedDetail = "/api/InventoryTransactions/GetAllDetail";
            public const string ReceivedDetailGet = "/api/InventoryTransactions/GetDetail/{0}";
            public const string ReceivedDetailCreate = "/api/InventoryTransactions/AddDetail";
            public const string ReceivedDetailUpdate = "/api/InventoryTransactions/UpdateDetail/{0}";
            public const string ReceivedDetailDelete = "/api/InventoryTransactions/DeleteDetail/{0}";



            // Inventory Requisition 
            public const string RequisitionHeader = "/api/InventoryIssue/GetAllHeader";
            public const string RequestHeader = "/api/InventoryIssue/GetAllRequestHeader";
            public const string RequisitionHeaderGet = "/api/InventoryIssue/GetHeader/{0}";
            public const string RequisitionHeaderCreate = "/api/InventoryIssue/AddHeader";
            public const string RequisitionHeaderUpdate = "/api/InventoryIssue/UpdateHeader/{0}";
            public const string RequisitionHeaderDelete = "/api/InventoryIssue/DeleteHeader/{0}";

            public const string InvRequisition = "/api/InventoryIssue/InvRequisition/{0}";
            public const string InvReqVerify = "/api/InventoryIssue/InvReqVerify/{0}";
            public const string InvReqApprove = "/api/InventoryIssue/InvReqApprove/{0}";
            public const string InvReqReturn = "/api/InventoryIssue/InvReqReturn/{0}";

            public const string RequisitionDetail = "/api/InventoryIssue/GetAllDetail";
            public const string RequisitionDetailGet = "/api/InventoryIssue/GetDetail/{0}";
            public const string RequisitionDetailCreate = "/api/InventoryIssue/AddDetail";
            public const string RequisitionDetailUpdate = "/api/InventoryIssue/UpdateDetail/{0}";
            public const string RequisitionDetailDelete = "/api/InventoryIssue/DeleteDetail/{0}";


            // Inventory Transfer
            public const string TransferHeader = "/api/InventoryTransfer/GetAllHeader";
            public const string TransferHeaderGet = "/api/InventoryTransfer/GetHeader/{0}";
            public const string TransferHeaderCreate = "/api/InventoryTransfer/AddHeader";
            public const string TransferHeaderUpdate = "/api/InventoryTransfer/UpdateHeader/{0}";
            public const string TransferHeaderDelete = "/api/InventoryTransfer/DeleteHeader/{0}";

            public const string InvTransfer = "/api/InventoryTransfer/InvTransfer/{0}";
            public const string InvTransVerify = "/api/InventoryTransfer/InvTransVerify/{0}";
            public const string InvTransApprove = "/api/InventoryTransfer/InvTransApprove/{0}";
            public const string InvTransReturn = "/api/InventoryTransfer/InvTransReturn/{0}";

            public const string TransferDetail = "/api/InventoryTransfer/GetAllDetail";
            public const string TransferDetailGet = "/api/InventoryTransfer/GetDetail/{0}";
            public const string TransferDetailCreate = "/api/InventoryTransfer/AddDetail";
            public const string TransferDetailUpdate = "/api/InventoryTransfer/UpdateDetail/{0}";
            public const string TransferDetailDelete = "/api/InventoryTransfer/DeleteDetail/{0}";


            // Inventory Adjustment
            public const string AdjustmentHeader = "/api/InventoryAdjustment/GetAllHeader";
            public const string AdjustmentHeaderGet = "/api/InventoryAdjustment/GetHeader/{0}";
            public const string AdjustmentHeaderCreate = "/api/InventoryAdjustment/AddHeader";
            public const string AdjustmentHeaderUpdate = "/api/InventoryAdjustment/UpdateHeader/{0}";
            public const string AdjustmentHeaderDelete = "/api/InventoryAdjustment/DeleteHeader/{0}";

            public const string InvAdjustment = "/api/InventoryAdjustment/InvAdjustment/{0}";
            public const string InvAdjVerify = "/api/InventoryAdjustment/InvAdjVerify/{0}";
            public const string InvAdjApprove = "/api/InventoryAdjustment/InvAdjApprove/{0}";
            public const string InvAdjReturn = "/api/InventoryAdjustment/InvAdjReturn/{0}";

            public const string AdjustmentDetail = "/api/InventoryAdjustment/GetAllDetail";
            public const string AdjustmentDetailGet = "/api/InventoryAdjustment/GetDetail/{0}";
            public const string AdjustmentDetailCreate = "/api/InventoryAdjustment/AddDetail";
            public const string AdjustmentDetailUpdate = "/api/InventoryAdjustment/UpdateDetail/{0}";
            public const string AdjustmentDetailDelete = "/api/InventoryAdjustment/DeleteDetail/{0}";


            // Inventory Adjustment
            public const string InventoryByWarehouse = "/api/InventoryByWarehouse/GetAll";
            public const string InventoryByWarehouseGet = "/api/InventoryByWarehouse/Get/{0}";
            public const string InventoryByWarehouseCreate = "/api/InventoryByWarehouse/Add";
            public const string InventoryByWarehouseUpdate = "/api/InventoryByWarehouse/Update/{0}";
            public const string InventoryByWarehouseDelete = "/api/InventoryByWarehouse/Delete/{0}";

            //VetBundles
            public const string VetBundles = "/api/VetSetups/Get";
            public const string VetBundlesCreate = "/api/VetSetups/Add";
            public const string VetBundlesGet = "/api/VetSetups/Get/{0}";
            public const string VetBundlesUpdate = "/api/VetSetups/Update/{0}";
            public const string VetBundlesDelete = "/api/VetSetups/Delete/{0}";

            public const string VetterTransactions = "/api/VetTransact/GetVettrans";
            public const string VetterTransactionsGet = "/api/VetTransact/GetVettrans/{0}";
            public const string UpdateVetterTransactions = "/api/VetTransact/UpdateVettrans/{0}";

            //Feedback
            public const string GetClientComplain = "/api/feedback/GetAllFeedBacks";
            public const string FeedBackResponses = "/api/feedback/GetAllFeedBackResp/{0}";

            //VetteeList
            public const string VetteeList = "/api/VetTransact/GetVettee";
            public const string VetteeCreate = "/api/VetTransact/AddVettees";
            public const string VetteeGet = "/api/VetTransact/GetVettee/{0}";
            public const string VetteeUpdate = "/api/VetTransact/UpdateVettee/{0}";
            //public const string UpdateVettersForm = "/api/VetTransact/UpdateVettersForm/{0}";
            public const string UpdateVettersForm = "/api/VetTransact/UpdateVettersForm";
            public const string VetteeDelete = "/api/VetTransact/DeleteVettees/{0}";

            //VetService
            public const string VetService = "/api/VetSetups/GetAllVetService";
            public const string VetServiceCreate = "/api/VetSetups/AddVetService";
            public const string VetServiceGet = "/api/VetSetups/GetVetService/{0}";
            public const string VetServiceUpdate = "/api/VetSetups/UpdateVetService/{0}";
            public const string VetServiceDelete = "/api/VetSetups/DeleteVetService/{0}";

            //Wallets and Transactions
            public const string GetWalletByUserId = "/api/Payment/GetUserWalletTrans";
            public const string GetAllWalletTrans = "/api/Payment/GetAllWalletTrans";
            public const string VetterActionTo = "/api/VetTransact/VetterActionTo";
            public const string WalletTransUpdate = "/api/Payment/UpdateWalletTrans/{0}";
            public const string WalletTransCreate = "/api/Payment/AddWalletTrans";
            public const string GetUserWalletTrans = "/api/Payment/GetUserWalletTrans/{0}";

            // Invoice Header
            public const string InvoiceHeader = "/api/Invoice/GetAllHeader";
            public const string InvoiceHeaderGet = "/api/Invoice/GetHeader/{0}";
            public const string InvoiceHeaderCreate = "/api/Invoice/AddHeader";
            public const string InvoiceHeaderUpdate = "/api/Invoice/UpdateHeader/{0}";
            public const string InvoiceHeaderDelete = "/api/Invoice/DeleteHeader/{0}";


            // Invoice Header
            public const string InvoiceDetail = "/api/Invoice/GetAllDetail";
            public const string InvoiceDetailGet = "/api/Invoice/GetDetail/{0}";
            public const string InvoiceDetailCreate = "/api/Invoice/AddDetail";
            public const string InvoiceDetailUpdate = "/api/Invoice/UpdateDetail/{0}";
            public const string InvoiceDetailDelete = "/api/Invoice/DeleteDetail/{0}";

            //Purchase
            public const string PurchaseHeaders = "/api/Purchase/GetAllHeader";
            public const string PurchaseHeader = "/api/Purchase/GetHeader/{0}";
            public const string PurchaseHeaderDelete = "/api/Purchase/DeleteHeader/{0}";
            public const string PurchaseHeaderCreate = "/api/Purchase/AddHeader";
            public const string PurchaseDetailCreate = "/​api/Purchase/AddDetail";
            public const string PurchaseHeaderGet = "/api/Purchase/GetHeader/{0}";
            public const string PurchaseDetail = "/api/Purchase/GetDetail";
            public const string PurchaseHeaderUpdate = "/api/Purchase/UpdateHeader/{0}";

            //Bill Informations
            public const string BillHeader = "/api/Purchase/GetAllHeader";
            public const string BillHeaderById = "/api/Purchase/GetHeader/{0}";
            public const string BillHeaderCreate = "/api/Purchase/AddHeader";
            public const string BillHeaderUpdate = "/api/Purchase/UpdateHeader/{0}";
            public const string BillHeaderGet = "/api/Purchase/GetHeader/{0}";
            public const string BillHeaderDelete = "/api/Purchase/DeleteHeader/{0}";

            public const string BillDetailCreate = "/api/Purchase/AddDetail";
            public const string BillDetailGett = "/api/Purchase/GetDetail/{0}";
            public const string BillDetailGet = "/api/Purchase/GetDetailByHeader/{0}";
            public const string BillDetailUpdate = "/api/Purchase/UpdateDetail/{0}";

            //Account - ReceiptHeader
            public const string ReceiptHeader = "/api/Receipt/GetReceiptHeader";
            public const string ReceiptHeaderCreate = "/api/Receipt/AddReceiptHeader";
            public const string ReceiptHeaderGet = "/api/Receipt/GetReceiptHeader/{0}";
            public const string ReceiptHeaderUpdate = "/api/Receipt/UpdateReceiptHeader/{0}";
            public const string ReceiptHeaderDelete = "/api/Receipt/DeleteReceiptHeader/{0}";

            //Account - ReceiptDetail

            public const string ReceiptDetail = "/api/Receipt/GetReceiptDetail";
            public const string ReceiptDetailCreate = "/api/Receipt/AddReceiptDetail";
            public const string ReceiptDetailGet = "/api/Receipt/GetReceiptDetail/{0}";
            public const string ReceiptDetailUpdate = "/api/Receipt/UpdateReceiptDetail/{0}";
            public const string ReceiptDetailDelete = "/api/Receipt/DeleteReceiptDetail/{0}";

        }




    }
}