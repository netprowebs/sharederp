﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.Collections.Generic;
using static SharedErp.Utils.Constants;

namespace SharedErp.Utils.Alerts
{
    public static class AlertExtensions
    {
        private const string Alerts = "_Alerts";
        public static IDictionary<string, Alert> GetAlerts(this ITempDataDictionary tempData)
        {
            return tempData.Get<Dictionary<string, Alert>>(Alerts) ?? new Dictionary<string, Alert>();
        }

        public static void SetAlert(this ITempDataDictionary tempData, Alert alert)
        {
            var alerts = tempData.Get<Dictionary<string, Alert>>(Alerts) ?? new Dictionary<string, Alert>();

            if (!alerts.ContainsKey(Keys.Alerts))
            {
                alerts.Add(Keys.Alerts, alert);
                tempData.Put(Alerts, alerts);
                return;
            }

            alerts[Keys.Alerts] = alert;
            tempData.Put(Alerts, alerts);
        }

        public static IActionResult WithSuccess(this IActionResult result, string message)
        {
            return new AlertDecoratorResult(result, "alert-success", message);
        }
        public static IActionResult WithInfo(this IActionResult result, string message)
        {
            return new AlertDecoratorResult(result, "alert-info", message);
        }
        public static IActionResult WithWarning(this IActionResult result, string message)
        {
            return new AlertDecoratorResult(result, "alert-warning", message);
        }
        public static IActionResult WithError(this IActionResult result, string message)
        {
            return new AlertDecoratorResult(result, "alert-danger", message);
        }
    }
}
