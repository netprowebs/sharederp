﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.Threading.Tasks;

namespace SharedErp.Utils.Alerts
{
    public class AlertDecoratorResult : IKeepTempDataResult
    {
        public IActionResult InnerResult { get; set; }
        public string AlertClass { get; set; }
        public string Message { get; set; }
        public AlertDecoratorResult(IActionResult innerResult, string alertClass, string message)
        {
            InnerResult = innerResult;
            AlertClass = alertClass;
            Message = message;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            var factory = context.HttpContext.RequestServices.GetService(typeof(ITempDataDictionaryFactory)) as ITempDataDictionaryFactory;
            var tempData = factory?.GetTempData(context.HttpContext);

            tempData?.SetAlert(new Alert(AlertClass, Message));

            await InnerResult.ExecuteResultAsync(context);
        }
    }
}
