﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharedErp.Utils
{
    public static class CollectionExtensions
    {
        public static string ToDelimitedString<T>(this IEnumerable<T> items, string delimeter = ",")
        {
            var list = items.Select(item => Convert.ToString(item)).ToList();

            return 
                list.Count <= 1
                ? $"{list.FirstOrDefault()}"
                : list.Aggregate((previous, next) => $"{previous}{delimeter}{next}");
        }

        //public static async void PopulateAssociations(this List<Vehicle> vehicles, List<CaptainAssociation> associations)
        //{

        //    if (vehicles == null || vehicles.Count <= 0)
        //        return;

        //    if (associations == null || associations.Count <= 0)
        //        return;



        //}

        //public static string GetAssignmentString(this Vehicle vehicle)
        //{
        //    if (vehicle == null)
        //        return string.Empty;


        //    if (vehicle.VehicleAssignment == null)
        //        return string.Empty;


        //    return vehicle.VehicleAssignment.ToString();
        //}
        public static string SeperateWords(this string str)
        {

            if (string.IsNullOrWhiteSpace(str))
                return str;

            string output = "";
            char[] chars = str.ToCharArray();

            for (int i = 0; i < chars.Length; i++)
            {
                if (i == chars.Length - 1 || i == 0 || Char.IsWhiteSpace(chars[i]))
                {
                    output += chars[i];
                    continue;
                }

                if (char.IsUpper(chars[i]) && Char.IsLower(chars[i - 1]))
                    output += " " + chars[i];
                else
                    output += chars[i];
            }

            return output;
        }
    }
}
