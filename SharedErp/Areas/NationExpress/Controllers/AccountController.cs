﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Models.Enum;
using Microsoft.AspNetCore.Http;

namespace SharedErp.Areas.NationExpress.Controllers
{
    [Area("NationExpress")]
    public class AccountController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public AccountController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Dashboards()
        {
            //TempData["SectorId"] = SectorsName.CargoAndLogistics.ToString();

            HttpContext.Session.SetString("SectorId", SectorsName.CargoAndLogistics.ToString());

            return View();
        }


    }
}
