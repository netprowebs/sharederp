﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Security.Models;
using System.Collections.Generic;
using SharedErp.Areas.Admin.Models;
using Newtonsoft.Json;

namespace SharedErp.Areas.Security.Controllers
{
    [Area("Admin")]
    public class FeedbacksController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public FeedbacksController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<IActionResult> Index(int id)
        {

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.GetClientComplain);
            var url2 = string.Format(Constants.ClientRoutes.FeedBackResponses,1);

            var result = await client.GetAsync<List<FeedBackDTO>>(url);           
            var model = new ComplaintAndResponseDTO();
            //model.feedBack = result.Object;


            var result2 = await client.GetAsync<List<FeedBackResponseDTO>>(url2);
            model.response = result2.Object;

            ViewData["Complaint"] = result.Object;
            ViewData["Response"] = result2.Object;

            //if (model != null)
            //{
            //    return View(model);
            //}

            return View();
        }




        [HttpGet]
        public async Task<IActionResult> GetFeedBackResponseById(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.GetClientComplain);
            var url2 = string.Format(Constants.ClientRoutes.FeedBackResponses);

            var result = await client.GetAsync<List<FeedBackDTO>>(url);
            var result2 = await client.GetAsync<List<FeedBackResponseDTO>>(url2);

            var model = new ComplaintAndResponseDTO();

            model.feedBack = result.Object;
            model.response = result2.Object;


            //var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }

    }
}
