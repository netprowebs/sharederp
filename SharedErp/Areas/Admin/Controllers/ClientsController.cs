﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Security.Models;
using SharedErp.Areas.Admin.Models;
using System;
using SHB.Core.Entities.Enums;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;

namespace SharedErp.Areas.Security.Controllers
{
    [Area("Admin")]
    public class ClientsController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public ClientsController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index(string Stext, string fromDate, string toDate)
        {
            TempData["vStext"] = Stext;
            TempData["vdatefrom"] = fromDate;
            TempData["vdateto"] = toDate;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetData(string Str, string fromDate, string toDate, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.CustomerInfos,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<CustomerInformationDTO>>(url);

            var pagedData = result.Object;

            DataTablesResponse response = null;

            var model = new DateModel
            {
                StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-60)),
                EndDate = Convert.ToDateTime(DateTime.Now).AddHours(23)
            };

            if (!string.IsNullOrEmpty(Str) && Str != "undefined")
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items.Where(x => x.CustomerEmail == Str));
            }
            else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined")
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items.Where(x => x.CreatedTime >= Convert.ToDateTime(fromDate) && x.CreatedTime <= Convert.ToDateTime(toDate)));
            }
            else
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items);
                //pagedData.Items.Where(x => x.CreatedTime >= model.StartDate && x.CreatedTime <= model.EndDate));
                //pagedData.Items.Where(x => x.CreatedTime >= Convert.ToDateTime("2021-06-27") && x.CreatedTime <= Convert.ToDateTime("2021-08-27")));
            }
            return new DataTablesJsonResult(response, true);
        }



        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
  

            ViewBag.EndDate = DateTime.Now;
            ViewBag.StartDate = DateTime.Now.Date;

            return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(IFormCollection data, CustomerInformationDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                model.Password = "123456";
                model.LocationId = model.CustomerCity;
                var tnid = User.FindFirst("preferred_username")?.Value;
                model.TenantId = Convert.ToInt32(tnid);
                model.DeviceType = DeviceType.AdminWeb;
                model.LocationId = model.CustomerCity;

                List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
                if (model.FilePhoto != null)
                {
                   var filedata = await InfoForImage(model.FilePhoto, "Photo");
                    imageFileDTO.Add(filedata);
                    model.Photo = "Photo";
                }
                if (model.FileIdentityPhoto != null)
                {
                    var filedatab = await InfoForImage(model.FileIdentityPhoto, "IdentityPhoto");
                    imageFileDTO.Add(filedatab);
                    model.IdentityPhoto = "IdentityPhoto";
                }

                model.FilePhoto = null;
                model.FileIdentityPhoto = null;
                model.PictureList = imageFileDTO;

                var result = await client.PostAsJsonAsync<CustomerInformationDTO, bool>(Constants.ClientRoutes.CustomerInfoCreate, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            return View(model);
        }
        public async Task<ImageFileDTO> InfoForImage(IFormFile file,string txt)
        {
                    IFormFile VetUpload = file;
                    ImageFileDTO imageFileD = new ImageFileDTO();

                    byte[] datas;
                    using (var br = new BinaryReader(VetUpload.OpenReadStream()))
                        datas = br.ReadBytes((int)VetUpload.OpenReadStream().Length);
        
                    imageFileD.ContentType = VetUpload.ContentType;
                    imageFileD.fileName = VetUpload.FileName;
                    imageFileD.FilenameWithOutExt = Path.GetFileNameWithoutExtension(VetUpload.FileName);
                    imageFileD.data = datas;
                    imageFileD.ImgContent = txt;

            return imageFileD; 
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.CustomerInfoGet, id);
            var result = await client.GetAsync<CustomerInformationDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, CustomerInformationDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
                if (model.FilePhoto != null)
                {
                    var filedata = await InfoForImage(model.FilePhoto, "Photo");
                    imageFileDTO.Add(filedata);
                    model.Photo = "Photo";
                }
                if (model.FileIdentityPhoto != null)
                {
                    var filedatab = await InfoForImage(model.FileIdentityPhoto, "IdentityPhoto");
                    imageFileDTO.Add(filedatab);
                    model.IdentityPhoto = "IdentityPhoto";
                }

                model.FilePhoto = null;
                model.FileIdentityPhoto = null;
                model.PictureList = imageFileDTO;
                var url = string.Format(Constants.ClientRoutes.CustomerInfoUpdate, id);

                var result = await client.PutAsync<CustomerInformationDTO, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.CustomerInfoDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }


    }
}
