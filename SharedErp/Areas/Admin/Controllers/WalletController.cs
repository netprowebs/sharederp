﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Security.Models;
using SharedErp.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using SharedErp.Utils.Alerts;
using Microsoft.AspNetCore.Http;

namespace SharedErp.Areas.Security.Controllers
{
    [Area("Admin")]
    public class WalletController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public WalletController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index(string Stext, string fromDate, string toDate)
        {
            TempData["vStext"] = Stext;
            TempData["vdatefrom"] = fromDate;
            TempData["vdateto"] = toDate;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetData(string Str, string fromDate, string toDate, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            IServiceResponse<UserDTO> resp = null;

            var page = 0;
            var url = "";
            IServiceResponse<PagedRecordModel<WalletTransactionDTO>> result = null;
            DataTablesResponse response = null;

            var model = new DateModel
            {
                StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-60)),
                EndDate = Convert.ToDateTime(DateTime.Now).AddHours(23)
            };

            if (!string.IsNullOrEmpty(Str) && Str != "undefined")
            {
                if (Str.Contains("@") )
                {
                    var urlb = string.Format(Constants.ClientRoutes.AccountGetuserbyemail, Str);
                    resp = await client.GetAsync<UserDTO>(urlb);
                    Str = resp.Object.Id.ToString();
                    HttpContext.Session.SetString("WalletUserId", Str.ToString());
                    HttpContext.Session.SetString("WalletId", resp.Object.WalletId1.ToString());
                }

                 page = (request.Start / request.Length) + 1;
                 url = string.Format("{0}/{1}/{2}/{3}/{4}", Constants.ClientRoutes.GetWalletByUserId, Str,
                    page, request.Length, request.Search.Value);

                result = await client.GetAsync<PagedRecordModel<WalletTransactionDTO>>(url);
                var pagedData = result.Object;
               
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items.Where(x => x.UserId == Convert.ToInt32(Str)));
            }
            else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined")
            {
                page = (request.Start / request.Length) + 1;
                url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.GetAllWalletTrans,
                page, request.Length, request.Search.Value);

                result = await client.GetAsync<PagedRecordModel<WalletTransactionDTO>>(url);
                var pagedData = result.Object;
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items.Where(x => x.CreationTime >= Convert.ToDateTime(fromDate) && x.CreationTime <= Convert.ToDateTime(toDate)));
            }
            else
            {
                page = (request.Start / request.Length) + 1;
                url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.GetAllWalletTrans,
                page, request.Length, request.Search.Value);

                result = await client.GetAsync<PagedRecordModel<WalletTransactionDTO>>(url);
                var pagedData = result.Object;
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items);
            }

            if (result.Object.Count == 0)
            {
                ViewBag.WalletBal = 0;
            }
            else
            {
                ViewBag.WalletBal = result.Object.Items.FirstOrDefault().WalletBalance;
                TempData["vWalletBal"] = result.Object.Items.FirstOrDefault().WalletBalance.ToString();
                //ViewData["vvWalletBal"] = "66666";
            }

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> Save(WalletTransactionDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            IServiceResponse<bool> response = null;
            bool item = false;

            if (model.Id == Guid.Empty)
            {
                // Do save to database
                if (model.UserEmail.Contains("@"))
                {
                    var urlb = string.Format(Constants.ClientRoutes.AccountGetuserbyemail, model.UserEmail);
                    var resp = await client.GetAsync<UserDTO>(urlb);

                    model.TransCode = Guid.NewGuid().ToString().Remove(8).ToUpper();
                    model.UserId  = resp.Object.Id;
                    model.WalletId = (int)resp.Object.WalletId1;
                   
                    response = await client.PostAsJsonAsync<WalletTransactionDTO, bool>(Constants.ClientRoutes.WalletTransCreate, model);
                    item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
                }

            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.WalletTransUpdate, model.Id);
                var result = await client.PutAsync<WalletTransactionDTO, bool>(url, model);

                model.UserId  = Convert.ToInt32(HttpContext.Session.GetString("WalletUserId"));
                model.WalletId = Convert.ToInt32(HttpContext.Session.GetString("WalletId"));

                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return Json(new
            {
                data = item
            });
        }
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RouteGet, id);
            var result = await client.GetAsync<RouteDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RouteDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
    }
}
