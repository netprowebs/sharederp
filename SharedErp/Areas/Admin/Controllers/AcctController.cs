﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Security.Models;
using System.Collections.Generic;
using System;

namespace SharedErp.Areas.Security.Controllers
{
    [Area("Admin")]
    public class AcctController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public AcctController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }
        #region Fetch Dropdowns

        [HttpGet]
        public async Task<IActionResult> FetchDetailDropDown(string dropdesc, int id)
        {
            //int Type
            string tenantid = User.FindFirst("preferred_username")?.Value;
            string locationid = User.FindFirst("location")?.Value;
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (dropdesc == "StateList")
            {
                var state = await client.GetAsync<PagedRecordModel<StateModel>>(Constants.ClientRoutes.States);
                var itemState = state?.Object?.Items;
                //ViewBag.StateList = new SelectList(state.Object.Items, "Id", "Name");
                var model = new SelectList(itemState, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "RegionList")
            {
                var RegionList = await client.GetAsync<PagedRecordModel<RegionDTO>>(Constants.ClientRoutes.Regions);
                var itemRegionList = RegionList?.Object?.Items;
                var model = new SelectList(itemRegionList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "TerminalList")
            {
                var TerminalList = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
                var itemTerminalList = TerminalList?.Object?.Items;
                var model = new SelectList(itemTerminalList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "WarehouseBinList")
            {
                var bins = await client.GetAsync<IEnumerable<WarehouseBinDTO>>(string.Format(Constants.ClientRoutes.GetBinByWarehouse, id));

                var binList = bins?.Object;
                var model = new SelectList(binList, "Id", "WarehouseBinName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "RouteList")
            {
                var ItemCategoryList = await client.GetAsync<PagedRecordModel<RouteDTO>>(Constants.ClientRoutes.Routes);
                var itemCategoryList = ItemCategoryList?.Object?.Items;
                var model = new SelectList(itemCategoryList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "ItemFamilyList")
            {
                var ItemFamilyList = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(Constants.ClientRoutes.InventorySetupFamilies);
                var itemFamilyList = ItemFamilyList?.Object?.Items;
                var model = new SelectList(itemFamilyList, "Id", "FamilyDescription");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "InventoryAccountList")
            {
                var InventoryAccountList = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
                var inventoryAccountList = InventoryAccountList?.Object?.Items;
                var model = new SelectList(inventoryAccountList, "Id", "CGLAccountNumber");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "InventoryItemList")
            {
                var InventoryItemList = await client.GetAsync<PagedRecordModel<InventoryItemDTO>>(Constants.ClientRoutes.InventorySetup);
                var inventoryItemList = InventoryItemList?.Object?.Items;
                var model = new SelectList(inventoryItemList, "Id", "ItemName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "EmployeeList")
            {
                var EmployeeList = await client.GetAsync<PagedRecordModel<EmployeeModel>>(Constants.ClientRoutes.Employees);
                var employeeList = EmployeeList?.Object?.Items;
                var model = new SelectList(employeeList, "Id", "FullName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "ClientList")
            {
                var ClientList = await client.GetAsync<PagedRecordModel<CustomerInformationDTO>>(Constants.ClientRoutes.CustomerInfos);
                var clientList = ClientList?.Object?.Items;
                var model = new SelectList(clientList, "Id", "CustomerEmail");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "AllUsersList")
            {
                var EmployeeList = await client.GetAsync<PagedRecordModel<EmployeeModel>>(Constants.ClientRoutes.Employees);
                var employeeList = EmployeeList?.Object?.Items;
                var model = new SelectList(employeeList, "Id", "FullName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "DriverList")
            {
                var EmployeeList = await client.GetAsync<PagedRecordModel<EmployeeModel>>(Constants.ClientRoutes.Employees);
                var employeeList = EmployeeList?.Object?.Items;
                var model = new SelectList(employeeList, "Id", "FullName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "VehicleList")
            {
                var VehicleList = await client.GetAsync<PagedRecordModel<Vehicle>>(Constants.ClientRoutes.Vehicle);
                var vehicleList = VehicleList?.Object?.Items;
                var model = new SelectList(vehicleList, "Id", "RegistrationNumber");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "VehicleModelList")
            {
                var VehicleModelList = await client.GetAsync<PagedRecordModel<VehicleModel>>(Constants.ClientRoutes.VehicleModels);
                var vehicleModelList = VehicleModelList?.Object?.Items;
                var model = new SelectList(vehicleModelList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "RouteListing")
            {
                var RouteList = await client.GetAsync<PagedRecordModel<RouteDTO>>(Constants.ClientRoutes.Routes);
                var routeList = RouteList?.Object?.Items;
                var model = new SelectList(routeList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "CountryListing")
            {
                var CountryList = await client.GetAsync<PagedRecordModel<RouteDTO>>(Constants.ClientRoutes.States);
                var countryList = CountryList?.Object?.Items;
                var model = new SelectList(countryList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "RoleList")
            {
                var RoleList = await client.GetAsync<PagedRecordModel<RoleModel>>(Constants.ClientRoutes.Roles);
                var itemRoleList = RoleList?.Object?.Items.Where(x => x.CompanyInfoId == Convert.ToInt32(tenantid));
                var model = new SelectList(itemRoleList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else
            {
            }
            return null;
        }
        #endregion

        public IActionResult Subscribe()
        {
            return View();
        }

    }
}
