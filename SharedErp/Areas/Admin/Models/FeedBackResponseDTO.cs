﻿using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Admin.Models
{
    public class FeedBackResponseDTO
    {
        public int Id { get; set; }
        public int ComplaintID { get; set; }
        public string EmployeeMessage { get; set; }
        public string ClientMessage { get; set; }
        public DateTime EmployeeDate { get; set; }
        public DateTime ClientDate { get; set; }
        public bool Responded { get; set; }


        // for the header Complain table
        public string FullName { get; set; }
        public string Email { get; set; }
        public ComplaintTypes ComplaintType { get; set; }
        public PriorityLevel PriorityLevel { get; set; }
        public string ComplainReference { get; set; }
        public string ComplainMessage { get; set; }
        public DateTime ComplainTransDate { get; set; }
        public string RepliedMessage { get; set; }
        public string ComplainTime { get; set; }
        public DateTime CreationTime { get; set; }

    }
}
