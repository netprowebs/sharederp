﻿using SharedErp.Models.Enum;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Admin.Models
{
    public class CoreConstants
    {
        public const string DefaultAccount = "administrator@lme.com";
        public const string WebBookingAccount = "web@libmot.com";
        public const string IosBookingAccount = "ios@libmot.com";
        public const string AndroidBookingAccount = "android@libmot.com";
        public const string DateFormat = "dd MMMM, yyyy";
        public const string TimeFormat = "hh:mm tt";
        public const string SystemDateFormat = "dd/MM/yyyy";
    }
}
