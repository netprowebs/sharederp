﻿using SharedErp.Models.Enum;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Admin.Models
{
    public class DateModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? EndDate { get; set; }

        //String DateString = Convert.ToString("07/02/1993");
        //IFormatProvider culture = new CultureInfo("en-US");
        //DateTime dateVal = DateTime.ParseExact(DateString, "MM/dd/yyyy", CultureInfo.InvariantCulture);
        //'yyyy-mm-dd'       
    }
}
