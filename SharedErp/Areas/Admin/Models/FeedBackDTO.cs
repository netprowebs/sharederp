﻿using SharedErp.Models.Enum;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Admin.Models
{
    public class FeedBackDTO
    {
        public int Id { get; set; }
        public int FedbackId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public ComplaintTypes ComplaintType { get; set; }
        public PriorityLevel PriorityLevel { get; set; }
        public string BookingReference { get; set; }
        public string Message { get; set; }
        public DateTime TransDate { get; set; }
        public bool Responded { get; set; }
        public string RepliedMessage { get; set; }
        public string Time { get; set; }
    }
}
