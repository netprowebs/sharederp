﻿namespace SHB.Core.Entities.Enums
{
    public enum CouponType
    {
        Percentage = 0,
        Fixed = 1
    }
}
