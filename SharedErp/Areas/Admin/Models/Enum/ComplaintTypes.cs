﻿using System;

namespace SHB.Core.Entities.Enums
{
    public enum ComplaintTypes
    {
        MissedTrips = 0,
        TicketPayments = 1,
        DriverService = 2,
        BusServices = 3,
        GeneralIssue = 4,
        SuggestionOrAccolades = 5
    }
}
