﻿namespace SHB.Core.Entities.Enums
{
    public enum TerminalType
    {
        Physical = 0,
        Virtual = 1
    }
}
