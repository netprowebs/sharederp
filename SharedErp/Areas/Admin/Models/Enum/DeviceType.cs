﻿namespace SHB.Core.Entities.Enums
{
    public enum DeviceType
    {
        AdminWeb,
        Android,
        iOS,
        Windows,
        Symbian,
        Tizen,
        Website,
    }
}
