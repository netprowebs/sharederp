﻿namespace SHB.Core.Entities.Enums
{
    public enum PaymentStatus
    {
        Pending = 0,
        Paid = 1,
        Declined = 2,
        Approved = 3,
    }
}
