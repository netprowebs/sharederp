﻿using SharedErp.Models.Enum;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Admin.Models
{
    public class ActionDto
    {
        public List<string> modellist { get; set; }
        public VetterStatus ActionType { get; set; }
        public int EmployeeId { get; set; }
    }
}
