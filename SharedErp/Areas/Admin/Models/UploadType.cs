﻿using SharedErp.Models.Enum;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Admin.Models
{
    public enum UploadType
    {
        Image,
        Video
    }
}
