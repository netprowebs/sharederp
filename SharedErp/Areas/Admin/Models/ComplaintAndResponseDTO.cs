﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Admin.Models
{
    public class ComplaintAndResponseDTO
    {
        public List<FeedBackResponseDTO> response { get; set; }
        public List<FeedBackDTO> feedBack { get; set; }
    }
}
