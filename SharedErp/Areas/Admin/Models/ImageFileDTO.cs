﻿using Microsoft.AspNetCore.Http;
using SharedErp.Models.Enum;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Admin.Models
{
    public class ImageFileDTO
    {
        public int Id { get; set; }
        public string FolderName { get; set; }
        public Guid RequisitionID { get; set; }
        public DateTime CreationTime { get; set; }
        public int CreatorUserId { get; set; }
        public string FileReference { get; set; }
        public UploadType UploadType { get; set; }
        public UploadFormat UploadFormat { get; set; }
        public string UploadPath { get; set; }
        public string ImgContent { get; set; }
        public string Description { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public int UserId { get; set; }
        public int TenantId { get; set; }
        public DateTime DeletionTime { get; set; }
        public int DeleterUserId { get; set; }
        public bool IsDeleted { get; set; }
        public int LastModifierUserId { get; set; }
        public DateTime LastModificationTime { get; set; }
        public IFormFile file { get; set; }
        public byte[] data { get; set; }
        public string ContentType { get; set; }
        public string fileName { get; set; }
        public string FilenameWithOutExt { get; set; }
    }
}
