﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class PagedRecordModel<T>
    {
        public int Count { get; set; }
        public int PageCount { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
        public int TotalItemCount { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}
