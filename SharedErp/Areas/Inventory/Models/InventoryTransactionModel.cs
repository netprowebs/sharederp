﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class InventoryTransactionModel
    {
        public IEnumerable<InventoryReceivedDetailModel> InventoryReceivedDetailModel { get; set; }
        public InventoryReceivedHeaderModel InventoryReceivedHeaderModel { get; set; }
    }
}
