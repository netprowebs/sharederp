﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class NextNumberDTO
    {
        public int Id { get; set; }
        public string NextNumberName { get; set; }
        public string NextNumberValue { get; set; }
        public string NextNumberPrefix { get; set; }
        public string NextNumberSeparator { get; set; }
    }
}
