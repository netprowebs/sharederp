﻿using SharedErp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Security.Models
{
    public class TerminalModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Image { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonNo { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public DateTime TerminalStartDate { get; set; }

        public TerminalType TerminalType { get; set; }
        public int StateId { get; set; }
        //public string  State { get; set; }
        public string TerminalCode { get; set; }
        public bool IsNew { get; set; }

        public bool IsCommision { get; set; }
        public decimal OnlineDiscount { get; set; }
        public bool IsOnlineDiscount { get; set; }
        public int? CompanyId { get; set; }
    }

}
