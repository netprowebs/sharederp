﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class ItemTypeDTO
    {
 
        public int Id { get; set; }
        public string ItemTypeName { get; set; }
        public string ItemTypeDescription { get; set; }
        //public ICollection<InventoryItem> InventoryItems { get; set; }
    }
}

