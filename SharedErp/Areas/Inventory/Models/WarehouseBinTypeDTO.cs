﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class WarehouseBinTypeDTO
    {
        public int Id { get; set; }
        public string WarehouseBinTypeName { get; set; }
        public string WarehouseBinTypeDescription { get; set; }
        public int CompanyId { get; set; }
        public bool IsActive { get; set; }
    }
}
