﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class InventoryAdjustmentModel
    {
        public IEnumerable<InventoryAdjustmentDetailDTO> InventoryAdjustmentDetailDTO { get; set; }
        public InventoryAdjustmentHeaderDTO InventoryAdjustmentHeaderDTO { get; set; }
    }
}
