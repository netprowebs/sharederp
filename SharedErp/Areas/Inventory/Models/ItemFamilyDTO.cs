﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class ItemFamilyDTO
    {
        public int Id { get; set; }
        public string ItemFamilyCode { get; set; }
        public string FamilyDescription { get; set; }
        public string FamilyLongDescription { get; set; }
        public string FamilyPictureURL { get; set; }

        //public ICollection<InventoryItem> InventoryItems { get; set; }
        public ICollection<ItemCategoryDTO> ItemCategories { get; set; }
    }
}
