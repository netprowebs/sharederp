﻿using SharedErp.Models.Enum;
using SharedErp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class Vehicle
    {


        public int Id { get; set; }
        public string RegistrationNumber { get; set; }
        public string ChasisNumber { get; set; }
        public string EngineNumber { get; set; }
        public string IMEINumber { get; set; }
        public string Type { get; set; }
         public VehicleStatus VehicleStatus { get; set; }
          public string Status => VehicleStatus.ToString();

        public int VehicleModelId { get; set; }
        public string VehicleModelName { get; set; }
        public VehicleModel VehicleModel { get; set; }

        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        public bool IsOperational { get; set; }

        public int? EmployeeId { get; set; }
        [Required]
         public int? DriverId { get; set; }
        public string DriverName { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public DateTime? LicenseDate { get; set; }
        public DateTime? InsuranceDate { get; set; }
        public string Description { get; set; }
        public DateTime? LicenseExpiration { get; set; }
        public DateTime? InsuranceExpiration { get; set; }
        public DateTime? RoadWorthinessDate { get; set; }
        public DateTime? RoadWorthinessExpiration { get; set; }

        //     public int Id { get; set; }

        //     public string Details { get; set; }
        //     [Required(ErrorMessage = "Please enter a valid registration number.")]
        //     public string RegistrationNumber { get; set; }
        //     public string ChasisNumber { get; set; }
        //     public string EngineNumber { get; set; }
        //     public VehicleStatus VehicleStatus { get; set; }
        //     public string Status => VehicleStatus.ToString();

        //     public DateTime? DateCreated { get; set; }

        //     public int VehicleModelId { get; set; }
        //     public string VehicleModelName { get; set; }

        //     public int? LocationId { get; set; }
        //     public string LocationName { get; set; }
        //     [Required]
        //     public int? DriverId { get; set; }
        //     public string DriverName { get; set; }

        //     public int? FranchizeId { get; set; }
        //     public string FranchiseName { get; set; }
        //     public string DriverNo { get; set; }

        //     [Display(Name = "License Date")]
        //     [DataType(DataType.Date)]
        //     public DateTime? LicenseDate { get; set; }
        //     public string DateLicense => LicenseDate?.ToString(Constants.HumanDateFormat);

        //     [Display(Name = "Insurance Date")]
        //     [DataType(DataType.Date)]
        //     public DateTime? InsuranceDate { get; set; }
        //     public string DateInsurance => InsuranceDate?.ToString(Constants.HumanDateFormat);
        //     public string Description { get; set; }


        ////Below aere Extra columns yet to be added to db

        //     [Display(Name = "LicenseExpiration Date")]
        //     [DataType(DataType.Date)]
        //     public DateTime? LicenseExpiration { get; set; }
        //     public string DateLicenseExpiration => LicenseExpiration?.ToString(Constants.HumanDateFormat);

        //     [Display(Name = "InsuranceExpiration Date")]
        //     [DataType(DataType.Date)]
        //     public DateTime? InsuranceExpiration { get; set; }
        //     public string DateInsuranceExpiration => InsuranceExpiration?.ToString(Constants.HumanDateFormat);

        //     [Display(Name = "RoadWorthinessDate ")]
        //     [DataType(DataType.Date)]
        //     public DateTime? RoadWorthinessDate { get; set; }
        //     public string DateRoadWorthiness => RoadWorthinessDate?.ToString(Constants.HumanDateFormat);

        //     [Display(Name = "RoadWorthinessExpiration ")]
        //     [DataType(DataType.Date)]
        //     public DateTime? RoadWorthinessExpiration { get; set; }
        //     public string DateRoadWorthinessExpiration => RoadWorthinessExpiration?.ToString(Constants.HumanDateFormat);


    }
}
