﻿using System.ComponentModel.DataAnnotations;

namespace SharedErp.Models
{
    public class LoginModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public bool RememberLogin { get; set; }
        public string Location { get; set; }
    }
}
