﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models.Enum
{
    public enum TravelStatus
    {
        Pending = 0,
        Travelled = 1,
        NoShow = 2,
        Rescheduled = 3
    }
}
