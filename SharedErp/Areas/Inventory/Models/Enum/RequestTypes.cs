﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SharedErp.Models.Enum
{
    //[Flags]
    public enum RequestTypes
    {
        [Display(Name = "Stock")] Stock = 1,
        [Display(Name = "Payment")] Payment = 2,
        [Display(Name = "Maintenance")] Maintenance = 3
    }
}
