﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models.Enum
{
    public enum TransactionStatus
    {
        Captured = 0,
        Verified = 1,
        Approved = 2,
        Default = 3
    }
}
