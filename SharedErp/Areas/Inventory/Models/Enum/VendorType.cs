﻿namespace SharedErp.Models.Enum
{
    public enum VendorType
    {
        Inventory = 0,
        Workshop = 1
    }
}
