﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models.Enum
{
    public enum BookingType
    {
        Terminal = 0,
        Advanced = 1,
        Online = 2,
        All = 3,
        BookOnHold = 4,
        Agent = 5
    }


    public enum FareType
    {
        Discount = 0,
        Increase = 1
    }

    public enum FareAdjustmentType
    {
        Value = 0,
        Percentage = 1
    }
    public enum FareParameterType
    {
        Route = 0,
        Terminal = 1
    }

}
