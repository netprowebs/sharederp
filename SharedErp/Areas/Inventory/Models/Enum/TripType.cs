﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models.Enum
{
    public enum TripType
    {
        OneWay = 0,
        Return = 1
    }
}
