﻿using System.ComponentModel.DataAnnotations;

namespace SharedErp.Models.Enum
{
    public enum SectorsName
    {
        [Display(Name = "InventoryAndPOS")] InventoryAndPOS = 1,
        [Display(Name = "CargoAndLogistics")] CargoAndLogistics = 2,
        [Display(Name = "Transport")] Transport = 3,
        [Display(Name = "RealEstate")] RealEstate = 4,
        [Display(Name = "Insurance")] Insurance = 5,
        [Display(Name = "Farm")] Farm = 6,
        [Display(Name = "AccountingAndPayroll")] AccountingAndPayroll = 7,
        [Display(Name = "Education")] Education = 8,
        [Display(Name = "OilAndGas")] OilAndGas = 9,
        [Display(Name = "Security")] Security = 10,
               [Display(Name = "Medical")] Medical = 11
    }
}
