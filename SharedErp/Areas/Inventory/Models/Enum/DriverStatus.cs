﻿namespace SharedErp.Models.Enum
{
    public enum DriverStatus
    {
        Idle = 0,
        OnAJourney = 1,
        Suspended = 2,
        OnLeave = 3,
        Dismissal = 4,
        Decease =5,
        Started = 6
    }
}