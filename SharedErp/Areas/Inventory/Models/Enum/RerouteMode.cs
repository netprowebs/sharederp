﻿namespace SharedErp.Models.Enum
{
    public enum RerouteMode
    {
        Admin = 0,
        Android = 1,
        IOS = 2,
        OnlineWebsite = 3,
        OnlineMobile = 4
    }
}
