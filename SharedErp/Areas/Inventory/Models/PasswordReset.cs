﻿namespace SharedErp.Models
{
    public class PasswordReset
    {
        public string UserNameOrEmail { get; set; }
        public string Code { get; set; }
        public string NewPassword { get; set; }
    }
}
