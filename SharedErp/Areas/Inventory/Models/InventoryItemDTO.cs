﻿using SharedErp.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class InventoryItemDTO
    {
        public int Id { get; set; }
        public string ItemCode { get; set; }
        public bool IsActive { get; set; }
        public int ItemTypeId { get; set; }
        [NotMapped]
        public string ItemTypeName { get; set; }
        public string ItemName { get; set; }
        public string itemDescription { get; set; }
        public int ItemCategoryID { get; set; }
        [NotMapped]
        public string ItemCategoryName { get; set; }
        public int ItemFamilyID { get; set; }
        [NotMapped]
        public string ItemFamilyName { get; set; }
        public int BrandTypeID { get; set; }
        [NotMapped]
        public string BrandTypeName { get; set; }
        public string PictureURL { get; set; }
        public string ItemWeight { get; set; }
        public string ItemUPCCode { get; set; }
        public string ItemColor { get; set; }
        public int ItemDefaultWarehouseID { get; set; }
        public virtual string ItemDefaultWarehouseName { get; set; }
        public int ItemDefaultWarehouseBinID { get; set; }
        public virtual string ItemDefaultWarehouseBinName { get; set; }
        public string ItemUOM { get; set; }
        public int GLItemSalesAccountId { get; set; }
        public string GLItemSalesAccountName { get; set; }
        public int GLItemCOGSAccountId { get; set; }
        public string GLItemCOGSAccountName { get; set; }
        public int GLItemInventoryAccountId { get; set; }
        public string GLItemInventoryAccountName { get; set; }
        public decimal Price { get; set; }
        public string ItemPricingCode { get; set; }
        public string VendorID { get; set; }
        public float ReOrderLevel { get; set; }
        public float ReOrderQty { get; set; }
        public decimal LIFO { get; set; }
        public decimal LIFOValue { get; set; }
        public decimal LIFOCost { get; set; }
        public decimal Average { get; set; }
        public decimal AverageValue { get; set; }
        public decimal AverageCost { get; set; }
        public decimal FIFOFIFOValue { get; set; }
        public decimal FIFOCost { get; set; }
        public decimal Expected { get; set; }
        public decimal ExpectedValue { get; set; }
        public decimal ExpectedCost { get; set; }
        public bool IsSerialLotItem { get; set; }
        public bool AllowPurchaseTrans { get; set; }
        public bool AllowSalesTrans { get; set; }
        public bool AllowInventoryTrans { get; set; }
        public int CompanyId { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
