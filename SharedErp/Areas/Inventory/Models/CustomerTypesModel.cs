﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class CustomerTypesModel
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int CustomerTypeId { get; set; }
        public string CustomerTypeDescription { get; set; }
        public string SalesControlAccount { get; set; }
        public string COSControlAccount { get; set; }
        public string DebtorsControlAccount { get; set; }
        public string CurrencyExchangeGainOrLossAccount { get; set; }
        public string DiscountsAccount { get; set; }
        public string DiscountRate { get; set; }
        public string BillingType { get; set; }
    }
}
