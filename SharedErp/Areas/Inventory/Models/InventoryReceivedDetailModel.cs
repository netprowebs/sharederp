﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class InventoryReceivedDetailModel
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public Guid InventoryReceivedID { get; set; }
        public int? ItemID { get; set; }
        public string Description { get; set; }
        public int RequestedQty { get; set; }
        public int WarehouseID { get; set; }
        public int WarehouseBinID { get; set; }
        public int ToWarehouseID { get; set; }
        public int ToWarehouseBinID { get; set; }
        public int? GLExpenseAccount { get; set; }
        public float ItemValue { get; set; }
        public float ItemCost { get; set; }
        public int? CostMethod { get; set; }
        public int? ProjectID { get; set; }
        public int? GLAnalysisType1 { get; set; }
        public int? GLAnalysisType2 { get; set; }
        public int? AssetID { get; set; }
        public string PONumber { get; set; }
        public string ItemUPCCode { get; set; }
        public float ReceivedQty { get; set; }
        public int? InventoryTransferID { get; set; }
    }
}
