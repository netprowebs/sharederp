﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class InventoryTransferModel
    {
        public IEnumerable<InventoryTransferDetailDTO> InventoryTransferDetailDTO { get; set; }
        public InventoryTransferHeaderDTO InventoryTransferHeaderDTO { get; set; }
    }
}
