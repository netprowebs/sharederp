﻿using Microsoft.AspNetCore.Http;
using SharedErp.Areas.Admin.Models;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SharedErp.Models
{
    public class CustomerInformationDTO
    {
        public int Id { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerOtherName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerAddress1 { get; set; }
        public string Password { get; set; }
        public string NextOfKin { get; set; }
        public string NextOfKinPhone { get; set; }
        public string CustomerAddress2 { get; set; }
        public string CustomerAddress3 { get; set; }
        public int CustomerCity { get; set; }
        public int CustomerState { get; set; }
        public string CustomerZip { get; set; }
        public int CustomerCountry { get; set; }
        public string CustomerFax { get; set; }
        public string CustomerWebPage { get; set; }
        public string CustomerSalutation { get; set; }
        public int? CustomerTypeID { get; set; }
        public string TaxIDNo { get; set; }
        public string VATTaxIDNumber { get; set; }
        public string VatTaxOtherNumber { get; set; }
        public int CurrencyID { get; set; }
        public int? GLSalesAccount { get; set; }
        public string TermsID { get; set; }
        public string TermsStart { get; set; }
        public int? TaxGroupID { get; set; }
        public decimal CreditLimit { get; set; }
        public string CreditComments { get; set; }
        public string PaymentDay { get; set; }
        public DateTime CustomerSince { get; set; }
        public string SendCreditMemos { get; set; }
        public string SendDebitMemos { get; set; }
        public string Statements { get; set; }
        public int WarehouseID { get; set; }
        public string WarehouseGLAccount { get; set; }
        public decimal AccountBalance { get; set; }
        public bool IsActive { get; set; }
        public int WalletId { get; set; }
        public int TenantId { get; set; }
        public int UserId { get; set; }
        public string Otp { get; set; }
        public bool OtpIsUsed { get; set; }
        public DateTime? OTPLastUsedDate { get; set; }
        public int? OtpNoOfTimeUsed { get; set; }
        public DeviceType DeviceType { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public string ReferralCode { get; set; }
        public string IdentificationCode { get; set; }
        public IdentificationType IdentificationType { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime IDIssueDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime IDExpireDate { get; set; }
        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        public string IdentityPhoto { get; set; }
        public string Photo { get; set; }
        //public List<IFormFile> IdentityPhoto { get; set; }
        public IFormFile FileIdentityPhoto { get; set; }
        public IFormFile FilePhoto { get; set; }

        public List<ImageFileDTO> PictureList { get; set; }
        public DateTime CreatedTime { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }

        public int RoleId { get; set; }
  

    }
}
