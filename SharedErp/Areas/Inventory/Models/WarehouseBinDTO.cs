﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class WarehouseBinDTO
    {
        public int Id { get; set; }
        public string WarehouseBinIDCode { get; set; }
        public int WarehouseID { get; set; }
        public WarehouseDTO Warehouse { get; set; }
        public string WarehouseBinName { get; set; }
        public string WarehouseBinNumber { get; set; }
        public string WarehouseBinZone { get; set; }
        public string WarehouseBinType { get; set; }
        public string WarehouseBinLocation { get; set; }
        public string WarehouseBinLength { get; set; }
        public string WarehouseBinWidth { get; set; }
        public string WarehouseBinHeight { get; set; }
        public long WarehouseBinWeight { get; set; }
        public float MinimumQuantity { get; set; }
        public float MaximumQuantity { get; set; }
        public string OverFlowBin { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        
    }
}
