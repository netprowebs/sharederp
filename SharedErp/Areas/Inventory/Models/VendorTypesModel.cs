﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class VendorTypesModel
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string VendorTypeDescription { get; set; }
        public string CreditorsControlAccount { get; set; }
        public string CurrencyExchangeGainOrLossAccount { get; set; }
        public string DiscountsAccount { get; set; }
        public string DiscountRate { get; set; }
    }
}
