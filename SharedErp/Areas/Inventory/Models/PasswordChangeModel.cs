﻿using System.ComponentModel.DataAnnotations;

namespace SharedErp.Models
{
    public class PasswordChangeModel
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [Required, MinLength(6)]
        public string NewPassword { get; set; }
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }
        [Required]
        public string CurrentPassword { get; set; }
    }
}
