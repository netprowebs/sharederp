﻿

namespace SharedErp.Models
{
    public class SubRouteDTO
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public int NameId { get; set; }

        public int RouteId { get; set; }
        public RouteDTO Route { get; set; }
    }
}
