﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class DriverModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        //[JsonIgnore]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfEmployment { get; set; }
        //[JsonIgnore]
        public DateTime AssignedDate { get; set; }
        public string Name { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Designation { get; set; }
        public string ResidentialAddress { get; set; }
        public string NextOfKin { get; set; }
        //public IFormFile Picture { get; set; }
        public string NextOfKinNumber { get; set; }
        public string BankName { get; set; }
        public string BankAccount { get; set; }
        public string WalletNumber { get; set; }
        public decimal WalletBalance { get; set; }
        [Display(Name = "License Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? LicenseDate { get; set; }
        public int? DuePeriod { get; set; }
        public int? UserId { get; set; }
        public string Email { get; set; }
        public string Alias { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ExpiringDate { get; set; }

    }
}
