﻿using SharedErp.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class InventoryByWarehouseDTO
    {
        public Guid Id { get; set; }
        public int CompanyID { get; set; }
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public int WarehouseID { get; set; }
        public int WarehouseBinID { get; set; }
        public string QtyOnHand { get; set; }
        public string QtyCommitted { get; set; }
        public string QtyOnOrder { get; set; }
        public string QtyOnBackorder { get; set; }
        public string CycleCode { get; set; }
        public DateTime LastCountDate { get; set; }
        public decimal ItemCost { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
