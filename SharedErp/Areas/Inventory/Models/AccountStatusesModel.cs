﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class AccountStatusesModel
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string AccountStatus { get; set; }
        public string AccountStatusDescription { get; set; }
        public int? UserType { get; set; }
    }
}
