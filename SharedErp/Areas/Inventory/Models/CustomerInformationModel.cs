﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class CustomerInformationModel
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int? AccountStatus { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress1 { get; set; }
        public string CustomerAddress2 { get; set; }
        public string CustomerAddress3 { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerState { get; set; }
        public string CustomerZip { get; set; }
        public string CustomerCountry { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerFax { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerWebPage { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerSalutation { get; set; }
        public int? CustomerTypeID { get; set; }
        public string TaxIDNo { get; set; }
        public string VATTaxIDNumber { get; set; }
        public string VatTaxOtherNumber { get; set; }
        public int CurrencyID { get; set; }
        public int? GLSalesAccount { get; set; }
        public int TermsID { get; set; }
        public string TermsStart { get; set; }
        public int TaxGroupID { get; set; }
        public string PriceMatrix { get; set; }
        public string PriceMatrixCurrent { get; set; }
        public string CreditRating { get; set; }
        public decimal CreditLimit { get; set; }
        public string CreditComments { get; set; }
        public string PaymentDay { get; set; }
        public DateTime ApprovalDate { get; set; }
        public DateTime CustomerSince { get; set; }
        public string SendCreditMemos { get; set; }
        public string SendDebitMemos { get; set; }
        public string Statements { get; set; }
        public string StatementCycleCode { get; set; }
        public string CustomerSpecialInstructions { get; set; }
        public string CustomerShipToId { get; set; }
        public string CustomerShipForId { get; set; }
        public string ShipMethodID { get; set; }
        public int WarehouseID { get; set; }
        public string WarehouseGLAccount { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public string ReferedBy { get; set; }
        public DateTime ReferedDate { get; set; }
        public string ReferalURL { get; set; }
        public decimal AccountBalance { get; set; }
        public DateTime CreationTime { get; set; }
        public bool IsActive { get; set; }
    }
}
