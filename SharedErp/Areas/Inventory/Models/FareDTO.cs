﻿using System;

namespace SharedErp.Models
{
    public class FareDTO 
    {
        public int Id { get; set; }
        public string DepartureTime { get; set; }
        public string TripCode { get; set; }
        public bool AvailableOnline { get; set; }
        public string ParentRouteDepartureTime { get; set; }

        public int RouteId { get; set; }
        public RouteDTO Route { get; set; }

        public int? VehicleModelId { get; set; }
        public VehicleModel VehicleModel { get; set; }
        public int? ParentRouteId { get; set; }
        public Guid? ParentTripId { get; set; }
        public decimal Amount { get; set; }
    }
}
