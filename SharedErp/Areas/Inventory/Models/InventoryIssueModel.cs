﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class InventoryIssueModel
    {
        public IEnumerable<RequisitionDetailDTO> RequisitionDetailDTO { get; set; }
        public RequisitionHeaderDTO RequisitionHeaderDTO { get; set; }
    }
}
