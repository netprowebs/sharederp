﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class InventoryGeneralLedgerChartOfAccountDTO
    {
        public int Id { get; set; }
        public string CGLAccountNumber { get; set; }
        public string GLAccountName { get; set; }
        public string GLAccountDescription { get; set; }
        public string GLAccountUse { get; set; }
        public string GLAccountType { get; set; }
        public string GLBalanceType { get; set; }
        public bool GLReportingAccount { get; set; }
        public string CurrencyID { get; set; }
        public float CurrencyExchangeRate { get; set; }
        public decimal GLAccountBalance { get; set; }
        public decimal GLAccountBeginningBalance { get; set; }
        public string GLOtherNotes { get; set; }
        public string CashFlowType { get; set; }
    }
}
