﻿using SharedErp.Models.Enum;
using SharedErp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class VehicleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberOfSeats { get; set; }
        public string VehicleModelTypeCode { get; set; }
        public int VehicleMakeId { get; set; }

    }
}
