﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SharedErp.Models
{
    public class CompanyInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPhoneNo { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime PilotPayrollDate { get; set; }
        public string BranchedFrom { get; set; }
        public bool IsParentCompany { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? IsParentDate { get; set; }
        public string Language { get; set; }
        public string CompanySizeRange { get; set; }
        public string Country { get; set; }
        public string PrimaryInterest { get; set; }
        public int BookingDaysRange { get; set; }
        public bool IsActive { get; set; }
        public bool IsTranspRecieved { get; set; }
        public string SmsUrl { get; set; }
        public string HostingUrl { get; set; }
        public string TenantUrl { get; set; }
        public int CountryId { get; set; }
        public int CityId { get; set; }
        public string LastName { get; set; }

    }
}
