﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public enum IsActive
    {
        Yes,
        No
    }
    public enum RouteType
    {
        Short,
        Medium,
        Long
    }
    public enum JourneyType
    {
        Loaded,
        Blown,
        Pickup,
        Rescue,
        Transload,
        Hire
    }
    public enum TerminalType
    {
        Physical,
        Virtual
    }

    public enum BookingTypes
    {
        Terminal,
        Advanced,
        Online,
        All,
        BookOnHold,
        Agent
    }

    public enum PlatformType
    {
        Web = 1,
        Android = 2,
        IOS = 3,
    }
}
