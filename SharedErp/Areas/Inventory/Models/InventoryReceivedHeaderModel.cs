﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Models
{
    public class InventoryReceivedHeaderModel
    {
        public Guid Id { get; set; }
        public int CompanyId { get; set; }
        public int? AdjustmentTypeID { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime TransactionDate { get; set; }
        public int? WarehouseID { get; set; }
        public int? WarehouseBinID { get; set; }
        [Required]
        public string Notes { get; set; }
        public string Void { get; set; }
        public bool Captured { get; set; }
        public bool Verified { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime? VerifiedDate { get; set; }
        public bool Issued { get; set; }
        public string IssuedBy { get; set; }
        public DateTime? IssuedDate { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public string PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public string BatchControlNumber { get; set; }
        public string BatchControlTotal { get; set; }
        public string Signature { get; set; }
        public string SignaturePassword { get; set; }
        public string SupervisorSignature { get; set; }
        public string ManagerSignature { get; set; }
        public int? Currency { get; set; }
        public decimal CurrencyExchangeRate { get; set; }
        [Required]
        public string Reference { get; set; }
        public int? WarehouseCustomerID { get; set; }
        public string DeliveryNote { get; set; }
        public string SiteNumber { get; set; }
        public string VehicleRegistration { get; set; }
        public int? DriversId { get; set; }
        public int? FromCompanyID { get; set; }
        public int? FromWarehouseID { get; set; }
        public int? FromWarehouseBinID { get; set; }
        public int? InventoryIssueTransferID { get; set; }

        public DateTime? RecieveDate { get; set; }
        public PlatformType PlatformType { get; set; }
    }
}
