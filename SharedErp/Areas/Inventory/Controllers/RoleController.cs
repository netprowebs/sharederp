﻿using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;

namespace SharedErp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class RoleController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;



        public RoleController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
       
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Roles,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<RoleModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);

        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var companyInfos = await client.GetAsync<PagedRecordModel<CompanyInfoDTO>>(Constants.ClientRoutes.CompanyInfo);
            var comp = await client.GetAsync(Constants.ClientRoutes.CompanyInfo);
            //ViewBag.Companies = new SelectList(comp?.Object?.Items, "Name", "Name");

            ViewData["Companies"] = new SelectList(companyInfos?.Object?.Items, "Id", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RoleModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<RoleModel, bool>(Constants.ClientRoutes.RoleCreates, model);


                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        //public IActionResult GetClaims()
        //{
        //    return View();
        //}

        //[HttpGet]
        //public async Task<IActionResult> GetClaims(IDataTablesRequest request)
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    var page = (request.Start / request.Length) + 1;
        //    var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.RoleClaims,
        //        page, request.Length, request.Search.Value);

        //    var result = await client.GetAsync<PagedRecordModel<ClaimModel>>(url);

        //    var pagedData = result.Object;

        //    var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
        //    return new DataTablesJsonResult(response, true);

        //}

        public ActionResult GetClaim(int id)
        {
            //return RedirectToAction("Index", "VehicleAllocationDetail", new { Id = id });
            //TempData["Tget"] = id;
            HttpContext.Session.SetString("ClaimID", id.ToString());
            return RedirectToAction("Index", new RouteValueDictionary(
            new { controller = "Claim", action = "GetData" }));
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RoleDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        //[HttpGet]
        //public IActionResult Create()
        //{
        //    return View();
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create(DriverDTO model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //        //var files = HttpContext.Request.Form.Files;
        //        var fileName = "";

        //        if (model.Picture != null)
        //        {
        //            var uploads = Path.Combine(_appEnvironment.WebRootPath, "images");

        //            fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(model.Picture.FileName).ToString();
        //            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
        //            {
        //                await model.Picture.CopyToAsync(fileStream);
        //                //model.Picture = fileName;
        //            }
        //        }

        //        var driverModel = new DriverModel
        //        {
        //            Code = model.Code,
        //            Name = model.Name,
        //            ResidentialAddress = model.ResidentialAddress,
        //            Phone1 = model.Phone1,
        //            Phone2 = model.Phone2,
        //            NextOfKin = model.NextOfKin,
        //            NextOfKinNumber = model.NextOfKinNumber,
        //            DateOfEmployment = model.DateOfEmployment,
        //            Designation = model.Designation,
        //            AssignedDate = model.AssignedDate,
        //            BankAccount = model.BankAccount,
        //            BankName = model.BankName,
        //            Picture = fileName,
        //            LicenseDate = model.LicenseDate,
        //            DuePeriod = model.DuePeriod,
        //            Email = model.Email,
        //            Alias = model.Alias
        //        };

        //        var result = await client.PostAsJsonAsync<DriverModel, bool>(Constants.ClientRoutes.DriverCreate, driverModel);

        //        if (result.IsValid && result.Object)
        //        {
        //            return RedirectToAction("index");
        //        }
        //        else
        //        {
        //            foreach (var item in result.ValidationErrors)
        //            {
        //                ModelState.AddModelError(item.Key, string.Join(",", item.Value));
        //            }

        //            ModelState.AddModelError("", result.ShortDescription);
        //        }
        //    }

        //    return View(model);
        //}

        //[HttpGet]
        //public async Task<IActionResult> Update(int id)
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    var url = string.Format(Constants.ClientRoutes.DriverGet, id);
        //    var result = await client.GetAsync<DriverModel>(url);

        //    var model = result.Object;

        //    if (model != null)
        //    {
        //        var item = new DriverDTO
        //        {
        //            Id = model.Id,
        //            Name = model.Name,
        //            Code = model.Code,
        //            DateOfEmployment = model.DateOfEmployment,
        //            AssignedDate = model.AssignedDate,
        //            Phone1 = model.Phone1,
        //            Phone2 = model.Phone2,
        //            BankAccount = model.BankAccount,
        //            BankName = model.BankName,
        //            Designation = model.Designation,
        //            NextOfKin = model.NextOfKin,
        //            NextOfKinNumber = model.NextOfKinNumber,
        //            ResidentialAddress = model.ResidentialAddress,
        //            Picture = null,
        //            LicenseDate = model.LicenseDate,
        //            DuePeriod = model.DuePeriod,
        //            UserId = model.UserId,
        //            Email = model.Email
        //        };
        //        return View(item);
        //    }

        //    return NotFound();
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Update(int id, DriverModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //        var url = string.Format(Constants.ClientRoutes.DriverUpdate, id);

        //        var result = await client.PutAsync<DriverModel, bool>(url, model);

        //        if (result.IsValid && result.Object)
        //        {
        //            return RedirectToAction("index");
        //        }
        //        else
        //        {
        //            foreach (var item in result.ValidationErrors)
        //            {
        //                ModelState.AddModelError(item.Key, string.Join(",", item.Value));
        //            }

        //            ModelState.AddModelError("", result.ShortDescription);
        //        }
        //    }

        //    return View(model);
        //}


        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RoleGet, id);
            var result = await client.GetAsync<RoleModel>(url);


            var companyInfos = await client.GetAsync<PagedRecordModel<CompanyInfoDTO>>(Constants.ClientRoutes.CompanyInfo);
            var comp = await client.GetAsync(Constants.ClientRoutes.CompanyInfo);
            //ViewBag.Companies = new SelectList(comp?.Object?.Items, "Name", "Name");

            ViewData["Companies"] = new SelectList(companyInfos?.Object?.Items, "Id", "Name");

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, RoleModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.RoleUpdate, id);

                var result = await client.PutAsync<RoleModel, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

    }
}
