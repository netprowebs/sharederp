﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SharedErp.Areas.Security.Controllers
{
    [Area("Inventory")]
    public class InventoryAdjustmentController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public InventoryAdjustmentController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        #region Inventory Adjustment header
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.AdjustmentHeader,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<InventoryAdjustmentHeaderDTO>>(url);
            // geting existing permissions or claims for the logged in user  
            var hasCanInvSave = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvSave");
            var hasCanInvVerify = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvVerify");
            var hasCanInvApprove = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvApprove");

            // this condition is checking if the loggedin user have cansave record permission with one extra permission
            if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == false)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Issued == false && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else if (hasCanInvSave == true && hasCanInvVerify == true && hasCanInvApprove == false)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Issued == true && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == true)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Issued == true && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
                return new DataTablesJsonResult(response, true);
            }


        }


        public async Task<IActionResult> Create()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            await FetchRequiredDropDowns();

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateHeader(InventoryAdjustmentHeaderDTO model)
        {
            bool items = false;

            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                // generate header id so that it can be used by the detail
                var hid = Guid.NewGuid();
                TempData["ID"] = hid;

                var InvTransHeaderModel = new InventoryAdjustmentHeaderDTO
                {
                    Id = hid,
                    Code = model.Code,
                    AdjustmentDate = model.AdjustmentDate,
                    AdjustmentType = model.AdjustmentType,
                    Reason = model.Reason,
                    Notes = model.Notes,
                    Status = model.Status,
                    Reference = model.Reference,
                    Narratives = model.Narratives,
                };
                //Code (Autogen), TransDate, AdjustType (Cost/Aquired/Assembly/Beginning/Breakage/Environment/Shrink/Spoilage) , Reference, Narratives. Checkbox(Cost,Count)
                var result = await client.PostAsJsonAsync<InventoryAdjustmentHeaderDTO, bool>(Constants.ClientRoutes.AdjustmentHeaderCreate, InvTransHeaderModel);
                items = result.Object;

                if (result.IsValid && result.Object)
                {
                    //return RedirectToAction("index");
                    //return View(items);                  
                    var models = InvTransHeaderModel;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }



        private async Task FetchRequiredDropDowns(int? selectedLocationId = null, int? selectedVehicle = null, int? selectedDriverId = null,
        int? selectedWarehouse = null, int? selectedWarehousebin = null, int? selectedFranchiseId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var itemType = await client.GetAsync<PagedRecordModel<ItemTypeDTO>>(Constants.ClientRoutes.InventorySetupItemType);
            var itemCategory = await client.GetAsync<PagedRecordModel<ItemCategoryDTO>>(Constants.ClientRoutes.InventorySetupCategory);
            var itemFamily = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(Constants.ClientRoutes.InventorySetupFamilies);
            var wareHouse = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);
            var wareHouseBin = await client.GetAsync<PagedRecordModel<WarehouseBinDTO>>(Constants.ClientRoutes.InventorySetupWarehouseBin);
            var GLAccount = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
            //var vendors = await client.GetAsync<PagedRecordModel<VendorInformationModel>>(Constants.ClientRoutes.VendorInfos);

            //var unique = itemType?.Object?.Items;
            //var InventoryCategory = itemCategory?.Object?.Items;
            //var InventoryFamily = itemFamily?.Object?.Items;
            //var itemWareHouse = wareHouse?.Object?.Items;
            //var itemWareHouseBin = wareHouseBin?.Object?.Items;
            //var salesAccount = itemSales?.Object?.Items;


            ViewBag.wareHouse = new SelectList(wareHouse.Object.Items, "Id", "WarehouseName", selectedWarehouse);
            ViewBag.wareHousebin = new SelectList(wareHouseBin.Object.Items, "Id", "WarehouseBinName", selectedWarehousebin);

            ViewBag.itemType = new SelectList(itemType.Object.Items, "Id", "ItemTypeName", selectedWarehouse);
            ViewBag.itemCategory = new SelectList(itemCategory.Object.Items, "Id", "CategoryName", selectedWarehousebin);

            ViewBag.itemFamily = new SelectList(itemFamily.Object.Items, "Id", "FamilyDescription", selectedWarehouse);
            ViewBag.GLAccount = new SelectList(GLAccount.Object.Items, "Id", "CGLAccountNumber", selectedWarehousebin);

            //ViewBag.DriverId = new SelectList(driverRequest.Object.Items, "Id", "Name", selectedDriverId);

        }




        [HttpPost]
        public async Task<IActionResult> GetAdjustmentHeaderbyId(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.AdjustmentHeaderGet, id);
            var result = await client.GetAsync<InventoryAdjustmentHeaderDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = model
                });

            }

            return NotFound();
        }


        [HttpGet]
        public async Task<IActionResult> Update(Guid id)
        {
            if (id == Guid.Empty)
            {
                id = (Guid)TempData["ID"];
            }


            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            InventoryAdjustmentModel AllModel = new InventoryAdjustmentModel();
            var url = string.Format(Constants.ClientRoutes.AdjustmentHeaderGet, id);
            var result = await client.GetAsync<InventoryAdjustmentHeaderDTO>(url);
            AllModel.InventoryAdjustmentHeaderDTO = result.Object;
            if (result != null)
            {
                await FetchRequiredDropDowns();
                TempData["vid"] = id;
                return View(AllModel.InventoryAdjustmentHeaderDTO);
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> UpdateHeader(InventoryAdjustmentHeaderDTO model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var InvAdjHeaderModel = new InventoryAdjustmentHeaderDTO
                {
                    Code = model.Code,
                    AdjustmentDate = model.AdjustmentDate,
                    AdjustmentType = model.AdjustmentType,
                    Reason = model.Reason,
                    Notes = model.Notes,
                    Status = model.Status,
                    Reference = model.Reference,
                    Narratives = model.Narratives
                };
                var url = string.Format(Constants.ClientRoutes.AdjustmentHeaderUpdate, model.Id);

                var result = await client.PutAsync<InventoryAdjustmentHeaderDTO, bool>(url, InvAdjHeaderModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteHeader(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.AdjustmentHeaderDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            var models = result.Object;

            if (models != false)
            {
                return Json(new
                {
                    data = models
                });

            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Verify(Guid id, InventoryAdjustmentHeaderDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InvAdjVerify, id);

            var result = await client.PutAsync<InventoryAdjustmentHeaderDTO, bool>(url, model);

            //return RedirectToAction("index");
            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();

        }

        [HttpPost]
        public async Task<IActionResult> Approve(Guid id, InventoryAdjustmentHeaderDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InvAdjApprove, id);

            var result = await client.PutAsync<InventoryAdjustmentHeaderDTO, bool>(url, model);
            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Adjust(Guid id, InventoryAdjustmentHeaderDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InvAdjustment, id);

            var result = await client.PutAsync<InventoryAdjustmentHeaderDTO, bool>(url, model);
            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Return(Guid id, int Rttype, InventoryAdjustmentHeaderDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            InventoryAdjustmentHeaderDTO Modelb = new InventoryAdjustmentHeaderDTO();
            Modelb.SiteNumber = Rttype.ToString();
            var url = string.Format(Constants.ClientRoutes.InvAdjReturn, id);

            var result = await client.PutAsync<InventoryAdjustmentHeaderDTO, bool>(url, Modelb);

            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();
        }

        #endregion Inventory Adjustment header

        #region Inventory Adjustment Detail

        [HttpGet]
        public async Task<IActionResult> GetDetails(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.AdjustmentDetail,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<InventoryAdjustmentDetailDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }





        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AddAdjustmentDetail(InventoryAdjustmentDetailDTO model)
        {
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<InventoryAdjustmentDetailDTO, bool>(Constants.ClientRoutes.AdjustmentDetailCreate, model);

                if (result.IsValid && result.Object)
                {
                    var models = result;
                    return Json(new
                    {
                        data = models

                    });

                    //return RedirectToAction("Update");
                    //return View(model);
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> UpdateAdjustmentDetail(InventoryAdjustmentDetailDTO model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var InvAdjDetailModel = new InventoryAdjustmentDetailDTO
                {
                    Id = model.Id,
                    ItemID = model.ItemID,
                    //IssuedQty = model.IssuedQty,
                    Cost = model.Cost,
                    GLAdjustmentPostingAccount = model.GLAdjustmentPostingAccount,
                    //PONumber = model.PONumber,
                };
                //Item, Cost , GLAcount,(On Count Checked, show columns System Qty (Auto Populate), Counted Qty)
                var url = string.Format(Constants.ClientRoutes.AdjustmentDetailUpdate, model.Id);

                var result = await client.PutAsync<InventoryAdjustmentDetailDTO, bool>(url, InvAdjDetailModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        public async Task<IActionResult> DeleteDetail(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.AdjustmentDetailDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpPost]
        public async Task<List<InventoryAdjustmentDetailDTO>> GetDetail(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var details = await client.GetAsync<InventoryReceivedDetailModel>($"{Constants.ClientRoutes.ReceivedDetailGet}");

            var url = string.Format(Constants.ClientRoutes.AdjustmentDetailGet, id);

            var result = await client.GetAsync<List<InventoryAdjustmentDetailDTO>>(url);
            var model = result.Object;
            return model;

        }

        #endregion InventoryAdjustment Detail
    }
}
