﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SharedErp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class VendorTypeController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public VendorTypeController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.VendorTypes,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<VendorTypesModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        [HttpGet]
        public IActionResult Create()
        {

            //var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var companyInfos = await client.GetAsync<PagedRecordModel<CompanyInfoDTO>>(Constants.ClientRoutes.CompanyInfo);
            //var comp = await client.GetAsync(Constants.ClientRoutes.CompanyInfo);
            ////ViewBag.Companies = new SelectList(comp?.Object?.Items, "Name", "Name");

            //var unique = companyInfos?.Object?.Items.Where(x => x.BranchedFrom == null);

            //ViewData["Companies"] = new SelectList(unique, "Name", "Name");


            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VendorTypesModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<VendorTypesModel, bool>(Constants.ClientRoutes.VendorTypeCreate, model);


                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VendorTypeGet, id);
            var result = await client.GetAsync<VendorTypesModel>(url);


            //var companyInfos = await client.GetAsync<PagedRecordModel<CompanyInfoDTO>>(Constants.ClientRoutes.CompanyInfo);
            //var comp = await client.GetAsync(Constants.ClientRoutes.CompanyInfo);
            ////ViewBag.Companies = new SelectList(comp?.Object?.Items, "Name", "Name");

            //var unique = companyInfos?.Object?.Items.Where(x => x.BranchedFrom == null);

            //ViewData["Companies"] = new SelectList(unique, "Name", "Name");

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, VendorTypesModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.VendorTypeUpdate, id);

                var result = await client.PutAsync<VendorTypesModel, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VendorTypeDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
    }
}
