﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SharedErp.Areas.Security.Controllers
{
    [Area("Inventory")]
    public class ReportController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public ReportController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        // commit
        #region InventoryOnHand
        public IActionResult InventoryOnHand()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> InventoryOnHandGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventoryByWarehouse,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<InventoryByWarehouseDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(InventoryByWarehouseDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<InventoryByWarehouseDTO, bool>(Constants.ClientRoutes.InventoryByWarehouseCreate, model);


                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("InventoryOnHand");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> Update(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventoryByWarehouseGet, id);
            var result = await client.GetAsync<InventoryByWarehouseDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(Guid id, InventoryByWarehouseDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.InventoryByWarehouseUpdate, id);

                var result = await client.PutAsync<InventoryByWarehouseDTO, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("InventoryOnHand");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventoryByWarehouseDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }



        private async Task FetchRequiredDropDowns(int? selectedLocationId = null, int? selectedVehicle = null, int? selectedDriverId = null,
        int? selectedWarehouse = null, int? selectedWarehousebin = null, int? selectedFranchiseId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var itemType = await client.GetAsync<PagedRecordModel<ItemTypeDTO>>(Constants.ClientRoutes.InventorySetupItemType);
            var itemCategory = await client.GetAsync<PagedRecordModel<ItemCategoryDTO>>(Constants.ClientRoutes.InventorySetupCategory);
            var itemFamily = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(Constants.ClientRoutes.InventorySetupFamilies);
            var wareHouse = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);
            var wareHouseBin = await client.GetAsync<PagedRecordModel<WarehouseBinDTO>>(Constants.ClientRoutes.InventorySetupWarehouseBin);
            var GLAccount = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
            //var vendors = await client.GetAsync<PagedRecordModel<VendorInformationModel>>(Constants.ClientRoutes.VendorInfos);

            //var unique = itemType?.Object?.Items;
            //var InventoryCategory = itemCategory?.Object?.Items;
            //var InventoryFamily = itemFamily?.Object?.Items;
            //var itemWareHouse = wareHouse?.Object?.Items;
            //var itemWareHouseBin = wareHouseBin?.Object?.Items;
            //var salesAccount = itemSales?.Object?.Items;


            ViewBag.wareHouse = new SelectList(wareHouse.Object.Items, "Id", "WarehouseName", selectedWarehouse);
            ViewBag.wareHousebin = new SelectList(wareHouseBin.Object.Items, "Id", "WarehouseBinName", selectedWarehousebin);

            ViewBag.itemType = new SelectList(itemType.Object.Items, "Id", "ItemTypeName", selectedWarehouse);
            ViewBag.itemCategory = new SelectList(itemCategory.Object.Items, "Id", "CategoryName", selectedWarehousebin);

            ViewBag.itemFamily = new SelectList(itemFamily.Object.Items, "Id", "FamilyDescription", selectedWarehouse);
            ViewBag.GLAccount = new SelectList(GLAccount.Object.Items, "Id", "CGLAccountNumber", selectedWarehousebin);

            //ViewBag.DriverId = new SelectList(driverRequest.Object.Items, "Id", "Name", selectedDriverId);

        }




        [HttpPost]
        public async Task<IActionResult> Get(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventoryByWarehouseGet, id);
            var result = await client.GetAsync<InventoryByWarehouseDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = model
                });

            }

            return NotFound();
        }


        #endregion InventoryOnHand


    }
}
