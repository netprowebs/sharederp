﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SharedErp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CustomerInfoController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public CustomerInfoController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.CustomerInfos,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<CustomerInformationModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var accountStatuses = await client.GetAsync<PagedRecordModel<AccountStatusesModel>>(Constants.ClientRoutes.AccountStats);
            var CustomerTypes = await client.GetAsync<PagedRecordModel<CustomerTypesModel>>(Constants.ClientRoutes.CustomerTypes);
            var warehouses = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);


            var acoountStats = accountStatuses?.Object?.Items;
            var Customertyps = CustomerTypes?.Object?.Items;
            var wareHouses = warehouses?.Object?.Items;


            ViewData["AccountStats"] = new SelectList(acoountStats, "Id", "AccountStatus");
            ViewData["CustomerTypes"] = new SelectList(Customertyps, "Id", "CustomerTypeDescription");
            ViewData["WareHouses"] = new SelectList(wareHouses, "Id", "WarehouseName");



            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CustomerInformationModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<CustomerInformationModel, bool>(Constants.ClientRoutes.CustomerInfoCreate, model);


                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.CustomerInfoGet, id);
            var result = await client.GetAsync<CustomerInformationModel>(url);


            //var companyInfos = await client.GetAsync<PagedRecordModel<CompanyInfoDTO>>(Constants.ClientRoutes.CompanyInfo);
            //var comp = await client.GetAsync(Constants.ClientRoutes.CompanyInfo);
            ////ViewBag.Companies = new SelectList(comp?.Object?.Items, "Name", "Name");

            //var unique = companyInfos?.Object?.Items.Where(x => x.BranchedFrom == null);

            //ViewData["Companies"] = new SelectList(unique, "Name", "Name");

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, CustomerInformationModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.CustomerInfoUpdate, id);

                var result = await client.PutAsync<CustomerInformationModel, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.CustomerInfoDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
    }
}
