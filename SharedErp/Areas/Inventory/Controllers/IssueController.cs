﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SharedErp.Areas.Security.Controllers
{
    [Area("Inventory")]
    public class IssueController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public IssueController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        // commit
        #region InventoryIssueheader
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.RequisitionHeader,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<RequisitionHeaderDTO>>(url);
            // geting existing permissions or claims for the logged in user  
            var hasCanInvSave = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvSave");
            var hasCanInvVerify = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvVerify");
            var hasCanInvApprove = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvApprove");

            // this condition is checking if the loggedin user have cansave record permission with one extra permission
            if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == false)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == false && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else if (hasCanInvSave == true && hasCanInvVerify == true && hasCanInvApprove == false)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == true && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == true)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == true && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
                return new DataTablesJsonResult(response, true);
            }


        }


        public async Task<IActionResult> Create()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            await FetchRequiredDropDowns();

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateHeader(RequisitionHeaderDTO model)
        {
            bool items = false;

            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                // generate header id so that it can be used by the detail
                var hid = Guid.NewGuid();
                TempData["ISSID"] = hid;

                var InvReqHeaderModel = new RequisitionHeaderDTO
                {
                    Id = hid,
                    WarehouseID = model.WarehouseID,
                    WarehouseBinID = model.WarehouseBinID,
                    AdjustmentTypeID = model.AdjustmentTypeID,
                    //v = model.VehicleRegistration,
                    //dr = model.DriversId,
                    Notes = model.Notes,
                    TransactionDate = model.TransactionDate
                };

                var result = await client.PostAsJsonAsync<RequisitionHeaderDTO, bool>(Constants.ClientRoutes.RequisitionHeaderCreate, InvReqHeaderModel);
                items = result.Object;

                if (result.IsValid && result.Object)
                {
                    //return RedirectToAction("index");
                    //return View(items);                  
                    var models = InvReqHeaderModel;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }



        private async Task FetchRequiredDropDowns(int? selectedLocationId = null, int? selectedVehicle = null, int? selectedDriverId = null,
        int? selectedWarehouse = null, int? selectedWarehousebin = null, int? selectedFranchiseId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var itemType = await client.GetAsync<PagedRecordModel<ItemTypeDTO>>(Constants.ClientRoutes.InventorySetupItemType);
            var itemCategory = await client.GetAsync<PagedRecordModel<ItemCategoryDTO>>(Constants.ClientRoutes.InventorySetupCategory);
            var itemFamily = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(Constants.ClientRoutes.InventorySetupFamilies);
            var wareHouse = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);
            var wareHouseBin = await client.GetAsync<PagedRecordModel<WarehouseBinDTO>>(Constants.ClientRoutes.InventorySetupWarehouseBin);
            var GLAccount = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
            //var vendors = await client.GetAsync<PagedRecordModel<VendorInformationModel>>(Constants.ClientRoutes.VendorInfos);

            //var unique = itemType?.Object?.Items;
            //var InventoryCategory = itemCategory?.Object?.Items;
            //var InventoryFamily = itemFamily?.Object?.Items;
            //var itemWareHouse = wareHouse?.Object?.Items;
            //var itemWareHouseBin = wareHouseBin?.Object?.Items;
            //var salesAccount = itemSales?.Object?.Items;


            ViewBag.wareHouse = new SelectList(wareHouse.Object.Items, "Id", "WarehouseName", selectedWarehouse);
            ViewBag.wareHousebin = new SelectList(wareHouseBin.Object.Items, "Id", "WarehouseBinName", selectedWarehousebin);

            ViewBag.itemType = new SelectList(itemType.Object.Items, "Id", "ItemTypeName", selectedWarehouse);
            ViewBag.itemCategory = new SelectList(itemCategory.Object.Items, "Id", "CategoryName", selectedWarehousebin);

            ViewBag.itemFamily = new SelectList(itemFamily.Object.Items, "Id", "FamilyDescription", selectedWarehouse);
            ViewBag.GLAccount = new SelectList(GLAccount.Object.Items, "Id", "CGLAccountNumber", selectedWarehousebin);

            //ViewBag.DriverId = new SelectList(driverRequest.Object.Items, "Id", "Name", selectedDriverId);

        }




        [HttpPost]
        public async Task<IActionResult> GetRequisitionHeaderbyId(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RequisitionHeaderGet, id);
            var result = await client.GetAsync<RequisitionHeaderDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = model
                });

            }

            return NotFound();
        }


        [HttpGet]
        public async Task<IActionResult> Update(Guid id)
        {
            if (id == Guid.Empty)
            {
                id = (Guid)TempData["ID"];
            }


            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            InventoryIssueModel AllModel = new InventoryIssueModel();
            var url = string.Format(Constants.ClientRoutes.RequisitionHeaderGet, id);
            var result = await client.GetAsync<RequisitionHeaderDTO>(url);
            AllModel.RequisitionHeaderDTO = result.Object;
            if (result != null)
            {
                await FetchRequiredDropDowns();
                TempData["vid"] = id;
                return View(AllModel.RequisitionHeaderDTO);
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> UpdateHeader(RequisitionHeaderDTO model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var InvReqHeaderModel = new RequisitionHeaderDTO
                {
                    Id = model.Id,
                    WarehouseID = model.WarehouseID,
                    WarehouseBinID = model.WarehouseBinID,
                    //VehicleRegistration = model.VehicleRegistration,
                    //DriversId = model.DriversId,
                    Notes = model.Notes,
                    TransactionDate = model.TransactionDate,
                    Captured = model.Captured
                };
                var url = string.Format(Constants.ClientRoutes.RequisitionHeaderUpdate, model.Id);

                var result = await client.PutAsync<RequisitionHeaderDTO, bool>(url, InvReqHeaderModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteHeader(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RequisitionHeaderDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            var models = result.Object;

            if (models != false)
            {
                return Json(new
                {
                    data = models
                });

            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Verify(Guid id, RequisitionHeaderDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InvReqVerify, id);

            var result = await client.PutAsync<RequisitionHeaderDTO, bool>(url, model);

            //return RedirectToAction("index");
            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();

        }

        [HttpPost]
        public async Task<IActionResult> Approve(Guid id, RequisitionHeaderDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InvReqApprove, id);

            var result = await client.PutAsync<RequisitionHeaderDTO, bool>(url, model);
            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Issue(Guid id, RequisitionHeaderDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InvRequisition, id);

            var result = await client.PutAsync<RequisitionHeaderDTO, bool>(url, model);
            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Return(Guid id, int Rttype, RequisitionHeaderDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            RequisitionHeaderDTO Modelb = new RequisitionHeaderDTO();
            Modelb.SiteNumber = Rttype.ToString();
            var url = string.Format(Constants.ClientRoutes.InvReqReturn, id);

            var result = await client.PutAsync<RequisitionHeaderDTO, bool>(url, Modelb);

            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();
        }

        #endregion InventoryIssueheader

        #region InventoryIssueDetail

        [HttpGet]
        public async Task<IActionResult> GetDetails(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.RequisitionDetail,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<RequisitionDetailDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }





        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AddRequisitionDetail(RequisitionDetailDTO model)
        {
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<RequisitionDetailDTO, bool>(Constants.ClientRoutes.RequisitionDetailCreate, model);

                if (result.IsValid && result.Object)
                {
                    var models = result;
                    return Json(new
                    {
                        data = models

                    });

                    //return RedirectToAction("Update");
                    //return View(model);
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> UpdateRequisitionDetail(RequisitionDetailDTO model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var InvReqDetailModel = new RequisitionDetailDTO
                {
                    Id = model.Id,
                    WarehouseID = model.WarehouseID,
                    WarehouseBinID = model.WarehouseBinID,
                    ItemUPCCode = model.ItemUPCCode,
                    ItemID = model.ItemID,
                    IssuedQty = model.IssuedQty,
                    ItemCost = model.ItemCost,
                    GLExpenseAccount = model.GLExpenseAccount,
                    PONumber = model.PONumber,
                    RequisitionID = model.RequisitionID,
                };
                var url = string.Format(Constants.ClientRoutes.RequisitionDetailUpdate, model.Id);

                var result = await client.PutAsync<RequisitionDetailDTO, bool>(url, InvReqDetailModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        public async Task<IActionResult> DeleteDetail(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RequisitionDetailDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpPost]
        public async Task<List<RequisitionDetailDTO>> GetDetail(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var details = await client.GetAsync<InventoryReceivedDetailModel>($"{Constants.ClientRoutes.ReceivedDetailGet}");

            var url = string.Format(Constants.ClientRoutes.RequisitionDetailGet, id);

            var result = await client.GetAsync<List<RequisitionDetailDTO>>(url);
            var model = result.Object;
            return model;

        }



        #endregion InventoryIssueDetail
    }
}
