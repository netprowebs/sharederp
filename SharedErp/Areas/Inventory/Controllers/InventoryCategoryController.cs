﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Models;
using SharedErp.Utils;

namespace SharedErp.Areas.Admin.Controllers
{
    [Area("Admin")]

    public class InventoryCategoryController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public InventoryCategoryController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllItemCategories(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetupCategory,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<ItemCategoryDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ItemCategoryDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<ItemCategoryDTO, bool>(Constants.ClientRoutes.InventorySetupCategoryCreate, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Createe()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Createe(ItemCategoryDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<ItemCategoryDTO, bool>(Constants.ClientRoutes.InventorySetupCategoryCreate, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("create", "inventorysetup");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupCategoryGet, id);
            var result = await client.GetAsync<ItemCategoryDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, ItemCategoryDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.InventorySetupCategoryUpdate, id);

                var result = await client.PutAsync<ItemCategoryDTO, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupCategoryDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
    }
}
