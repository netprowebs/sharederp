﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Utils.Alerts;
using SharedErp.Areas.Security.Models;
using SharedErp.Areas.Admin.Models;
using SHB.Core.Entities.Enums;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace SharedErp.Areas.Admin.Controllers
{

    [Area("Inventory")]
    public class InventorySetupController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public InventorySetupController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> ItemSetups()
        {
            return View();
        }

        //public async Task<IActionResult> ItemIndex()
        //{
        //    return View();
        //}

        #region Part B

        #region Inventory Items
        [HttpGet]
        public async Task<IActionResult> GetAllInventoryItems(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetup,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<InventoryItemDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }



        [HttpGet]
        public async Task<IActionResult> Create()
        {

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var itemType = await client.GetAsync<PagedRecordModel<ItemTypeDTO>>(Constants.ClientRoutes.InventorySetupItemType);
            var itemCategory = await client.GetAsync<PagedRecordModel<ItemCategoryDTO>>(Constants.ClientRoutes.InventorySetupCategory);
            var itemFamily = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(Constants.ClientRoutes.InventorySetupFamilies);
            var wareHouse = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);
            var wareHouseBin = await client.GetAsync<PagedRecordModel<WarehouseBinDTO>>(Constants.ClientRoutes.InventorySetupWarehouseBin);
            var itemSales = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
            var vendors = await client.GetAsync<PagedRecordModel<VendorInformationModel>>(Constants.ClientRoutes.VendorInfos);

            var unique = itemType?.Object?.Items;
            var InventoryCategory = itemCategory?.Object?.Items;
            var InventoryFamily = itemFamily?.Object?.Items;
            var itemWareHouse = wareHouse?.Object?.Items;
            var itemWareHouseBin = wareHouseBin?.Object?.Items;
            var salesAccount = itemSales?.Object?.Items;
            var vendor = vendors?.Object?.Items;


            ViewData["ItemTypelist"] = new SelectList(unique, "Id", "ItemTypeName");
            ViewData["InventoryCategory"] = new SelectList(InventoryCategory, "Id", "CategoryName");
            ViewData["InventoryFamily"] = new SelectList(InventoryFamily, "Id", "FamilyDescription");
            ViewData["ItemWareHouse"] = new SelectList(itemWareHouse, "Id", "WarehouseName");
            ViewData["ItemWareHouseBin"] = new SelectList(itemWareHouseBin, "Id", "WarehouseBinName");
            ViewData["ItemSalesAccount"] = new SelectList(salesAccount, "Id", "CGLAccountNumber");
            ViewData["ItemCOGSAccount"] = new SelectList(salesAccount, "Id", "CGLAccountNumber");
            ViewData["ItemInventoryAccount"] = new SelectList(salesAccount, "Id", "CGLAccountNumber");
            ViewData["Vendors"] = new SelectList(vendor, "Id", "VendorName");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(InventoryItemDTO model)
        {


            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<InventoryItemDTO, bool>(Constants.ClientRoutes.InventorySetupCreate, model);



                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }

            }

            return View(model);

        }

        public async Task<IActionResult> InventoryItemSave(int Id, string Code, string Name, string Description)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            InventoryItemDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new InventoryItemDTO
            {
                Id = Id,
                ItemCode = Code,
                ItemName = Name,
                itemDescription = Description,
                //GLAccountType = Type
                //WarehouseID = 3
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<InventoryItemDTO, bool>(Constants.ClientRoutes.InventorySetupCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.InventorySetupUpdate, Id);
                var result = await client.PutAsync<InventoryItemDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("index");
        }
        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var itemType = await client.GetAsync<PagedRecordModel<ItemTypeDTO>>(Constants.ClientRoutes.InventorySetupItemType);
            var itemCategory = await client.GetAsync<PagedRecordModel<ItemCategoryDTO>>(Constants.ClientRoutes.InventorySetupCategory);
            var itemFamily = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(Constants.ClientRoutes.InventorySetupFamilies);
            var wareHouse = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);
            var wareHouseBin = await client.GetAsync<PagedRecordModel<WarehouseBinDTO>>(Constants.ClientRoutes.InventorySetupWarehouseBin);
            var itemSales = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);

            var ItemType = itemType?.Object?.Items;
            var InventoryCategory = itemCategory?.Object?.Items;
            var InventoryFamily = itemFamily?.Object?.Items;
            var itemWareHouse = wareHouse?.Object?.Items;
            var itemWareHouseBin = wareHouseBin?.Object?.Items;
            var salesAccount = itemSales?.Object?.Items;

            ViewData["ItemTypelist"] = new SelectList(ItemType, "Id", "ItemTypeName");
            ViewData["InventoryCategory"] = new SelectList(InventoryCategory, "Id", "CategoryName");
            ViewData["InventoryFamily"] = new SelectList(InventoryFamily, "Id", "FamilyDescription");
            ViewData["ItemWareHouse"] = new SelectList(itemWareHouse, "Id", "WarehouseName");
            ViewData["ItemWareHouseBin"] = new SelectList(itemWareHouseBin, "Id", "WarehouseBinName");
            ViewData["ItemSalesAccount"] = new SelectList(salesAccount, "Id", "CGLAccountNumber");
            ViewData["ItemCOGSAccount"] = new SelectList(salesAccount, "Id", "CGLAccountNumber");
            ViewData["ItemInventoryAccount"] = new SelectList(salesAccount, "Id", "CGLAccountNumber");


            var url = string.Format(Constants.ClientRoutes.InventorySetupGet, id);
            var result = await client.GetAsync<InventoryItemDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, InventoryItemDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.InventorySetupUpdate, id);

                var result = await client.PutAsync<InventoryItemDTO, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        #endregion

        #region Warehouse

        [HttpGet]
        public async Task<IActionResult> WarehouseGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetupWarehouse,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }


        [HttpGet]
        public async Task<IActionResult> WarehouseGetDropDown(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetupWarehouse,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);

            return new DataTablesJsonResult(response, true);

        }

        public async Task<IActionResult> WarehouseSave(int Id, string Code, string Name, string Address1)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            WarehouseDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new WarehouseDTO
            {
                WarehouseCode = Code,
                WarehouseName = Name,
                WarehouseAddress1 = Address1,
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<WarehouseDTO, bool>(Constants.ClientRoutes.InventorySetupWarehouseCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.InventorySetupWarehouseUpdate, Id);
                var result = await client.PutAsync<WarehouseDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("index");
        }


        public async Task<IActionResult> WarehouseUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupWarehouseGet, id);
            var result = await client.GetAsync<WarehouseDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> WarehouseDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupWarehouseDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
        #endregion

        #region Warehouse bin
        //Warehouse Bin Logic starts here


        [HttpGet]
        public async Task<IActionResult> WarehouseBinGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetupWarehouseBin,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<WarehouseBinDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> WarehouseBinSave(int Id, string Code, string Name, string Location, bool IsActive, int WarehouseID)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            WarehouseBinDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new WarehouseBinDTO
            {
                Id = Id,
                WarehouseBinIDCode = Code,
                WarehouseBinName = Name,
                WarehouseBinLocation = Location,
                IsActive = IsActive,
                WarehouseID = WarehouseID
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<WarehouseBinDTO, bool>(Constants.ClientRoutes.InventorySetupWarehouseBinCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.InventorySetupWarehouseBinUpdate, Id);
                var result = await client.PutAsync<WarehouseBinDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("index");
        }

        public async Task<IActionResult> WarehouseBinUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupWarehouseBinGet, id);
            var result = await client.GetAsync<WarehouseBinDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> WarehouseBinDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupWarehouseBinDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
        #endregion

        #region Item Category
        // Item Category Logic Starts here

        [HttpGet]
        public async Task<IActionResult> ItemCategoryGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetupCategory,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<ItemCategoryDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> ItemCategorySave(int Id, string Code, string Name, string Description, string LongDescription)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            ItemCategoryDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new ItemCategoryDTO
            {
                Id = Id,
                ItemCategoryCode = Code,
                CategoryName = Name,
                CategoryDescription = Description,
                CategoryLongDescription = LongDescription
                //WarehouseID = 3
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<ItemCategoryDTO, bool>(Constants.ClientRoutes.InventorySetupCategoryCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.InventorySetupCategoryUpdate, Id);
                var result = await client.PutAsync<ItemCategoryDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("index");
        }

        public async Task<IActionResult> ItemCategoryUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupCategoryGet, id);
            var result = await client.GetAsync<ItemCategoryDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> ItemCategoryDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupCategoryDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        #endregion

        #region Item Family
        // Item Family Logic Starts here

        [HttpGet]
        public async Task<IActionResult> ItemFamilyGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetupFamilies,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> ItemFamilySave(int Id, string Code, string Description, string LongDescription, string PictureUrl)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            ItemFamilyDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new ItemFamilyDTO
            {
                Id = Id,
                ItemFamilyCode = Code,
                FamilyDescription = Description,
                FamilyLongDescription = LongDescription,
                FamilyPictureURL = PictureUrl
                //WarehouseID = 3
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<ItemFamilyDTO, bool>(Constants.ClientRoutes.InventorySetupFamiliesCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.InventorySetupFamiliesUpdate, Id);
                var result = await client.PutAsync<ItemFamilyDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("index");
        }

        public async Task<IActionResult> ItemFamilyUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupFamiliesGet, id);
            var result = await client.GetAsync<ItemFamilyDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> ItemFamilyDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupFamiliesDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        #endregion

        #region Ledger Account
        // Ledger Account Logic Starts here

        [HttpGet]
        public async Task<IActionResult> LedgerAccountGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetupGeneralLedger,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> LedgerAccountSave(int Id, string Number, string Name, string Description, string Type)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            InventoryGeneralLedgerChartOfAccountDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new InventoryGeneralLedgerChartOfAccountDTO
            {
                Id = Id,
                CGLAccountNumber = Number,
                GLAccountName = Name,
                GLAccountDescription = Description,
                GLAccountType = Type
                //WarehouseID = 3
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<InventoryGeneralLedgerChartOfAccountDTO, bool>(Constants.ClientRoutes.InventorySetupGeneralLedgerCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.InventorySetupGeneralLedgerUpdate, Id);
                var result = await client.PutAsync<InventoryGeneralLedgerChartOfAccountDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("index");
        }

        public async Task<IActionResult> LedgerAccountUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupGeneralLedgerGet, id);
            var result = await client.GetAsync<InventoryGeneralLedgerChartOfAccountDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> LedgerAccountDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupGeneralLedgerrDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        #endregion

        #region Adjustment Type
        // Adjustment Type Logic Starts here

        [HttpGet]
        public async Task<IActionResult> AdjustmentTypeGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetupAdjustmentType,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<InventoryAdjustmentTypeDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> AdjustmentTypeSave(int Id, string Name, string Description)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            InventoryAdjustmentTypeDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new InventoryAdjustmentTypeDTO
            {
                Id = Id,
                AdjustmentTypeName = Name,
                AdjustmentTypeDescription = Description
                //WarehouseID = 3
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<InventoryAdjustmentTypeDTO, bool>(Constants.ClientRoutes.InventorySetupAdjustmentTypeCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.InventorySetupAdjustmentTypeUpdate, Id);
                var result = await client.PutAsync<InventoryAdjustmentTypeDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("index");
        }

        public async Task<IActionResult> AdjustmentTypeUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupAdjustmentTypeGet, id);
            var result = await client.GetAsync<InventoryAdjustmentTypeDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> AdjustmentTypeDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupAdjustmentTypeDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        #endregion

        #endregion


        #region Part C
        #region Customer Information
        // Customer Information Logic Starts here

        [HttpGet]
        public async Task<IActionResult> CustomerInfoGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.CustomerInfos,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<CustomerInformationDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> CustomerInfoSave(int Id, string Name, string LastName, string Phone, int TypeID, DateTime Time, string Email, string State, bool IsActive, string Address)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            CustomerInformationDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new CustomerInformationDTO
            {
                Id = Id,
                CustomerFirstName = Name,
                CustomerLastName = LastName,
                CustomerPhone = Phone,
                CustomerTypeID = TypeID,
                CreatedTime = Time,
                CustomerEmail = Email,
                //CustomerState = State,
                IsActive = IsActive,
                CustomerAddress1 = Address,
                //CustomerTypeID = Description,
                //WarehouseID = 3
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<CustomerInformationDTO, bool>(Constants.ClientRoutes.CustomerInfoCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.CustomerInfoUpdate, Id);
                var result = await client.PutAsync<CustomerInformationDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("ItemSetups");
        }

        public async Task<IActionResult> CustomerInfoUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.CustomerInfoGet, id);
            var result = await client.GetAsync<CustomerInformationDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> CustomerInfoDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.CustomerInfoDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        #endregion

        #region Customer Type
        // Customer Information Logic Starts here

        [HttpGet]
        public async Task<IActionResult> CustomerTypeGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.CustomerTypes,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<CustomerTypesDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> CustomerTypeSave(int Id, string Rate, string Description, string CompanyID)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            CustomerTypesDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new CustomerTypesDTO
            {
                ID = Id,
                CustomerTypeDescription = Description,
                DiscountRate = Rate,
                CompanyID = CompanyID,
                //CustomerTypeID = Description,
                //WarehouseID = 3
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<CustomerTypesDTO, bool>(Constants.ClientRoutes.CustomerTypeCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.CustomerTypeUpdate, Id);
                var result = await client.PutAsync<CustomerTypesDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("ItemSetups");
        }

        public async Task<IActionResult> CustomerTypeUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.CustomerTypeGet, id);
            var result = await client.GetAsync<CustomerTypesDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> CustomerTypeDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.CustomerTypeDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        #endregion

        #region Vendor Information
        // Vendor Information Logic Starts here

        [HttpGet]
        public async Task<IActionResult> VendorInfoGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.VendorInfos,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<VendorInformationDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> VendorInfoSave(int Id, string Name, int TypeID, string Phone, DateTime Time, string State, string Address)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            VendorInformationDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new VendorInformationDTO
            {
                ID = Id,
                VendorName = Name,
                VendorTypeID = TypeID,
                VendorPhone = Phone,
                CreationTime = Time,
                VendorState = State,
                VendorAddress1 = Address,
                //CustomerTypeID = Description,
                //WarehouseID = 3
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<VendorInformationDTO, bool>(Constants.ClientRoutes.VendorInfoCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.VendorInfoUpdate, Id);
                var result = await client.PutAsync<VendorInformationDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("ItemSetups");
        }

        public async Task<IActionResult> VendorInfoUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VendorInfoGet, id);
            var result = await client.GetAsync<VendorInformationDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> VendorInfoDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VendorInfoDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        #endregion

        #region Vendor Type
        // Vendor Type Logic Starts here

        [HttpGet]
        public async Task<IActionResult> VendorTypeGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.VendorTypes,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<VendorTypesDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> VendorTypeSave(int Id, string Name, string Description, int CompanyID)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            VendorTypesDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new VendorTypesDTO
            {
                ID = Id,
                Name = Description,
                VendorTypeDescription = Description,
                CompanyId = CompanyID,
                //CustomerTypeID = Description,
                //WarehouseID = 3
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<VendorTypesDTO, bool>(Constants.ClientRoutes.VendorTypeCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.VendorTypeUpdate, Id);
                var result = await client.PutAsync<VendorTypesDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("ItemSetups");
        }

        public async Task<IActionResult> VendorTypeUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VendorTypeGet, id);
            var result = await client.GetAsync<VendorTypesDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> VendorTypeDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VendorTypeDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        #endregion

        #region Terminal
        // Terminal Logic Starts here

        [HttpGet]
        public async Task<IActionResult> TerminalGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Terminals,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<TerminalModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);

        }

        public async Task<IActionResult> TerminalSave(int Id, string Code, string Name, string Address, string Phone, float Latitude, float Longitude, int TypeID, bool IsActive)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            TerminalModel model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new TerminalModel
            {
                //Id = Id,
                Code = Code,
                Name = Name,
                Address = Address,
                ContactPersonNo = Phone,
                Latitude = Latitude,
                Longitude = Longitude,
                //TerminalType = (TerminalType)TypeID,
                //IsActive = IsActive,
                //CustomerTypeID = Description,
                //WarehouseID = 3
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<TerminalModel, bool>(Constants.ClientRoutes.TerminalCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.TerminalUpdate, Id);
                var result = await client.PutAsync<TerminalModel, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("ItemSetups");
        }

        public async Task<IActionResult> TerminalUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.TerminalGet, id);
            var result = await client.GetAsync<TerminalModel>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> TerminalDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.TerminalDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        #endregion

        #endregion

        #region Part D

        #region Inventory Item

        public IActionResult ItemIndex()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ItemGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetup,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<InventoryItemDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        [HttpGet]
        public async Task<IActionResult> ItemCreate()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> ItemCreate(InventoryItemDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<InventoryItemDTO, bool>(Constants.ClientRoutes.InventorySetupCreate, model);


                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("itemindex");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> ItemUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupGet, id);
            var result = await client.GetAsync<InventoryItemDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ItemUpdate(int id, InventoryItemDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.InventorySetupUpdate, id);

                var result = await client.PutAsync<InventoryItemDTO, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("itemindex");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ItemDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        //[HttpPost]
        //public async Task<IActionResult> GetItembyId(int id)
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    var url = string.Format(Constants.ClientRoutes.InventorySetupGet, id);
        //    var result = await client.GetAsync<InventoryReceivedHeaderModel>(url);

        //    var model = result.Object;

        //    if (model != null)
        //    {
        //        return Json(new
        //        {
        //            data = model
        //        });

        //    }

        //    return NotFound();
        //}
        #endregion

        #region Employees 

        public IActionResult EmployeesIndex()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> EmployeesGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Employees,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<EmployeeModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> EmployeeSave(int Id, string FirstName, string LastName, bool IsActive, string Code, int RoleId, int TerminalId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            EmployeeModel model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new EmployeeModel
            {
                Id = Id,
                FirstName = FirstName,
                LastName = LastName,
                EmployeeCode = Code,
                IsActive = IsActive,
                RoleId = RoleId,
                TerminalId = TerminalId,
                //da = TerminalId
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<EmployeeModel, bool>(Constants.ClientRoutes.EmployeeCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.EmployeeUpdate, Id);
                var result = await client.PutAsync<EmployeeModel, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("index");
        }

        [HttpGet]
        public async Task<IActionResult> EmployeesCreate()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            //await FetchRequiredDropDowns();

            ViewBag.EndDate = DateTime.Now;
            ViewBag.StartDate = DateTime.Now.Date;

            return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EmployeesCreate(EmployeeModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                model.DeviceType = DeviceType.AdminWeb;
                // for picture insertion
                List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
                if (model.FilePhoto != null)
                {
                    var filedata = await InfoForImage(model.FilePhoto, "Photo");
                    imageFileDTO.Add(filedata);
                    model.EmployeePhoto = "Photo";
                }
                model.FilePhoto = null;
                model.PictureList = imageFileDTO;

                var result = await client.PostAsJsonAsync<EmployeeModel, bool>(Constants.ClientRoutes.EmployeeCreate, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("EmployeesIndex");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            //await FetchRequiredDropDowns(model.RoleId, model.TerminalId);

            return View(model);
        }

        public async Task<ImageFileDTO> InfoForImage(IFormFile file, string txt)
        {
            IFormFile VetUpload = file;
            ImageFileDTO imageFileD = new ImageFileDTO();

            byte[] datas;
            using (var br = new BinaryReader(VetUpload.OpenReadStream()))
                datas = br.ReadBytes((int)VetUpload.OpenReadStream().Length);

            imageFileD.ContentType = VetUpload.ContentType;
            imageFileD.fileName = VetUpload.FileName;
            imageFileD.FilenameWithOutExt = Path.GetFileNameWithoutExtension(VetUpload.FileName);
            imageFileD.data = datas;
            imageFileD.ImgContent = txt;

            return imageFileD;
        }

        private async Task FetchRequiredDropDowns(int? selectedRoleId = null, int? selectedTerminalId = null, int? selectedDepartmentId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var stateRequest = await client.GetAsync<PagedRecordModel<RoleModel>>(Constants.ClientRoutes.Roles);
            // var terminalRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);

            ViewBag.RoleId = new SelectList(stateRequest.Object.Items, "Id", "Name", selectedRoleId);
            ViewData["RolesList"] = new SelectList(stateRequest.Object.Items, "Id", "Name");
            // ViewBag.TerminalId = new SelectList(terminalRequest.Object.Items, "Id", "Name", selectedTerminalId);
        }

        [HttpGet]
        public async Task<IActionResult> FetchDetailDropDown(string dropdesc, int id)
        {
            //int Type
            string tenantid = User.FindFirst("preferred_username")?.Value;
            string locationid = User.FindFirst("location")?.Value;
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (dropdesc == "WarehouseList")
            {
                var wareHouse = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);
                var itemWareHouse = wareHouse?.Object?.Items;
                ViewBag.WarehouseList = new SelectList(wareHouse.Object.Items, "Id", "Name");
                var model = new SelectList(itemWareHouse, "Id", "WarehouseName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "RoleList")
            {
                var RoleList = await client.GetAsync<PagedRecordModel<RoleModel>>(Constants.ClientRoutes.Roles);
                var itemRoleList = RoleList?.Object?.Items.Where(x => x.CompanyInfoId == Convert.ToInt32(tenantid));
                var model = new SelectList(itemRoleList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "TerminalList")
            {
                var TerminalList = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
                var itemTerminalList = TerminalList?.Object?.Items;
                var model = new SelectList(itemTerminalList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "WarehouseBinList")
            {
                var bins = await client.GetAsync<IEnumerable<WarehouseBinDTO>>(string.Format(Constants.ClientRoutes.GetBinByWarehouse, id));

                var binList = bins?.Object;
                var model = new SelectList(binList, "Id", "WarehouseBinName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "ItemCategoryList")
            {
                var ItemCategoryList = await client.GetAsync<PagedRecordModel<ItemCategoryDTO>>(Constants.ClientRoutes.InventorySetupCategory);
                var itemCategoryList = ItemCategoryList?.Object?.Items;
                var model = new SelectList(itemCategoryList, "Id", "CategoryName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "ItemFamilyList")
            {
                var ItemFamilyList = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(Constants.ClientRoutes.InventorySetupFamilies);
                var itemFamilyList = ItemFamilyList?.Object?.Items;
                var model = new SelectList(itemFamilyList, "Id", "FamilyDescription");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "InventoryAccountList")
            {
                var InventoryAccountList = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
                var inventoryAccountList = InventoryAccountList?.Object?.Items;
                var model = new SelectList(inventoryAccountList, "Id", "CGLAccountNumber");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "InventoryItemList")
            {
                var InventoryItemList = await client.GetAsync<PagedRecordModel<InventoryItemDTO>>(Constants.ClientRoutes.InventorySetup);
                var inventoryItemList = InventoryItemList?.Object?.Items;
                var model = new SelectList(inventoryItemList, "Id", "ItemName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "EmployeeList")
            {
                var EmployeeList = await client.GetAsync<PagedRecordModel<EmployeeModel>>(Constants.ClientRoutes.Employees);
                var employeeList = EmployeeList?.Object?.Items;
                var model = new SelectList(employeeList, "Id", "FullName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "VehicleList")
            {
                var VehicleList = await client.GetAsync<PagedRecordModel<Vehicle>>(Constants.ClientRoutes.Vehicles);
                //var result = await client.GetAsync<PagedRecordModel<Vehicle>>(url);
                var vehicleList = VehicleList?.Object?.Items;
                var model = new SelectList(vehicleList, "Id", "RegistrationNumber");
                return Json(new
                {
                    data = model
                });
            }
            else
            {
            }
            return null;
        }

        [HttpGet]
        public async Task<IActionResult> EmployeesUpdate(int id)
        {

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.EmployeeGet, id);
            var result = await client.GetAsync<EmployeeModel>(url);

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EmployeesUpdate(int id, EmployeeModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                model.DeviceType = DeviceType.AdminWeb;
                // for picture insertion
                List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
                if (model.FilePhoto != null)
                {
                    var filedata = await InfoForImage(model.FilePhoto, "Photo");
                    imageFileDTO.Add(filedata);
                    model.EmployeePhoto = "Photo";
                }
                model.FilePhoto = null;
                model.PictureList = imageFileDTO;

                var url = string.Format(Constants.ClientRoutes.EmployeeUpdate, id);

                var result = await client.PutAsync<EmployeeModel, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("driverindex");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EmployeesDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.EmployeeDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }


        public IActionResult Dashboard()
        {
            return View();
        }


        #endregion

        #endregion
    }
}