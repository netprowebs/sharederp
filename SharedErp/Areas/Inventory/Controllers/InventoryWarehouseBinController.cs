﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Security.Models;

namespace SharedErp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class InventoryWarehouseBinController : Controller
    {

        private readonly IHttpClientFactory _httpClientFactory;

        public InventoryWarehouseBinController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllWarehouseBins(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetupWarehouseBin,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<WarehouseBinDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IEnumerable<WarehouseBinDTO>> GetBins(int warehouseId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var bins = await client.GetAsync<IEnumerable<WarehouseBinDTO>>(string.Format(Constants.ClientRoutes.GetBinByWarehouse, warehouseId));

            var items = bins.Object;

            return items;
        }

        [HttpGet]
        public async Task<IActionResult> Createe()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var wareHouse = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);

            var unique = wareHouse?.Object?.Items;



            ViewData["Warehouse"] = new SelectList(unique, "Id", "WarehouseName");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Createe(WarehouseBinDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                //model.WarehouseID = 1;
                var result = await client.PostAsJsonAsync<WarehouseBinDTO, bool>(Constants.ClientRoutes.InventorySetupWarehouseBinCreate, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("create", "inventorysetup");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var wareHouse = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);
            var location = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            var binTypes = await client.GetAsync<PagedRecordModel<WarehouseBinTypeDTO>>(Constants.ClientRoutes.InventorySetupWarehouseBinType);

            var unique = wareHouse?.Object?.Items;
            var binLocation = location?.Object?.Items;
            var binType = binTypes?.Object?.Items;

            ViewData["Warehouse"] = new SelectList(unique, "Id", "WarehouseName");
            ViewData["BinLocation"] = new SelectList(binLocation, "Name", "Name");
            ViewData["BinTypes"] = new SelectList(binType, "WarehouseBinTypeName", "WarehouseBinTypeName");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(WarehouseBinDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                //model.WarehouseID = 1;
                var result = await client.PostAsJsonAsync<WarehouseBinDTO, bool>(Constants.ClientRoutes.InventorySetupWarehouseBinCreate, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var wareHouse = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);

            var unique = wareHouse?.Object?.Items;



            ViewData["Warehouse"] = new SelectList(unique, "Id", "WarehouseName");

            var url = string.Format(Constants.ClientRoutes.InventorySetupWarehouseBinGet, id);
            var result = await client.GetAsync<WarehouseBinDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, WarehouseBinDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.InventorySetupWarehouseBinUpdate, id);

                var result = await client.PutAsync<WarehouseBinDTO, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupWarehouseBinDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
    }
}
