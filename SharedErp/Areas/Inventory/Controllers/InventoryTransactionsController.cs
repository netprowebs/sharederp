﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SharedErp.Areas.Admin.Controllers
{
    [Area("Inventory")]
    public class InventoryTransactionsController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public InventoryTransactionsController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        // commit
        #region InventoryRecieveheader
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.ReceivedHeader,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<InventoryReceivedHeaderModel>>(url);
            // geting existing permissions or claims for the logged in user  
            var hasCanInvSave = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvSave");
            var hasCanInvVerify = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvVerify");
            var hasCanInvApprove = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvApprove");

            // this condition is checking if the loggedin user have cansave record permission with one extra permission
            if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == false)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == false && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else if (hasCanInvSave == true && hasCanInvVerify == true && hasCanInvApprove == false)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == true && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == true)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == true && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
                return new DataTablesJsonResult(response, true);
            }


        }


        public async Task<IActionResult> Create()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            await FetchRequiredDropDowns();
            ViewBag.StartDate = DateTime.Now.Date;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateHeader(InventoryReceivedHeaderModel model)
        {
            bool items = false;

            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                // generate header id so that it can be used by the detail
                var hid = Guid.NewGuid();
                TempData["ID"] = hid;

                var InvRecHeaderModel = new InventoryReceivedHeaderModel
                {
                    Id = hid,
                    WarehouseID = model.WarehouseID,
                    WarehouseBinID = model.WarehouseBinID,
                    VehicleRegistration = model.VehicleRegistration,
                    DriversId = model.DriversId,
                    Notes = model.Notes,
                    Reference = model.Reference,
                    TransactionDate = model.TransactionDate
                };

                var result = await client.PostAsJsonAsync<InventoryReceivedHeaderModel, bool>(Constants.ClientRoutes.ReceivedHeaderCreate, InvRecHeaderModel);
                items = result.Object;

                if (result.IsValid && result.Object)
                {
                    //return RedirectToAction("index");
                    //return View(items);                  
                    var models = InvRecHeaderModel;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }



        private async Task FetchRequiredDropDowns(int? selectedLocationId = null, int? selectedVehicle = null, int? selectedDriverId = null,
        int? selectedWarehouse = null, int? selectedWarehousebin = null, int? selectedFranchiseId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var itemType = await client.GetAsync<PagedRecordModel<ItemTypeDTO>>(Constants.ClientRoutes.InventorySetupItemType);
            var itemCategory = await client.GetAsync<PagedRecordModel<ItemCategoryDTO>>(Constants.ClientRoutes.InventorySetupCategory);
            var itemFamily = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(Constants.ClientRoutes.InventorySetupFamilies);
            var wareHouse = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);
            var wareHouseBin = await client.GetAsync<PagedRecordModel<WarehouseBinDTO>>(Constants.ClientRoutes.InventorySetupWarehouseBin);
            var GLAccount = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
            //var vendors = await client.GetAsync<PagedRecordModel<VendorInformationModel>>(Constants.ClientRoutes.VendorInfos);

            //var unique = itemType?.Object?.Items;
            //var InventoryCategory = itemCategory?.Object?.Items;
            //var InventoryFamily = itemFamily?.Object?.Items;
            //var itemWareHouse = wareHouse?.Object?.Items;
            //var itemWareHouseBin = wareHouseBin?.Object?.Items;
            //var salesAccount = itemSales?.Object?.Items;


            ViewBag.wareHouse = new SelectList(wareHouse.Object.Items, "Id", "WarehouseName", selectedWarehouse);
            ViewBag.wareHousebin = new SelectList(wareHouseBin.Object.Items, "Id", "WarehouseBinName", selectedWarehousebin);

            ViewBag.itemType = new SelectList(itemType.Object.Items, "Id", "ItemTypeName", selectedWarehouse);
            ViewBag.itemCategory = new SelectList(itemCategory.Object.Items, "Id", "CategoryName", selectedWarehousebin);

            ViewBag.itemFamily = new SelectList(itemFamily.Object.Items, "Id", "FamilyDescription", selectedWarehouse);
            ViewBag.GLAccount = new SelectList(GLAccount.Object.Items, "Id", "CGLAccountNumber", selectedWarehousebin);

            //ViewBag.DriverId = new SelectList(driverRequest.Object.Items, "Id", "Name", selectedDriverId);

        }




        [HttpPost]
        public async Task<IActionResult> GetReceiveHeaderbyId(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.ReceivedHeaderGet, id);
            var result = await client.GetAsync<InventoryReceivedHeaderModel>(url);

            var model = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = model
                });

            }

            return NotFound();
        }


        [HttpGet]
        public async Task<IActionResult> Update(Guid id)
        {
            if (id == Guid.Empty)
            {
                id = (Guid)TempData["ID"];
            }


            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            InventoryTransactionModel AllModel = new InventoryTransactionModel();
            var url = string.Format(Constants.ClientRoutes.ReceivedHeaderGet, id);
            var result = await client.GetAsync<InventoryReceivedHeaderModel>(url);
            AllModel.InventoryReceivedHeaderModel = result.Object;
            if (result != null)
            {
                await FetchRequiredDropDowns();
                TempData["vid"] = id;
                return View(AllModel.InventoryReceivedHeaderModel);
            }
            ViewBag.StartDate = DateTime.Now.Date;
            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> UpdateHeader(InventoryReceivedHeaderModel model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var InvRecHeaderModel = new InventoryReceivedHeaderModel
                {
                    Id = model.Id,
                    WarehouseID = model.WarehouseID,
                    WarehouseBinID = model.WarehouseBinID,
                    VehicleRegistration = model.VehicleRegistration,
                    DriversId = model.DriversId,
                    Notes = model.Notes,
                    Reference = model.Reference,
                    TransactionDate = model.TransactionDate,
                    AdjustmentTypeID = 1,
                    Captured = model.Captured
                };
                var url = string.Format(Constants.ClientRoutes.ReceivedHeaderUpdate, model.Id);

                var result = await client.PutAsync<InventoryReceivedHeaderModel, bool>(url, InvRecHeaderModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteHeader(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.ReceivedHeaderDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            var models = result.Object;

            if (models != false)
            {
                return Json(new
                {
                    data = models
                });

            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Verify(Guid id, InventoryReceivedHeaderModel model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InvRecVerify, id);

            var result = await client.PutAsync<InventoryReceivedHeaderModel, bool>(url, model);

            //return RedirectToAction("index");
            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();

        }

        [HttpPost]
        public async Task<IActionResult> Approve(Guid id, InventoryReceivedHeaderModel model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InvRecApprove, id);

            var result = await client.PutAsync<InventoryReceivedHeaderModel, bool>(url, model);
            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Recieve(Guid id, InventoryReceivedHeaderModel model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InvReceive, id);

            var result = await client.PutAsync<InventoryReceivedHeaderModel, bool>(url, model);
            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Return(Guid id, int Rttype, InventoryReceivedHeaderModel model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            InventoryReceivedHeaderModel Modelb = new InventoryReceivedHeaderModel();
            Modelb.SiteNumber = Rttype.ToString();
            var url = string.Format(Constants.ClientRoutes.InvRecReturn, id);

            var result = await client.PutAsync<InventoryReceivedHeaderModel, bool>(url, Modelb);

            var models = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = models
                });

            }

            return NotFound();
        }


        #endregion InventoryReceiveHeader


        #region InventoryReceiveDetail

        [HttpGet]
        public async Task<IActionResult> GetDetails(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.ReceivedDetail,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<InventoryReceivedDetailModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }





        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AddReceiveDetail(InventoryReceivedDetailModel model)
        {
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<InventoryReceivedDetailModel, bool>(Constants.ClientRoutes.ReceivedDetailCreate, model);

                if (result.IsValid && result.Object)
                {
                    var models = result;
                    return Json(new
                    {
                        data = models

                    });

                    //return RedirectToAction("Update");
                    //return View(model);
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> UpdateReceiveDetail(InventoryReceivedDetailModel model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var InvRecDetailModel = new InventoryReceivedDetailModel
                {
                    Id = model.Id,
                    WarehouseID = model.WarehouseID,
                    WarehouseBinID = model.WarehouseBinID,
                    ItemUPCCode = model.ItemUPCCode,
                    ItemID = model.ItemID,
                    ReceivedQty = model.ReceivedQty,
                    ItemCost = model.ItemCost,
                    GLExpenseAccount = model.GLExpenseAccount,
                    PONumber = model.PONumber,
                    InventoryReceivedID = model.InventoryReceivedID,
                };
                var url = string.Format(Constants.ClientRoutes.ReceivedDetailUpdate, model.Id);

                var result = await client.PutAsync<InventoryReceivedDetailModel, bool>(url, InvRecDetailModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        public async Task<IActionResult> DeleteDetail(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.ReceivedDetailDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpPost]
        public async Task<List<InventoryReceivedDetailModel>> GetDetail(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var details = await client.GetAsync<InventoryReceivedDetailModel>($"{Constants.ClientRoutes.ReceivedDetailGet}");

            var url = string.Format(Constants.ClientRoutes.ReceivedDetailGet, id);

            var result = await client.GetAsync<List<InventoryReceivedDetailModel>>(url);
            var model = result.Object;
            return model;

        }



        #endregion InventoryReceiveDetail




    }
}
