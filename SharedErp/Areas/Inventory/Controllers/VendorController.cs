﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;

namespace SharedErp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class VendorController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public VendorController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }

        //[HttpGet]
        //public async Task<IActionResult> GetAllVendors(IDataTablesRequest request)
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    var page = (request.Start / request.Length) + 1;
        //    var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Vendors,
        //        page, request.Length, request.Search.Value);

        //    var result = await client.GetAsync<PagedRecordModel<VendorModel>>(url);

        //    var pagedData = result.Object;

        //    var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
        //    return new DataTablesJsonResult(response, true);
        //}
    }
}
