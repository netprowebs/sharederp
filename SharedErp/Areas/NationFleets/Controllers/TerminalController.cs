﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Security.Models;
using SharedErp.Models;
using SharedErp.Utils;
using SharedErp.Utils.Alerts;

namespace SharedErp.Areas.NationFleets.Controllers
{
    [Area("NationFleets")]
    public class TerminalController : Controller
    {

        private readonly IHttpClientFactory _httpClientFactory;

        public TerminalController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }


        public IActionResult Index()
        {
            return View();
        }

        #region Fetch Dropdowns

        [HttpGet]
        public async Task<IActionResult> FetchDetailDropDown(string dropdesc, int id)
        {
            //int Type
            string tenantid = User.FindFirst("preferred_username")?.Value;
            string locationid = User.FindFirst("location")?.Value;
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (dropdesc == "StateList")
            {
                var state = await client.GetAsync<PagedRecordModel<StateModel>>(Constants.ClientRoutes.States);
                var itemState = state?.Object?.Items;
                //ViewBag.StateList = new SelectList(state.Object.Items, "Id", "Name");
                var model = new SelectList(itemState, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "RegionList")
            {
                var RegionList = await client.GetAsync<PagedRecordModel<RegionDTO>>(Constants.ClientRoutes.Regions);
                var itemRegionList = RegionList?.Object?.Items;
                var model = new SelectList(itemRegionList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "TerminalList")
            {
                var TerminalList = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
                var itemTerminalList = TerminalList?.Object?.Items;
                var model = new SelectList(itemTerminalList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "WarehouseBinList")
            {
                var bins = await client.GetAsync<IEnumerable<WarehouseBinDTO>>(string.Format(Constants.ClientRoutes.GetBinByWarehouse, id));

                var binList = bins?.Object;
                var model = new SelectList(binList, "Id", "WarehouseBinName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "RouteList")
            {
                var ItemCategoryList = await client.GetAsync<PagedRecordModel<RouteDTO>>(Constants.ClientRoutes.Routes);
                var itemCategoryList = ItemCategoryList?.Object?.Items;
                var model = new SelectList(itemCategoryList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "ItemFamilyList")
            {
                var ItemFamilyList = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(Constants.ClientRoutes.InventorySetupFamilies);
                var itemFamilyList = ItemFamilyList?.Object?.Items;
                var model = new SelectList(itemFamilyList, "Id", "FamilyDescription");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "InventoryAccountList")
            {
                var InventoryAccountList = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
                var inventoryAccountList = InventoryAccountList?.Object?.Items;
                var model = new SelectList(inventoryAccountList, "Id", "CGLAccountNumber");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "InventoryItemList")
            {
                var InventoryItemList = await client.GetAsync<PagedRecordModel<InventoryItemDTO>>(Constants.ClientRoutes.InventorySetup);
                var inventoryItemList = InventoryItemList?.Object?.Items;
                var model = new SelectList(inventoryItemList, "Id", "ItemName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "EmployeeList")
            {
                var EmployeeList = await client.GetAsync<PagedRecordModel<EmployeeModel>>(Constants.ClientRoutes.Employees);
                var employeeList = EmployeeList?.Object?.Items;
                var model = new SelectList(employeeList, "Id", "FullName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "DriverList")
            {
                var EmployeeList = await client.GetAsync<PagedRecordModel<EmployeeModel>>(Constants.ClientRoutes.Employees);
                var employeeList = EmployeeList?.Object?.Items;
                var model = new SelectList(employeeList, "Id", "FullName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "VehicleList")
            {
                var VehicleList = await client.GetAsync<PagedRecordModel<Vehicle>>(Constants.ClientRoutes.Vehicle);
                var vehicleList = VehicleList?.Object?.Items;
                var model = new SelectList(vehicleList, "Id", "RegistrationNumber");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "VehicleModelList")
            {
                var VehicleModelList = await client.GetAsync<PagedRecordModel<VehicleModel>>(Constants.ClientRoutes.VehicleModels);
                var vehicleModelList = VehicleModelList?.Object?.Items;
                var model = new SelectList(vehicleModelList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "RouteListing")
            {
                var RouteList = await client.GetAsync<PagedRecordModel<RouteDTO>>(Constants.ClientRoutes.Routes);
                var routeList = RouteList?.Object?.Items;
                var model = new SelectList(routeList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else
            {
            }
            return null;
        }
        #endregion

        #region Terminal

        [HttpGet]
        public async Task<IActionResult> TerminalGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Terminals,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<TerminalModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }


        [HttpGet]
        public async Task<IActionResult> TerminalGetDropDown(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Terminals,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<TerminalModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);

            return new DataTablesJsonResult(response, true);

        }

        public async Task<IActionResult> TerminalSave(int Id, string Code, string Name, string Address, string ContactPerson, int StateId, int Type)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            TerminalModel model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new TerminalModel
            {
                Code = Code,
                Name = Name,
                Address = Address,
                ContactPerson = ContactPerson,
                StateId = StateId,
                // TerminalType = type
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<TerminalModel, bool>(Constants.ClientRoutes.TerminalCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.TerminalUpdate, Id);
                var result = await client.PutAsync<TerminalModel, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("index");
        }


        public async Task<IActionResult> TerminalUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.TerminalUpdate, id);
            var result = await client.GetAsync<TerminalModel>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> TerminalDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.TerminalDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
        #endregion

        #region Region

        [HttpGet]
        public async Task<IActionResult> RegionGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Regions,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<RegionDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }


        [HttpGet]
        public async Task<IActionResult> RegionGetDropDown(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Regions,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<RegionDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);

            return new DataTablesJsonResult(response, true);

        }

        public async Task<IActionResult> RegionSave(int Id, string Code, string Name, string Address1)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            RegionDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new RegionDTO
            {
                Name = Name,
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<RegionDTO, bool>(Constants.ClientRoutes.TerminalCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.TerminalUpdate, Id);
                var result = await client.PutAsync<RegionDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("index");
        }


        public async Task<IActionResult> RegionUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RegionUpdate, id);
            var result = await client.GetAsync<RegionDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> RegionDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RegionDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
        #endregion

        #region State

        [HttpGet]
        public async Task<IActionResult> StateGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.States,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<StateModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }


        [HttpGet]
        public async Task<IActionResult> StateGetDropDown(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.States,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<StateModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);

            return new DataTablesJsonResult(response, true);

        }

        public async Task<IActionResult> StateSave(int Id, int RegionId, string Name, string RegionName)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            StateModel model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new StateModel
            {
                Name = Name,
                RegionId = RegionId,
                RegionName = RegionName
                //TransactionDate = TransDate.GetValueOrDefault()
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<StateModel, bool>(Constants.ClientRoutes.StateCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.StateUpdate, Id);
                var result = await client.PutAsync<StateModel, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return RedirectToAction("index");
        }


        public async Task<IActionResult> StateUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.StateUpdate, id);
            var result = await client.GetAsync<StateModel>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> StateDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.StateDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
        #endregion


    }
}
