﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Security.Models;
using SharedErp.Models;
using SharedErp.Utils;
using SharedErp.Utils.Alerts;

namespace SharedErp.Areas.NationFleets.Controllers
{
    [Area("NationFleets")]
    public class VehiclesController : Controller
    {

        private readonly IHttpClientFactory _httpClientFactory;

        public VehiclesController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }


        public IActionResult Index()
        {
            ViewBag.StartDate = DateTime.Now.Date;
            return View();
        }

        #region Fetch Dropdowns

        [HttpGet]
        public async Task<IActionResult> FetchDetailDropDown(string dropdesc, int id)
        {
            //int Type
            string tenantid = User.FindFirst("preferred_username")?.Value;
            string locationid = User.FindFirst("location")?.Value;
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (dropdesc == "StateList")
            {
                var state = await client.GetAsync<PagedRecordModel<StateModel>>(Constants.ClientRoutes.States);
                var itemState = state?.Object?.Items;
                //ViewBag.StateList = new SelectList(state.Object.Items, "Id", "Name");
                var model = new SelectList(itemState, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "RegionList")
            {
                var RegionList = await client.GetAsync<PagedRecordModel<RegionDTO>>(Constants.ClientRoutes.Regions);
                var itemRegionList = RegionList?.Object?.Items;
                var model = new SelectList(itemRegionList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "TerminalList")
            {
                var TerminalList = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
                var itemTerminalList = TerminalList?.Object?.Items;
                var model = new SelectList(itemTerminalList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "WarehouseBinList")
            {
                var bins = await client.GetAsync<IEnumerable<WarehouseBinDTO>>(string.Format(Constants.ClientRoutes.GetBinByWarehouse, id));

                var binList = bins?.Object;
                var model = new SelectList(binList, "Id", "WarehouseBinName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "RouteList")
            {
                var ItemCategoryList = await client.GetAsync<PagedRecordModel<RouteDTO>>(Constants.ClientRoutes.Routes);
                var itemCategoryList = ItemCategoryList?.Object?.Items;
                var model = new SelectList(itemCategoryList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "ItemFamilyList")
            {
                var ItemFamilyList = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(Constants.ClientRoutes.InventorySetupFamilies);
                var itemFamilyList = ItemFamilyList?.Object?.Items;
                var model = new SelectList(itemFamilyList, "Id", "FamilyDescription");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "InventoryAccountList")
            {
                var InventoryAccountList = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
                var inventoryAccountList = InventoryAccountList?.Object?.Items;
                var model = new SelectList(inventoryAccountList, "Id", "CGLAccountNumber");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "InventoryItemList")
            {
                var InventoryItemList = await client.GetAsync<PagedRecordModel<InventoryItemDTO>>(Constants.ClientRoutes.InventorySetup);
                var inventoryItemList = InventoryItemList?.Object?.Items;
                var model = new SelectList(inventoryItemList, "Id", "ItemName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "EmployeeList")
            {
                var EmployeeList = await client.GetAsync<PagedRecordModel<EmployeeModel>>(Constants.ClientRoutes.Employees);
                var employeeList = EmployeeList?.Object?.Items;
                var model = new SelectList(employeeList, "Id", "FullName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "VehicleList")
            {
                var VehicleList = await client.GetAsync<PagedRecordModel<Vehicle>>(Constants.ClientRoutes.Vehicle);
                var vehicleList = VehicleList?.Object?.Items;
                var model = new SelectList(vehicleList, "Id", "RegistrationNumber");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "VehicleModelList")
            {
                var VehicleModelList = await client.GetAsync<PagedRecordModel<VehicleModel>>(Constants.ClientRoutes.VehicleModels);
                var vehicleModelList = VehicleModelList?.Object?.Items;
                var model = new SelectList(vehicleModelList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "RouteListing")
            {
                var RouteList = await client.GetAsync<PagedRecordModel<RouteDTO>>(Constants.ClientRoutes.Routes);
                var routeList = RouteList?.Object?.Items;
                var model = new SelectList(routeList, "Id", "Name");
                return Json(new
                {
                    data = model
                });
            }
            else
            {
            }
            return null;
        }
        #endregion

        #region Vehicles

        [HttpGet]
        public async Task<IActionResult> VehiclesGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            
            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Vehicles,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<Vehicle>>(url);

            var pagedData = result.Object;
            ViewBag.StartDate = DateTime.Now.Date;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }


        public async Task<IActionResult> VehicleSave(Vehicle model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            IServiceResponse<bool> response = null;
            bool item = false;

            if (model.Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<Vehicle, bool>(Constants.ClientRoutes.VehicleCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.VehicleUpdate, model.Id);
                var result = await client.PutAsync<Vehicle, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return Json(new
            {
                data = item
            });
        }

        public async Task<IActionResult> VehicleUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VehicleGet, id);
            var result = await client.GetAsync<Vehicle>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> VehicleDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VehicleDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
        #endregion

        #region VehiclesModel

        [HttpGet]
        public async Task<IActionResult> VehicleModelGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.VehicleModels,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<VehicleModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }



        public async Task<IActionResult> VehicleModelSave(VehicleModel model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            IServiceResponse<bool> response = null;
            bool item = false;

            if (model.Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<VehicleModel, bool>(Constants.ClientRoutes.VehicleModelCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.VehicleUpdate, model.Id);
                var result = await client.PutAsync<VehicleModel, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return Json(new
            {
                data = item
            });
        }


        public async Task<IActionResult> VehicleModelUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VehicleModelGet, id);
            var result = await client.GetAsync<VehicleModel>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> VehicleModelDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VehicleModelDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
        #endregion


        #region VehicleMake

        [HttpGet]
        public async Task<IActionResult> VehicleMakeData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.VehicleMakes,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<VehicleMake>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }


  
        public async Task<IActionResult> VehicleMakeSave(VehicleMake model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            IServiceResponse<bool> response = null;
            bool item = false;

            if (model.Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<VehicleMake, bool>(Constants.ClientRoutes.VehicleMakeCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.VehicleMakeUpdate, model.Id);
                var result = await client.PutAsync<VehicleMake, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return Json(new
            {
                data = item
            });
        }


        public async Task<IActionResult> VehicleMakeUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VehicleMakeUpdate, id);
            var result = await client.GetAsync<VehicleMake>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> VehicleMakeDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.StateDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
        #endregion

    }
}
