﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Security.Models;
using SharedErp.Models;
using SharedErp.Utils;
using SharedErp.Utils.Alerts;

namespace SharedErp.Areas.NationFleets.Controllers
{
    [Area("NationFleets")]
    public class RouteController : Controller
    {

        private readonly IHttpClientFactory _httpClientFactory;

        public RouteController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }


        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Edit(int routeId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            TempData["vid"] = routeId;
            return View();
        }

        #region Route

        [HttpGet]
        public async Task<IActionResult> RouteGetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Routes,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<RouteDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }


  

        public async Task<IActionResult> RouteSave(RouteDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            IServiceResponse<bool> response = null;
            bool item = false;

            if (model.Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<RouteDTO, bool>(Constants.ClientRoutes.RouteCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.RouteUpdate, model.Id);
                var result = await client.PutAsync<RouteDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return Json(new
            {
                data = item
            });
        }


        public async Task<IActionResult> RouteUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RouteGet, id);
            var result = await client.GetAsync<RouteDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> RouteDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RouteDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

       
        #endregion



        #region Trip

        [HttpGet]
        public async Task<IActionResult> TripGetData(IDataTablesRequest request, int routeId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            TempData["vid"] = routeId;

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Trips,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<TripDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.RouteId == routeId));
            return new DataTablesJsonResult(response, true);
        }


        public async Task<IActionResult> TripSave(int Id, string Code, string ParentRouteDepartureTime, string DepartureTime, int RouteId,
            int vehicleModelId,bool AvailableOnline, int ParentRouteId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            TripDTO model = null;
            IServiceResponse<bool> response = null;
            bool item = false;

            model = new TripDTO
            {
                TripCode = Code,
                ParentRouteDepartureTime = ParentRouteDepartureTime,
                DepartureTime = DepartureTime,
                RouteId = RouteId,
                VehicleModelId = vehicleModelId,
                AvailableOnline = AvailableOnline,
                ParentRouteId = ParentRouteId
            };

            if (Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<TripDTO, bool>(Constants.ClientRoutes.TripCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.TripUpdate, Id);
                var result = await client.PutAsync<TripDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return Json(new
            {
                data = item
            });
        }


        public async Task<IActionResult> TripUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.TripGet, id);
            var result = await client.GetAsync<TripDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> TripDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.TripDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
        #endregion


        #region FarebyModel

        [HttpGet]
        public async Task<IActionResult> FareGetData(IDataTablesRequest request, int routeId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            TempData["vid"] = routeId;

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Fares,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<FareDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.RouteId == routeId));
            return new DataTablesJsonResult(response, true);
        }


        public async Task<IActionResult> FareSave(FareDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            IServiceResponse<bool> response = null;
            bool item = false;

            if (model.Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<FareDTO, bool>(Constants.ClientRoutes.FareCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.FareUpdate, model.Id);
                var result = await client.PutAsync<FareDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return Json(new
            {
                data = item
            });
        }


        public async Task<IActionResult> FareUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.FareGet, id);
            var result = await client.GetAsync<FareDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> FareDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.FareDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
        #endregion


        #region SubRoutes

        [HttpGet]
        public async Task<IActionResult> SubRouteGetData(IDataTablesRequest request, int routeId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            TempData["vid"] = routeId;

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.SubRoutes,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<SubRouteDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.RouteId == routeId));
            return new DataTablesJsonResult(response, true);
        }


        public async Task<IActionResult> SubrouteSave(SubRouteDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            IServiceResponse<bool> response = null;
            bool item = false;

            if (model.Id == 0)
            {
                // Do save to database
                response = await client.PostAsJsonAsync<SubRouteDTO, bool>(Constants.ClientRoutes.SubRouteCreate, model);
                item = response.Object;

                if (response.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }
            else
            {
                // Do Update to database
                var url = string.Format(Constants.ClientRoutes.SubRouteUpdate, model.Id);
                var result = await client.PutAsync<SubRouteDTO, bool>(url, model);
                item = result.Object;

                if (result.ValidationErrors.Count > 0)
                {
                    return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
                }
            }

            return Json(new
            {
                data = item
            });
        }


        public async Task<IActionResult> SubrouteUpdate(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.SubRouteGet, id);
            var result = await client.GetAsync<SubRouteDTO>(url);
            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }


        [HttpPost]
        public async Task<IActionResult> SubrouteDelete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.SubRouteDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
        #endregion

    }
}
