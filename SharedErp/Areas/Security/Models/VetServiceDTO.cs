﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Security.Models
{
    public class VetServiceDTO
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ToVetDaysCount { get; set; }
        public string DateCreated { get; set; }
        public int CostInUnit { get; set; }
        public decimal CostInAmount { get; set; }
    }
}