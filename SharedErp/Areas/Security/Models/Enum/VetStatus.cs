﻿namespace SHB.Core.Entities.Enums
{
    public enum VetStatus
    {
        Pending = 0,
        VetInprogress = 1,
        InTransit = 2,
        Available = 3,
        Concluded = 4,
    }

    public enum VetterStatus
    {
        Pending = 0,
        Assigned = 1,
        Accepted = 2,
        InTransit = 3,
        Available = 4,
        Concluded = 5,
        Cancelled = 6,
        Accidented = 7
    }
  
}
