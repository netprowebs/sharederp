﻿using Microsoft.AspNetCore.Http;
using SharedErp.Areas.Admin.Models;
using SharedErp.Models.Enum;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SharedErp.Areas.Security.Models
{
    public class Vettee
    {
        public Guid Id { get; set; }
        public int TenantId { get; set; }
        public int ClientId { get; set; }
        public string ReferenceCode { get; set; }
        public string VetteeLastName { get; set; }
        public string VetteeFirstName { get; set; }
        public string VetteeOtherName { get; set; }
        public int VetServiceID { get; set; }
        public string VetService { get; set; }
        public string PhoneNumber { get; set; }
        public string Picture { get; set; }
        public string ImageType { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public decimal Amount { get; set; }
        public decimal Vat { get; set; }
        public int UnitUsed { get; set; }
        public VetStatus VetteeStatus { get; set; }
        public DeviceType LoginDeviceType { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public string VetAddress { get; set; }
        public float VetLatitude { get; set; }
        public float VetLongitude { get; set; }
        public string Response { get; set; }
        public string CouponCode { get; set; }
        public int VetListCount { get; set; }
        public DateTime VetStartdate { get; set; }
        public DateTime VetEnddate { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public DateTime CreatedTime { get; set; }
        public long? CreatedUserId { get; set; }
        public string VetGeoLink { get; set; }
        public string VetOwnerStatus { get; set; }
        public string VetPeriodOfstay { get; set; }
        public string VetGuardians { get; set; }
        public string VettersRemarks { get; set; }
        //public List<IFormFile> Photo { get; set; }
        //public List<IFormFile> Video { get; set; }

        public IFormFile Photo { get; set; }
        public IFormFile Video { get; set; }


        public List<ImageFileDTO> PictureList { get; set; }
        public string VetPhotoUpload { get; set; }
        public string VetVideoUpload { get; set; }
    }
}
