﻿using SharedErp.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Security.Models
{
    public class VetBundlesDTO
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Decimal UnitCounts { get; set; }
        public Decimal AdsPrice { get; set; }
        public Decimal Amount { get; set; }
        public FareAdjustmentType CalcType { get; set; }
        public bool IsMax { get; set; }
        public string DateCreated { get; set; } = DateTime.Now.ToString();
    }
}
