﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic;
using SharedErp.Areas.Security.Models;
using SharedErp.Models;
using SharedErp.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using SharedErp.Utils;
using DataTables.AspNet.AspNetCore;
using SharedErp.Areas.Admin.Models;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc.Rendering;
using SHB.Core.Entities.Enums;

namespace SharedErp.Areas.Security.Controllers
{

    [Area("Security")] 
    public class AccountController : Controller
    {
        private IHttpClientFactory _httpClientFactory;
        public AccountController( IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Dashboards()
        {
            HttpContext.Session.SetString("SectorId",SectorsName.Security.ToString());

            return View();
        }


        [HttpGet]
        public async Task<IActionResult> getpendingvets()
        {
            string tenantid = User.FindFirst("preferred_username")?.Value;
            var client = _httpClientFactory.CreateClient(Utils.Constants.ClientWithToken);
            var Allvetlist = await client.GetAsync<PagedRecordModel<Vettee>>(Utils.Constants.ClientRoutes.VetteeList);
            var itemState = Allvetlist?.Object?.Items.Where(x => (x.VetteeStatus == VetStatus.Pending || x.VetteeStatus == VetStatus.InTransit || x.VetteeStatus == VetStatus.Available || x.VetteeStatus == VetStatus.VetInprogress ) && x.TenantId == Convert.ToInt32(tenantid)).Count();
            var model = itemState;
            return Json(new
            {
                data = model
            });
        }


        [HttpGet]
        public async Task<IActionResult> getCompletedvets()
        {
            string tenantid = User.FindFirst("preferred_username")?.Value;
            var client = _httpClientFactory.CreateClient(Utils.Constants.ClientWithToken);
            var Allvetlist = await client.GetAsync<PagedRecordModel<Vettee>>(Utils.Constants.ClientRoutes.VetteeList);
            var itemState = Allvetlist?.Object?.Items.Where(x => x.VetteeStatus == VetStatus.Concluded && x.TenantId == Convert.ToInt32(tenantid)).Count();
            var model = itemState;
            return Json(new
            {
                data = model
            });
        }

        [HttpGet]
        public async Task<IActionResult> getUserWalletbal()
        {
            string tenantid = User.FindFirst("preferred_username")?.Value;
            string Userid = User.FindFirst("id")?.Value;
            var client = _httpClientFactory.CreateClient(Utils.Constants.ClientWithToken);
            var url = string.Format(Utils.Constants.ClientRoutes.GetUserWalletTrans, Userid);
            var Allwalltransaction = await client.GetAsync<PagedRecordModel<WalletTransactionDTO>>(url);
            var itemState = Allwalltransaction?.Object?.Items.Where(x => x.TenantId == Convert.ToInt32(tenantid)).LastOrDefault();
            var model = itemState?.WalletBalance;
            return Json(new
            {
                data = model
            });
        }
    }
}
