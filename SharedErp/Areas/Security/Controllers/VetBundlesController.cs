﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Security.Models;

namespace SharedErp.Areas.Security.Controllers
{
    [Area("Security")]
    public class VetBundlesController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public VetBundlesController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }

        #region vetBundles

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.VetBundles,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<VetBundlesDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VetBundlesDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<VetBundlesDTO, bool>(Constants.ClientRoutes.VetBundlesCreate, model);


                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VetBundlesGet, id);
            var result = await client.GetAsync<VetBundlesDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, VetBundlesDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.VetBundlesUpdate, id);

                var result = await client.PutAsync<VetBundlesDTO, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VetBundlesDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        #endregion
    }
}
