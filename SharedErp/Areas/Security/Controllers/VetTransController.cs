﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Admin.Models;
using SharedErp.Areas.Security.Models;
using SharedErp.Models;
using SharedErp.Utils;
using SharedErp.Utils.Alerts;
using SHB.Core.Entities.Enums;

namespace SharedErp.Areas.NationFleets.Controllers
{
    [Area("Security")]
    public class VetTransController : Controller
    {

        private readonly IHttpClientFactory _httpClientFactory;

        public VetTransController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }


        public IActionResult Index(string Stext, string fromDate, string toDate)
        {
            TempData["vStext"] = Stext;
            TempData["vdatefrom"] = fromDate;
            TempData["vdateto"] = toDate;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetData(string Str, string fromDate, string toDate, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);         
            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.VetterTransactions,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<VettersTransactionDTO>>(url);

            var pagedData = result.Object;

            DataTablesResponse response = null;

            var model = new DateModel
            {
                StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-60)),
                EndDate = Convert.ToDateTime(DateTime.Now).AddHours(23)
            };
            
            if (!string.IsNullOrEmpty(Str) && Str != "undefined")
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items.Where(x => x.ReferenceCode == Str));
            }
            else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined")
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items.Where(x => x.CreatedTime >= Convert.ToDateTime(fromDate) && x.CreatedTime <= Convert.ToDateTime(toDate)));
            }
            else
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items);
                //pagedData.Items.Where(x => x.CreatedTime >= model.StartDate && x.CreatedTime <= model.EndDate));
                //pagedData.Items.Where(x => x.CreatedTime >= Convert.ToDateTime("2021-06-27") && x.CreatedTime <= Convert.ToDateTime("2021-08-27")));
            }
            return new DataTablesJsonResult(response, true);
        }



        public async Task<IActionResult> VetterSave(IFormCollection data, Vettee model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            IServiceResponse<bool> response = null;
            bool item = false;

            List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
            if (model.Photo != null)
            {
                var filedata = await InfoForImage(model.Photo, "Photo");
                imageFileDTO.Add(filedata);
                model.VetPhotoUpload = "Photo";
            }
            if (model.Video != null)
            {
                var filedata = await InfoForImage(model.Video, "Video");
                imageFileDTO.Add(filedata);
                model.VetVideoUpload = "Video";
            }

            model.Photo = null;
            model.Video = null;
            model.PictureList = imageFileDTO;

            // Do Update to database
            var result = await client.PostAsJsonAsync<Vettee, bool>(Constants.ClientRoutes.UpdateVettersForm, model);
            item = result.Object;

            if (result.ValidationErrors.Count > 0)
            {
                return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return Json(new
            {
                data = item
            });
        }

        public async Task<ImageFileDTO> InfoForImage(IFormFile file, string txt)
        {
            IFormFile VetUpload = file;
            ImageFileDTO imageFileD = new ImageFileDTO();

            byte[] datas;
            using (var br = new BinaryReader(VetUpload.OpenReadStream()))
                datas = br.ReadBytes((int)VetUpload.OpenReadStream().Length);

            imageFileD.ContentType = VetUpload.ContentType;
            imageFileD.fileName = VetUpload.FileName;
            imageFileD.FilenameWithOutExt = Path.GetFileNameWithoutExtension(VetUpload.FileName);
            imageFileD.data = datas;
            imageFileD.ImgContent = txt;

            return imageFileD;
        }


        public async Task<IActionResult> Update(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var url = string.Format(Constants.ClientRoutes.VetteeGet, id);
            var result = await client.GetAsync<Vettee>(url);
            var model = result.Object;
            return Json(new
            {
                data = model
            });
        }



        [HttpPost]
        public async Task<IActionResult> VetterActionTo(string id, string ActionType, ActionDto model)
        {
            List<string> modellist = new List<string>();
            modellist.Add("Id");
            modellist[0] = id.ToString();

            ActionDto modelB = new ActionDto();
            modelB.modellist = modellist;
            modelB.EmployeeId = model.EmployeeId;
            modelB.ActionType = (VetterStatus)Convert.ToInt32(ActionType);  
            
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var url = string.Format(Constants.ClientRoutes.VetterActionTo);
            var result = await client.PutAsync<ActionDto, bool>(url, modelB);

            return Json(new
            {
                data = result
            });

            //return RedirectToAction("index");
        }






    }
}
