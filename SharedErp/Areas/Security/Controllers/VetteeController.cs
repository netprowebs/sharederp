﻿
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Security.Models;
using SharedErp.Areas.Admin.Models;
using System;

namespace SharedErp.Areas.Security.Controllers
{
    [Area("Security")]
    public class VetteeController : Controller
    {   
        private readonly IHttpClientFactory _httpClientFactory;

        public VetteeController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index(string Stext, string fromDate, string toDate)
        {
            TempData["vStext"] = Stext;
            TempData["vdatefrom"] = fromDate;
            TempData["vdateto"] = toDate;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetData(string Str, string fromDate, string toDate, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.VetteeList,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<Vettee>>(url);

            var pagedData = result.Object;

            DataTablesResponse response = null;

            var model = new DateModel
            {
                StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-60)),
                EndDate = Convert.ToDateTime(DateTime.Now).AddHours(23)
            };

            if (!string.IsNullOrEmpty(Str) && Str != "undefined")
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items.Where(x => x.ReferenceCode == Str));
            }
            else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined")
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items.Where(x => x.CreatedTime >= Convert.ToDateTime(fromDate) && x.CreatedTime <= Convert.ToDateTime(toDate)));
            }
            else
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items);
                //pagedData.Items.Where(x => x.CreatedTime >= model.StartDate && x.CreatedTime <= model.EndDate));
                //pagedData.Items.Where(x => x.CreatedTime >= Convert.ToDateTime("2021-06-27") && x.CreatedTime <= Convert.ToDateTime("2021-08-27")));
            }
            return new DataTablesJsonResult(response, true);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Vettee data)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var result = await client.PostAsJsonAsync<Vettee, bool>(Constants.ClientRoutes.VetteeCreate, data);
                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }

            }

            return View(data);
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VetteeGet, id);
            var result = await client.GetAsync<Vettee>(url);

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, Vettee model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.VetteeUpdate, id);

                var result = await client.PutAsync<Vettee, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VetteeDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

    }
}