﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using SharedErp.Models;
using SharedErp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Accounts.Models;

namespace SharedErp.Areas.NetAccounts.Controllers
{
    [Area("Accounts")]
    public class PurchaseController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public PurchaseController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index(string Stext, string fromDate, string toDate)
        {
            TempData["vStext"] = Stext;
            TempData["vdatefrom"] = fromDate;
            TempData["vdateto"] = toDate;
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(string Str, string fromDate, string toDate, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.PurchaseHeaders,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<PurchaseHeaderDTO>>(url);
            // geting existing permissions or claims for the logged in user  
            var hasCanInvSave = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvSave");
            var hasCanInvVerify = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvVerify");
            var hasCanInvApprove = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvApprove");

            // this condition is checking if the loggedin user have cansave record permission with one extra permission
            if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == false)
            {
                var pagedData = result.Object;
                DataTablesResponse response = null;
                if(request.Search.Value != null)
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber.Contains(request.Search.Value) && x.Captured == false && x.Verified == false && x.Approved == false));
                }
                else if (!string.IsNullOrEmpty(Str) && Str != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber == Str && x.Captured == false && x.Verified == false && x.Approved == false));
                }
                else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined" || !string.IsNullOrEmpty(toDate) && toDate != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.PurchaseDate >= Convert.ToDateTime(fromDate) || x.PurchaseDate <= Convert.ToDateTime(toDate) &&
                    x.Captured == false && x.Verified == false && x.Approved == false));
                }
                else
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.Captured == false && x.Verified == false && x.Approved == false));
                }
                return new DataTablesJsonResult(response, true);
            }
            else if (hasCanInvSave == true && hasCanInvVerify == true && hasCanInvApprove == false)
            {
                var pagedData = result.Object;
                DataTablesResponse response = null;
                if (request.Search.Value != null)
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber.Contains(request.Search.Value) && x.Captured == true && x.Verified == false && x.Approved == false));
                }
                else if (!string.IsNullOrEmpty(Str) && Str != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber == Str && x.Captured == true && x.Verified == false && x.Approved == false));
                }
                else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined" || !string.IsNullOrEmpty(toDate) && toDate != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.PurchaseDate >= Convert.ToDateTime(fromDate) || x.PurchaseDate <= Convert.ToDateTime(toDate)
                    && x.Captured == true && x.Verified == false && x.Approved == false));
                }
                else
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.Captured == true && x.Verified == false || x.Approved == false));
                }
                return new DataTablesJsonResult(response, true);
            }
            else if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == true)
            {
                var pagedData = result.Object;
                DataTablesResponse response = null;
                if (request.Search.Value != null)
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber.Contains(request.Search.Value) && x.Captured == true && x.Verified == false && x.Approved == false));
                }
                else if (!string.IsNullOrEmpty(Str) && Str != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber == Str && x.Captured == true && x.Verified == false && x.Approved == false));
                }
                else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined" || !string.IsNullOrEmpty(toDate) && toDate != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.PurchaseDate >= Convert.ToDateTime(fromDate) || x.PurchaseDate <= Convert.ToDateTime(toDate)
                    && x.Captured == true && x.Verified == false && x.Approved == false));
                }
                else
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.Captured == true && x.Verified == false && x.Approved == false));
                }
                return new DataTablesJsonResult(response, true);
            }
            else
            {
                var pagedData = result.Object;
                DataTablesResponse response = null;
                if (request.Search.Value != null)
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber.Contains(request.Search.Value)));
                }
                else if (!string.IsNullOrEmpty(Str) && Str != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber == Str));
                }
                else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined" || !string.IsNullOrEmpty(toDate) && toDate != "undefined")
                {
                    //fromDate = "2021-08-27";
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.PurchaseDate >= Convert.ToDateTime(fromDate) || x.PurchaseDate <= Convert.ToDateTime(toDate)));
                }
                else
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items);
                }
                return new DataTablesJsonResult(response, true);
            }


        }


        public IActionResult Create()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Update(Guid id)
        {
            if (id == Guid.Empty)
            {
                id = (Guid)TempData["ID"];
            }


            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
           // InventoryTransactionModel AllModel = new InventoryTransactionModel();
            var url = string.Format(Constants.ClientRoutes.PurchaseHeader, id);
            var result = await client.GetAsync<PurchaseHeaderDTO>(url);
            //AllModel.InventoryReceivedHeaderModel = result.Object;
            if (result != null)
            {
                //await FetchRequiredDropDowns();
                TempData["vid"] = id;
                return View(result.Object);
            }
            ViewBag.StartDate = DateTime.Now.Date;
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> DeleteHeader(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.PurchaseHeaderDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            var models = result.Object;

            if (models != false)
            {
                return Json(new
                {
                    data = models
                });

            }
            return NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> FetchDetailDropDown(string dropdesc, int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (dropdesc == "VendorList")
            {
                var TerminalList = await client.GetAsync<PagedRecordModel<VendorInformationDTO >>(Constants.ClientRoutes.VendorInfos);
                var itemTerminalList = TerminalList?.Object?.Items;
                var model = new SelectList(itemTerminalList, "Id", "VendorName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "InventoryAccountList")
            {
                var InventoryAccountList = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
                var inventoryAccountList = InventoryAccountList?.Object?.Items;
                var model = new SelectList(inventoryAccountList, "Id", "CGLAccountNumber");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "InventoryItemList")
            {
                var InventoryItemList = await client.GetAsync<PagedRecordModel<InventoryItemDTO>>(Constants.ClientRoutes.InventorySetup);
                var inventoryItemList = InventoryItemList?.Object?.Items;
                var model = new SelectList(inventoryItemList, "Id", "ItemName");
                return Json(new
                {
                    data = model
                });
            }
            return null;
        }

        [HttpPost]
        public async Task<IActionResult> CreateHeader(PurchaseHeaderDTO model)
        {
            bool items = false;

            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                // generate header id so that it can be used by the detail
                var hid = Guid.NewGuid();
                TempData["ID"] = hid;

                var PurchaseHeaderModel = new PurchaseHeaderDTO
                {
                    TenantId = hid.ToString(),
                    Id = hid,
                    VendorID = model.VendorID,
                    Reference = model.Reference,
                    PurchaseDate = model.PurchaseDate,
                    PaymentDate = model.PaymentDate
                };

                var result = await client.PostAsJsonAsync<PurchaseHeaderDTO, bool>(Constants.ClientRoutes.PurchaseHeaderCreate, PurchaseHeaderModel);
                items = result.Object;

                if (result.IsValid && result.Object)
                {
                    //return RedirectToAction("index");
                    //return View(items);                  
                    var models = PurchaseHeaderModel;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateHeader(PurchaseHeaderDTO model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var purchaseHeaderModel = new PurchaseHeaderDTO
                {
                    Id = model.Id,
                    TenantId = model.TenantId,
                    VendorID = model.VendorID,
                    Reference = model.Reference,
                    PurchaseDate = model.PurchaseDate,
                    PaymentDate = model.PaymentDate
                };
                var url = string.Format(Constants.ClientRoutes.PurchaseHeaderUpdate, model.Id);

                var result = await client.PutAsync<PurchaseHeaderDTO, bool>(url, purchaseHeaderModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPurchaseDetail(PurchaseDetailDTO model)
        {
                if (ModelState.IsValid)
                {
                bool items = false;
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var PurchaseDetailModel = new PurchaseDetailDTO
                {
                    WarehouseBinID = 2,
                    SerialNumber = "hh",
                    ItemUOM = 9,
                    ItemWeight = 4,
                    CurrencyID = 1,
                    CurrencyExcRate = 7,
                    TaxGroupID = 7,
                    GLCOGAccount = 9,
                    DiscountAmount = 3,
                    TenantId = 4,
                    TaxInclusive = true,
                    ItemUnitPrice = 8,
                    WarehouseID = 1,
                    ItemUPCCode = "jj",
                    ItemID = model.ItemID,
                    Description = model.Description,
                    OrderQty = model.OrderQty,
                    ItemUnitCost = model.ItemUnitCost,
                    GLSalesAccount = model.GLSalesAccount,
                    TaxAmount = model.TaxAmount,
                    PurchaseId = model.PurchaseId,
                };

                var result = await client.PostAsJsonAsync<PurchaseDetailDTO, bool>(Constants.ClientRoutes.PurchaseDetailCreate, PurchaseDetailModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                    {
                        var models = result;
                        return Json(new
                        {
                            data = models

                        });

                        //return RedirectToAction("Update");
                        //return View(model);
                    }
                    else
                    {
                        foreach (var item in result.ValidationErrors)
                        {
                            ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                        }

                        ModelState.AddModelError("", result.ShortDescription);
                    }
                }
            
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> GetPurchaseHeaderbyId(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.PurchaseHeader, id);
            var result = await client.GetAsync<PurchaseHeaderDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = model
                });

            }

            return NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> GetDetails(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.PurchaseDetail,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<PurchaseDetailDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }
    }
}
