﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SharedErp.Areas.Accounts.Models;
using SharedErp.Areas.Admin.Models;
using SharedErp.Models;
using SharedErp.Models.Enum;
using SharedErp.Utils;
using SharedErp.Utils.Alerts;

namespace SharedErp.Areas.NetAccounts.Controllers
{
    [Area("Accounts")]
    public class InvoiceController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public InvoiceController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index(string Stext, string fromDate, string toDate)
        {
            TempData["vStext"] = Stext;
            TempData["vdatefrom"] = fromDate;
            TempData["vdateto"] = toDate;
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        //public IActionResult Update()
        //{
        //    return View();
        //}

        [HttpPost]
        public async Task<IActionResult> GetData(string Str, string fromDate, string toDate, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InvoiceHeader,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<InvoiceHeaderDTO>>(url);

            var pagedData = result.Object;

            DataTablesResponse response = null;

            var model = new DateModel
            {
                StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-60)),
                EndDate = Convert.ToDateTime(DateTime.Now).AddHours(23)
            };

            if (!string.IsNullOrEmpty(Str) && Str != "undefined")
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items.Where(x => x.InvoiceNumber == Str));
            }
            else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined")
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items.Where(x => x.InvoiceDate >= Convert.ToDateTime(fromDate) && x.InvoiceDate <= Convert.ToDateTime(toDate)));
            }
            else
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                pagedData.Items);
                //pagedData.Items.Where(x => x.CreatedTime >= model.StartDate && x.CreatedTime <= model.EndDate));
                //pagedData.Items.Where(x => x.CreatedTime >= Convert.ToDateTime("2021-06-27") && x.CreatedTime <= Convert.ToDateTime("2021-08-27")));
            }
            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        public async Task<IActionResult> CreateHeader(InvoiceHeaderDTO model)
        {
            bool items = false;

            if (!ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                // generate header id so that it can be used by the detail
                var hid = Guid.NewGuid();
                TempData["ID"] = hid;

                var InvRecHeaderModel = new InvoiceHeaderDTO
                {
                    Id = hid,
                    WarehouseID = model.WarehouseID,
                    PostedDate = model.PostedDate,
                    DueDate = model.DueDate,
                    UserId = model.UserId,
                    AccTranType = model.AccTranType,
                    Reference = model.Reference,
                };

                var result = await client.PostAsJsonAsync<InvoiceHeaderDTO, bool>(Constants.ClientRoutes.InvoiceHeaderCreate, InvRecHeaderModel);
                items = result.Object;

                if (result.IsValid && result.Object)
                {
                    //return RedirectToAction("index");
                    //return View(items);                  
                    var models = InvRecHeaderModel;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        public async Task<IActionResult> InvoiceSave(InvoiceHeaderDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            IServiceResponse<bool> response = null;
            bool item = false;
            // Do Update to database

            var url = string.Format(Constants.ClientRoutes.InvoiceHeaderUpdate, model.Id);
            var result = await client.PutAsync<InvoiceHeaderDTO, bool>(url, model);

            item = result.Object;

            if (result.ValidationErrors.Count > 0)
            {
                return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return Json(new
            {
                data = item
            });
        }



        //public async Task<IActionResult> Update(Guid id)
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    var url = string.Format(Constants.ClientRoutes.VetteeGet, id);
        //    var result = await client.GetAsync<InvoiceHeaderDTO>(url);
        //    var model = result.Object;
        //    return Json(new
        //    {
        //        data = model
        //    });
        //}

        [HttpGet]
        public async Task<IActionResult> Update(Guid id)
        {
            if (id == Guid.Empty)
            {
                id = (Guid)TempData["ID"];
            }


            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            InvoiceModel AllModel = new InvoiceModel();
            var url = string.Format(Constants.ClientRoutes.InvoiceHeaderGet, id);
            var result = await client.GetAsync<InvoiceHeaderDTO>(url);
            AllModel.InvoiceHeaderDTO = result.Object;
            if (result != null)
            {
                //await FetchRequiredDropDowns();
                TempData["vid"] = id;
                return View(AllModel.InvoiceHeaderDTO);
            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> GetInvoiceHeaderbyId(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InvoiceHeaderGet, id);
            var result = await client.GetAsync<InvoiceHeaderDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = model
                });

            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateHeader(InvoiceHeaderDTO model)
        {
            bool items = false;
            if (!ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var InvTransHeaderModel = new InvoiceHeaderDTO
                {
                    Id = model.Id,
                    WarehouseID = model.WarehouseID,
                    PostedDate = model.PostedDate,
                    DueDate = model.DueDate,
                    UserId = model.UserId,
                    AccTranType = model.AccTranType,
                    Reference = model.Reference,
                    Captured = model.Captured
                };
                var url = string.Format(Constants.ClientRoutes.InvoiceHeaderUpdate, model.Id);

                var result = await client.PutAsync<InvoiceHeaderDTO, bool>(url, InvTransHeaderModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> InvoiceActionTo(string id, string ActionType, InvoiceActionTypeDTO model)
        {
            List<string> modellist = new List<string>();
            modellist.Add("Id");
            modellist[0] = id.ToString();

            InvoiceActionTypeDTO modelB = new InvoiceActionTypeDTO();
            modelB.modellist = modellist;
            modelB.EmployeeId = model.EmployeeId;
            modelB.ActionType = (AccTransType)Convert.ToInt32(ActionType);

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var url = string.Format(Constants.ClientRoutes.VetterActionTo);
            var result = await client.PutAsync<InvoiceActionTypeDTO, bool>(url, modelB);

            return Json(new
            {
                data = result
            });

            //return RedirectToAction("index");
        }

        #region Invoice Details


        [HttpGet]
        public async Task<IActionResult> GetDetails(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InvoiceDetail,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<InvoiceDetailDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }


        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AddInvoiceDetail(InvoiceDetailDTO model)
        {
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<InvoiceDetailDTO, bool>(Constants.ClientRoutes.InvoiceDetailCreate, model);

                if (result.IsValid && result.Object)
                {
                    var models = result;
                    return Json(new
                    {
                        data = models

                    });

                    //return RedirectToAction("Update");
                    //return View(model);
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateInvoiceDetail(InvoiceDetailDTO model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var InvTransDetailModel = new InvoiceDetailDTO
                {
                    Id = model.Id,
                    WarehouseID = model.WarehouseID,
                    WarehouseBinID = model.WarehouseBinID,
                    ItemUPCCode = model.ItemUPCCode,
                    ItemID = model.ItemID,
                    OrderQty = model.OrderQty,
                    ItemUnitPrice = model.ItemUnitPrice,
                    TaxAmount = model.TaxAmount,
                    SerialNumber = model.SerialNumber,
                    Description = model.Description,
                    //PONumber = model.PONumber,
                    InvoiceId = model.InvoiceId,
                };
                var url = string.Format(Constants.ClientRoutes.InvoiceDetailUpdate, model.Id);

                var result = await client.PutAsync<InvoiceDetailDTO, bool>(url, InvTransDetailModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        public async Task<IActionResult> DeleteDetail(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InvoiceDetailDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpPost]
        public async Task<List<InvoiceDetailDTO>> GetDetail(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var details = await client.GetAsync<InventoryReceivedDetailModel>($"{Constants.ClientRoutes.ReceivedDetailGet}");

            var url = string.Format(Constants.ClientRoutes.InvoiceDetailGet, id);

            var result = await client.GetAsync<List<InvoiceDetailDTO>>(url);
            var model = result.Object;
            return model;

        }
        #endregion

    }
}
