﻿using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using SharedErp.Utils;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using SharedErp.Areas.Accounts.Models;
using SharedErp.Models;
using DataTables.AspNet.AspNetCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SharedErp.Areas.Accounts.Controllers
{
    [Area("Accounts")]
    public class ReceiptController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public ReceiptController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            //TempData["vStext"] = str;
            //TempData["vdatefrom"] = fromDate;
            //TempData["vdateto"] = toDate;

            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Update()
        {
            return View();
        }

        #region ReceiptHeader

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.ReceiptHeader,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<ReceiptHeader>>(url);
            // geting existing permissions or claims for the logged in user  
            var hasCanInvSave = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvSave");
            var hasCanInvVerify = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvVerify");
            var hasCanInvApprove = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvApprove");

            // this condition is checking if the loggedin user have cansave record permission with one extra permission
            if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == false)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == false && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else if (hasCanInvSave == true && hasCanInvVerify == true && hasCanInvApprove == false)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == true && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == true)
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == true && x.Verified == false && x.Approved == false));
                return new DataTablesJsonResult(response, true);
            }
            else
            {
                var pagedData = result.Object;
                var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
                return new DataTablesJsonResult(response, true);
            }


        }


        public async Task<IActionResult> CreateHeader()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            await FetchRequiredDropDowns();
            ViewBag.StartDate = DateTime.Now.Date;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateHeader(ReceiptHeader model)
        {
            bool items = false;

            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                // generate header id so that it can be used by the detail
                var hid = Guid.NewGuid();
                TempData["ID"] = hid;

                var receiptHeader = new ReceiptHeader
                {
                    Id = hid,
                    Reference = model.Reference,
                    TransactionDate = model.TransactionDate,
                    LodgementDate = model.LodgementDate,
                    Posted = model.Posted,
                    PostedBy = model.PostedBy,
                    PostedDate = model.PostedDate,
                    Approved = model.Approved,
                    ApprovedBy = model.ApprovedBy,
                    ApprovedDate = model.ApprovedDate,
                    TenantId = model.TenantId,
                    Captured = model.Captured,
                    CustomerID = model.CustomerID,
                    DueDate = model.DueDate,
                    ChequeNumber = model.ChequeNumber,
                    ReceiptNumber = model.ReceiptNumber,
                    ReceiptType = model.ReceiptType,
                    IsReverse = model.IsReverse,
                    LocationId = model.LocationId,
                    BankID = model.BankID,
                    Verified = model.Verified,
                    VerifiedBy = model.VerifiedBy,
                    VerifiedDate = model.VerifiedDate,
                    UserId = model.UserId,
                    Void = model.Void,
                    TotalAmount = model.TotalAmount
                    
                };

                var result = await client.PostAsJsonAsync<ReceiptHeader, bool>(Constants.ClientRoutes.ReceiptHeaderCreate, receiptHeader);
                items = result.Object;

                if (result.IsValid && result.Object)
                {
                    //return RedirectToAction("index");
                    //return View(items);                  
                    var models = receiptHeader;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        private async Task FetchRequiredDropDowns(int? selectedLocationId = null, int? selectedVehicle = null, int? selectedDriverId = null,
       int? selectedWarehouse = null, int? selectedWarehousebin = null, int? selectedFranchiseId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var itemType = await client.GetAsync<PagedRecordModel<ItemTypeDTO>>(Constants.ClientRoutes.InventorySetupItemType);
            var itemCategory = await client.GetAsync<PagedRecordModel<ItemCategoryDTO>>(Constants.ClientRoutes.InventorySetupCategory);
            var itemFamily = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(Constants.ClientRoutes.InventorySetupFamilies);
            var wareHouse = await client.GetAsync<PagedRecordModel<WarehouseDTO>>(Constants.ClientRoutes.InventorySetupWarehouse);
            var wareHouseBin = await client.GetAsync<PagedRecordModel<WarehouseBinDTO>>(Constants.ClientRoutes.InventorySetupWarehouseBin);
            var GLAccount = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
            //var vendors = await client.GetAsync<PagedRecordModel<VendorInformationModel>>(Constants.ClientRoutes.VendorInfos);

            //var unique = itemType?.Object?.Items;
            //var InventoryCategory = itemCategory?.Object?.Items;
            //var InventoryFamily = itemFamily?.Object?.Items;
            //var itemWareHouse = wareHouse?.Object?.Items;
            //var itemWareHouseBin = wareHouseBin?.Object?.Items;
            //var salesAccount = itemSales?.Object?.Items;


            ViewBag.wareHouse = new SelectList(wareHouse.Object.Items, "Id", "WarehouseName", selectedWarehouse);
            ViewBag.wareHousebin = new SelectList(wareHouseBin.Object.Items, "Id", "WarehouseBinName", selectedWarehousebin);

            ViewBag.itemType = new SelectList(itemType.Object.Items, "Id", "ItemTypeName", selectedWarehouse);
            ViewBag.itemCategory = new SelectList(itemCategory.Object.Items, "Id", "CategoryName", selectedWarehousebin);

            ViewBag.itemFamily = new SelectList(itemFamily.Object.Items, "Id", "FamilyDescription", selectedWarehouse);
            ViewBag.GLAccount = new SelectList(GLAccount.Object.Items, "Id", "CGLAccountNumber", selectedWarehousebin);

            //ViewBag.DriverId = new SelectList(driverRequest.Object.Items, "Id", "Name", selectedDriverId);

        }


        [HttpPost]
        public async Task<IActionResult> GetReceiptHeaderbyId(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.ReceivedHeaderGet, id);
            var result = await client.GetAsync<ReceiptHeader>(url);

            var model = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = model
                });

            }

            return NotFound();
        }


        [HttpGet]
        public async Task<IActionResult> Update(Guid id)
        {
            if (id == Guid.Empty)
            {
                id = (Guid)TempData["ID"];
            }


            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            ReceiptModel AllModel = new ReceiptModel();
            var url = string.Format(Constants.ClientRoutes.ReceiptHeaderGet, id);
            var result = await client.GetAsync<ReceiptHeader>(url);
            AllModel.ReceiptHeader = result.Object;
            if (result != null)
            {
                await FetchRequiredDropDowns();
                TempData["vid"] = id;
                return View(AllModel.ReceiptHeader);
            }
            ViewBag.StartDate = DateTime.Now.Date;
            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> UpdateHeader(ReceiptHeader model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var receiptHeader = new ReceiptHeader
                {
                    Id = model.Id,
                    Reference = model.Reference,
                    TransactionDate = model.TransactionDate,
                    LodgementDate = model.LodgementDate,
                    Posted = model.Posted,
                    PostedBy = model.PostedBy,
                    PostedDate = model.PostedDate,
                    Approved = model.Approved,
                    ApprovedBy = model.ApprovedBy,
                    ApprovedDate = model.ApprovedDate,
                    TenantId = model.TenantId,
                    Captured = model.Captured,
                    CustomerID = model.CustomerID,
                    DueDate = model.DueDate,
                    ChequeNumber = model.ChequeNumber,
                    ReceiptNumber = model.ReceiptNumber,
                    ReceiptType = model.ReceiptType,
                    IsReverse = model.IsReverse,
                    LocationId = model.LocationId,
                    BankID = model.BankID,
                    Verified = model.Verified,
                    VerifiedBy = model.VerifiedBy,
                    VerifiedDate = model.VerifiedDate,
                    UserId = model.UserId,
                    Void = model.Void,
                    TotalAmount = model.TotalAmount
                };
                var url = string.Format(Constants.ClientRoutes.ReceiptHeaderUpdate, model.Id);

                var result = await client.PutAsync<ReceiptHeader, bool>(url, receiptHeader);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteHeader(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.ReceiptHeaderDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            var models = result.Object;

            if (models != false)
            {
                return Json(new
                {
                    data = models
                });

            }
            return NotFound();
        }
        #endregion

        #region ReceiptDetails


        [HttpGet]
        public async Task<IActionResult> GetDetails(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.ReceiptDetail,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<ReceiptDetails>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }





        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AddReceiptDetail(ReceiptDetails model)
        {
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<ReceiptDetails, bool>(Constants.ClientRoutes.ReceiptDetailCreate, model);

                if (result.IsValid && result.Object)
                {
                    var models = result;
                    return Json(new
                    {
                        data = models

                    });

                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> UpdateReceiptDetail(ReceiptDetails model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var receiptDetails = new ReceiptDetails
                {
                    Id  = model.Id,
                    ReceiptId = model.ReceiptId,
                    TenantId = model.TenantId,
                    AssetId = model.AssetId,
                    AccountID = model.AccountID,
                    Amount = model.Amount,
                    CurrencyID = model.CurrencyID,
                    DocumentNo = model.DocumentNo,
                    ExchangeRate = model.ExchangeRate,
                    Narratives = model.Narratives,
                    ProjectId = model.ProjectId,
                    ReceiptHeader = model.ReceiptHeader,
                    TransType = model.TransType
                };
                var url = string.Format(Constants.ClientRoutes.ReceiptDetailUpdate, model.Id);

                var result = await client.PutAsync<ReceiptDetails, bool>(url, receiptDetails);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        public async Task<IActionResult> DeleteDetail(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.ReceiptDetailDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpPost]
        public async Task<List<ReceiptDetails>> GetDetailById(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.ReceiptDetailGet, id);

            var result = await client.GetAsync<List<ReceiptDetails>>(url);
            var model = result.Object;
            return model;

        }
        #endregion
    }
}
