﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SharedErp.Models.Enum;

namespace SharedErp.Areas.NetAccounts.Controllers
{
    [Area("Accounts")]
    public class AccountController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public AccountController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Dashboards()
        {
            HttpContext.Session.SetString("SectorId", SectorsName.AccountingAndPayroll.ToString());
            return View();
        }


    }
}
