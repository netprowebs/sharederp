﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SharedErp.Areas.Accounts.Models;
using SharedErp.Areas.Admin.Models;
using SharedErp.Models;
using SharedErp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SharedErp.Areas.Accounts.Controllers
{
    [Area("Accounts")]
    public class BillController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public BillController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index(string Stext, string fromDate, string toDate)
        {
            TempData["vStext"] = Stext;
            TempData["vdatefrom"] = fromDate;
            TempData["vdateto"] = toDate;
            return View();
        }

        #region BillHeader
        //[HttpGet]
        [HttpPost]
        public async Task<IActionResult> GetData(string Str, string fromDate, string toDate, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.BillHeader,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<BillsHeaderDTO>>(url);

            var pagedData = result.Object;

            DataTablesResponse response = null;


            var model = new DateModel
            {
                StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-60)),
                EndDate = Convert.ToDateTime(DateTime.Now).AddHours(23)
            };





            // geting existing permissions or claims for the logged in user  
            var hasCanInvSave = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvSave");
            var hasCanInvVerify = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvVerify");
            var hasCanInvApprove = User.HasClaim(c => c.Type == "Permission" && c.Value == "CanInvApprove");

            // this condition is checking if the loggedin user have cansave record permission with one extra permission
            if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == false)
            {
                if (request.Search.Value != null)
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber.Contains(request.Search.Value) && x.Captured == false && x.Verified == false && x.Approved == false));
                }

                if (!string.IsNullOrEmpty(Str) && Str != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber.Contains(Str) && x.Captured == false && x.Verified == false && x.Approved == false));
                }
                else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.PurchaseDate >= Convert.ToDateTime(fromDate) && x.PurchaseDate <= Convert.ToDateTime(toDate) && x.Captured == false && x.Verified == false && x.Approved == false));
                }
                else
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == false && x.Verified == false && x.Approved == false));

                }

            }
            else if (hasCanInvSave == true && hasCanInvVerify == true && hasCanInvApprove == false)
            {
                if (!string.IsNullOrEmpty(Str) && Str != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber == Str && x.Captured == true && x.Verified == false && x.Approved == false));
                }
                else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.PurchaseDate >= Convert.ToDateTime(fromDate) && x.PurchaseDate <= Convert.ToDateTime(toDate) && x.Captured == true && x.Verified == false && x.Approved == false));
                }
                else
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == true && x.Verified == false && x.Approved == false));

                }


            }
            else if (hasCanInvSave == true && hasCanInvVerify == false && hasCanInvApprove == true)
            {
                if (!string.IsNullOrEmpty(Str) && Str != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber == Str && x.Captured == true && x.Verified == false && x.Approved == false));
                }
                else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.PurchaseDate >= Convert.ToDateTime(fromDate) && x.PurchaseDate <= Convert.ToDateTime(toDate) && x.Captured == true && x.Verified == false && x.Approved == false));
                }
                else
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items.Where(x => x.Captured == true && x.Verified == false && x.Approved == false));
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Str) && Str != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.ContractNumber == Str));
                }
                else if (!string.IsNullOrEmpty(fromDate) && fromDate != "undefined")
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount,
                    pagedData.Items.Where(x => x.PurchaseDate >= Convert.ToDateTime(fromDate) && x.PurchaseDate <= Convert.ToDateTime(toDate)));
                }
                else
                {
                    response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
                }

            }

            return new DataTablesJsonResult(response, true);
        }


        public async Task<IActionResult> Create()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //await FetchRequiredDropDowns();
            ViewBag.StartDate = DateTime.Now.Date;
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> CreateHeader(BillsHeaderDTO model)
        {
            bool items = false;

            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                // generate header id so that it can be used by the detail
                var hid = Guid.NewGuid();
                TempData["ID"] = hid;

                var BillHeader = new BillsHeaderDTO
                {
                    Id = hid,
                    WarehouseID = model.WarehouseID,
                    VendorID = model.VendorID,
                    Reference = model.Reference,
                    PaymentDate = model.PaymentDate,
                    TenantId = model.TenantId,
                    PurchaseDate = model.PurchaseDate
                };

                var result = await client.PostAsJsonAsync<BillsHeaderDTO, bool>(Constants.ClientRoutes.BillHeaderCreate, BillHeader);
                items = result.Object;

                if (result.IsValid && result.Object)
                {
                    //return RedirectToAction("index");
                    //return View(items);                  
                    var models = BillHeader;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateHeader(BillsHeaderDTO model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var BillHeaderModel = new BillsHeaderDTO
                {
                    Id = model.Id,
                    WarehouseID = model.WarehouseID,
                    VendorID = model.VendorID,
                    Reference = model.Reference,
                    PaymentDate = model.PaymentDate,
                    PurchaseDate = model.PurchaseDate
                };
                var url = string.Format(Constants.ClientRoutes.BillHeaderUpdate, model.Id);

                var result = await client.PutAsync<BillsHeaderDTO, bool>(url, BillHeaderModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> FetchVendorDropDown(string dropdesc, int id)
        {
            //int Type
            string tenantid = User.FindFirst("preferred_username")?.Value;
            string locationid = User.FindFirst("location")?.Value;
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (dropdesc == "VendorList")
            {
                var VendList = await client.GetAsync<PagedRecordModel<VendorDTO>>(Constants.ClientRoutes.VendorInfos);
                var VendorList = VendList?.Object?.Items;
                var model = new SelectList(VendorList, "ID", "VendorName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "VendorItemList")
            {
                var InventoryItemList = await client.GetAsync<PagedRecordModel<InventoryItemDTO>>(Constants.ClientRoutes.InventorySetup);
                var inventoryItemList = InventoryItemList?.Object?.Items;
                var model = new SelectList(inventoryItemList, "Id", "ItemName");
                return Json(new
                {
                    data = model
                });
            }
            else if (dropdesc == "BillAccountList")
            {
                var InventoryAccountList = await client.GetAsync<PagedRecordModel<InventoryGeneralLedgerChartOfAccountDTO>>(Constants.ClientRoutes.InventorySetupGeneralLedger);
                var inventoryAccountList = InventoryAccountList?.Object?.Items;
                var model = new SelectList(inventoryAccountList, "Id", "CGLAccountNumber");
                return Json(new
                {
                    data = model
                });
            }


            return null;
        }

        [HttpPost]
        public async Task<IActionResult> GetBillHeaderbyId(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.BillHeaderGet, id);
            var result = await client.GetAsync<BillsHeaderDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return Json(new
                {
                    data = model
                });

            }

            return NotFound();
        }



        [HttpGet]
        public async Task<IActionResult> Update(Guid id)
        {
            if (id == Guid.Empty)
            {
                id = (Guid)TempData["ID"];
            }


            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            BillTransactionModel AllModel = new BillTransactionModel();
            var url = string.Format(Constants.ClientRoutes.BillHeaderById, id);
            var result = await client.GetAsync<BillsHeaderDTO>(url);
            AllModel.BillsHeaderDTO = result.Object;
            if (result != null)
            {
                //await FetchRequiredDropDowns();
                TempData["vid"] = id;
                return View(AllModel.BillsHeaderDTO);
            }
            ViewBag.StartDate = DateTime.Now.Date;
            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> DeleteHeader(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.BillHeaderDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            var models = result.Object;

            if (models != false)
            {
                return Json(new
                {
                    data = models
                });

            }
            return NotFound();
        }
        #endregion




        #region BillDetail

        //[HttpGet]
        //public async Task<IActionResult> GetDetails(IDataTablesRequest request)
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    var page = (request.Start / request.Length) + 1;
        //    var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.ReceivedDetail,
        //        page, request.Length, request.Search.Value);

        //    var result = await client.GetAsync<PagedRecordModel<InventoryReceivedDetailModel>>(url);

        //    var pagedData = result.Object;

        //    var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
        //    return new DataTablesJsonResult(response, true);
        //}



        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AddBillDetail(BillsDetailDTO model)
        {
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<BillsDetailDTO, bool>(Constants.ClientRoutes.BillDetailCreate, model);

                if (result.IsValid && result.Object)
                {
                    var models = result;
                    return Json(new
                    {
                        data = models

                    });

                    //return RedirectToAction("Update");
                    //return View(model);
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View(model);
        }



        [HttpPost]
        public async Task<List<BillsDetailDTO>> GetDetail(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var details = await client.GetAsync<InventoryReceivedDetailModel>($"{Constants.ClientRoutes.ReceivedDetailGet}");

            var url = string.Format(Constants.ClientRoutes.BillDetailGet, id);

            var result = await client.GetAsync<List<BillsDetailDTO>>(url);
            var model = result.Object;
            return model;

        }


        [HttpPost]
        public async Task<IActionResult> UpdateBillDetail(BillsDetailDTO model)
        {
            bool items = false;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var BillDetailModel = new BillsDetailDTO
                {
                    Id = model.Id,
                    Description = model.Description,
                    ItemUnitCost = model.ItemUnitCost,
                    ItemUnitPrice = model.ItemUnitPrice,
                    ItemID = model.ItemID,
                    OrderQty = model.OrderQty,
                    GLCOGAccount = model.GLCOGAccount
                };
                var url = string.Format(Constants.ClientRoutes.BillDetailUpdate, model.Id);

                var result = await client.PutAsync<BillsDetailDTO, bool>(url, BillDetailModel);
                items = result.Object;
                if (result.IsValid && result.Object)
                {
                    var models = items;
                    return Json(new
                    {
                        data = models
                    });
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }



        //[HttpPost]
        //public async Task<IActionResult> UpdateReceiveDetail(InventoryReceivedDetailModel model)
        //{
        //    bool items = false;
        //    if (ModelState.IsValid)
        //    {
        //        var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
        //        var InvRecDetailModel = new InventoryReceivedDetailModel
        //        {
        //            Id = model.Id,
        //            WarehouseID = model.WarehouseID,
        //            WarehouseBinID = model.WarehouseBinID,
        //            ItemUPCCode = model.ItemUPCCode,
        //            ItemID = model.ItemID,
        //            ReceivedQty = model.ReceivedQty,
        //            ItemCost = model.ItemCost,
        //            GLExpenseAccount = model.GLExpenseAccount,
        //            PONumber = model.PONumber,
        //            InventoryReceivedID = model.InventoryReceivedID,
        //        };
        //        var url = string.Format(Constants.ClientRoutes.ReceivedDetailUpdate, model.Id);

        //        var result = await client.PutAsync<InventoryReceivedDetailModel, bool>(url, InvRecDetailModel);
        //        items = result.Object;
        //        if (result.IsValid && result.Object)
        //        {
        //            var models = items;
        //            return Json(new
        //            {
        //                data = models
        //            });
        //        }
        //        else
        //        {
        //            foreach (var item in result.ValidationErrors)
        //            {
        //                ModelState.AddModelError(item.Key, string.Join(",", item.Value));
        //            }

        //            ModelState.AddModelError("", result.ShortDescription);
        //        }
        //    }

        //    return View(model);
        //}


        //public async Task<IActionResult> DeleteDetail(int id)
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    var url = string.Format(Constants.ClientRoutes.ReceivedDetailDelete, id);

        //    var result = await client.DeleteAsync<bool>(url);

        //    return Ok();
        //}

        //[HttpPost]
        //public async Task<List<InventoryReceivedDetailModel>> GetDetail(Guid id)
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    //var details = await client.GetAsync<InventoryReceivedDetailModel>($"{Constants.ClientRoutes.ReceivedDetailGet}");

        //    var url = string.Format(Constants.ClientRoutes.ReceivedDetailGet, id);

        //    var result = await client.GetAsync<List<InventoryReceivedDetailModel>>(url);
        //    var model = result.Object;
        //    return model;

        //}



        #endregion BillDetail

    }
}
