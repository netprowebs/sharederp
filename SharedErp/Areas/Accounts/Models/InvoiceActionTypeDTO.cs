﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Accounts.Models
{
    public class InvoiceActionTypeDTO
    {
            public List<string> modellist { get; set; }
            public AccTransType ActionType { get; set; }
            public int EmployeeId { get; set; }
    }
}
