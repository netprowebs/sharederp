﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Accounts.Models
{
    public class ReceiptDetails
    {
        public int Id { get; set; }
        public Guid ReceiptId { get; set; }
        public string ReceiptHeader { get; set; }
        public string TransType { get; set; }
        public int AccountID { get; set; }
        public string Narratives { get; set; }
        public string DocumentNo { get; set; }
        public decimal Amount { get; set; }
        public int CurrencyID { get; set; }
        public decimal ExchangeRate { get; set; }
        public int ProjectId { get; set; }
        public int AssetId { get; set; }
        public int TenantId { get; set; }
    }
}
