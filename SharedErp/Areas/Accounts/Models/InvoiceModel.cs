﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Accounts.Models
{
    public class InvoiceModel
    {
        public IEnumerable<InvoiceDetailDTO> InvoiceDetailDTO { get; set; }
        public InvoiceHeaderDTO InvoiceHeaderDTO { get; set; }
    }
}
