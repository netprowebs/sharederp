﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Accounts.Models
{
    public class ReceiptModel
    {
        public IEnumerable<ReceiptDetails> ReceiptDetails { get; set; }
        public ReceiptHeader ReceiptHeader { get; set; }
    }
}
