﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Accounts.Models
{
    public class ReceiptHeader
    {
        public Guid Id { get; set; }
        public int TenantId { get; set; }
        public string ReceiptNumber { get; set; }
        public string ReceiptType { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DueDate { get; set; }
        public string Reference { get; set; }
        public string ChequeNumber { get; set; }
        public int CustomerID { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LodgementDate { get; set; }
        public int BankID { get; set; }
        public decimal TotalAmount { get; set; }
        public int LocationId { get; set; }
        public int UserId { get; set; }
        public bool Captured { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime TransactionDate { get; set; }
        public bool Verified { get; set; }
        public string VerifiedBy { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime VerifiedDate { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public string PostedBy { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime PostedDate { get; set; }
        public bool IsReverse { get; set; }
        public bool Void { get; set; }
    }
}
