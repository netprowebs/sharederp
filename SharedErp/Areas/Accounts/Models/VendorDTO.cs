﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Accounts.Models
{
    public class VendorDTO
    {
        public int ID { get; set; }
        public int CompanyID { get; set; }
        public int? AccountStatus { get; set; }
        public string VendorName { get; set; }
        public string VendorAddress1 { get; set; }
        public string VendorAddress2 { get; set; }
        public string VendorAddress3 { get; set; }
        public string VendorCity { get; set; }
        public string VendorState { get; set; }
        public string VendorZip { get; set; }
        public string VendorCountry { get; set; }
        public string VendorPhone { get; set; }
        public string VendorFax { get; set; }
        public string VendorWebPage { get; set; }
        public int VendorTypeID { get; set; }
        public string AccountNumber { get; set; }
        public string ContactID { get; set; }
        public int? WarehouseID { get; set; }
        public string CurrencyID { get; set; }
        public string TermsID { get; set; }
        public string TermsStart { get; set; }
        public string GLPurchaseAccount { get; set; }
        public string TaxIDNo { get; set; }
        public string VATTaxIDNumber { get; set; }
        public string VatTaxOtherNumber { get; set; }
        public string TaxGroupID { get; set; }
        public string CreditLimit { get; set; }
        public string AvailibleCredit { get; set; }
        public string CreditComments { get; set; }
        public string CreditRating { get; set; }
        public string ApprovalDate { get; set; }
        public string CustomerSince { get; set; }
        public string FreightPayment { get; set; }
        public string CustomerSpecialInstructions { get; set; }
        public string SpecialTerms { get; set; }
        public string ConvertedFromCustomer { get; set; }
        public string VendorRegionID { get; set; }
        public string VendorSourceID { get; set; }
        public string VendorIndustryID { get; set; }
        public bool Confirmed { get; set; }
        public string ReferedBy { get; set; }
        public string ReferedDate { get; set; }
        public string ReferalURL { get; set; }
        public decimal AccountBalance { get; set; }
        public DateTime CreationTime { get; set; }
        public bool IsActive { get; set; }
    }
}
