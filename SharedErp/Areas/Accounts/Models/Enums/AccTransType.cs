﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Accounts.Models.Enums
{
    public enum AccTransType
    {
        Deposit,
        PayVoucher,
        DirectLodgement,
        Renewal,
        DebitMemo,
        CreditMemo,
        Invoice,
        Receipt,
        Purchase,
        Adjustment,
        Beginning,
        GLEntry,
        JVEntry,
        ServiceCharge,
        InvReceived,
        IssueStock,
        TransferTo,
        TransferFrom,
        Manufacture
    }
}
