﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedErp.Areas.Accounts.Models
{
    public class BillTransactionModel
    {
        public IEnumerable<BillsDetailDTO> BillsDetailDTO { get; set; }
        public BillsHeaderDTO BillsHeaderDTO { get; set; }
    }
}
